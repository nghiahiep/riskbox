from django.apps import AppConfig


class CatPageConfig(AppConfig):
    name = 'cat_page'
