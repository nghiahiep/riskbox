# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-10-05 18:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='black_list_on_id_dedupe_model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_national_id', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='customer_national_id')),
                ('customer_name', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='customer_name')),
                ('company_taxcode', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='company_taxcode')),
                ('company_name', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='company_name')),
                ('fb_number', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='fb_number')),
                ('phone_number', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='phone_number')),
                ('dsa_code', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='dsa_code')),
                ('blacklist_reason', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='blacklist_reason')),
                ('status', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='status')),
                ('exprired_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='exprired_date')),
                ('active_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='active_date')),
                ('deactive_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='deactive_date')),
            ],
        ),
        migrations.CreateModel(
            name='black_list_on_mafc_employee_dedupe_model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_national_id', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='customer_national_id')),
                ('customer_name', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='customer_name')),
                ('company_taxcode', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='company_taxcode')),
                ('company_name', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='company_name')),
                ('fb_number', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='fb_number')),
                ('phone_number', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='phone_number')),
                ('dsa_code', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='dsa_code')),
                ('blacklist_reason', models.CharField(blank=True, db_index=True, max_length=300, null=True, verbose_name='blacklist_reason')),
                ('status', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='status')),
                ('exprired_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='exprired_date')),
                ('active_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='active_date')),
                ('deactive_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='deactive_date')),
                ('date_of_joining', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='date_of_joining')),
                ('end_date', models.CharField(blank=True, db_index=True, max_length=100, null=True, verbose_name='end_date')),
            ],
        ),
        migrations.CreateModel(
            name='dedupe_id_model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(db_index=True, max_length=50, verbose_name='app_id')),
                ('customer_name', models.CharField(db_index=True, max_length=200, verbose_name='customer_name')),
                ('customer_id_number', models.CharField(db_index=True, max_length=100, verbose_name='customer_id_number')),
                ('dob', models.CharField(blank=True, db_index=True, default=None, max_length=20, null=True, verbose_name='dob')),
                ('dpd', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='issue_date')),
                ('issue_place', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='issue_place')),
                ('operate_date', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='operate_date')),
                ('product', models.CharField(blank=True, default=None, max_length=200, null=True, verbose_name='product')),
                ('status', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='status')),
                ('current_province', models.CharField(blank=True, default=None, max_length=300, null=True, verbose_name='current_province')),
                ('fb_province', models.CharField(blank=True, default=None, max_length=300, null=True, verbose_name='fb_province')),
                ('spouse_name', models.CharField(blank=True, default=None, max_length=300, null=True, verbose_name='spouse_name')),
                ('spouse_id', models.CharField(blank=True, default=None, max_length=300, null=True, verbose_name='spouse_id')),
                ('on_system', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='on_system')),
            ],
        ),
        migrations.CreateModel(
            name='dedupe_mobile_cux_model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(db_index=True, max_length=50, verbose_name='app_id')),
                ('customer_id_no', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='customer_id_no')),
                ('address_type', models.CharField(max_length=200, verbose_name='address_type')),
                ('cux_phone1', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='cux_phone1')),
                ('cux_mobile', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='cux_mobile')),
                ('cux_add1', models.CharField(blank=True, db_index=True, max_length=500, null=True, verbose_name='cux_add1')),
                ('dpd', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='issue_date')),
                ('status', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='status')),
            ],
        ),
        migrations.CreateModel(
            name='dedupe_mobile_ref_model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(db_index=True, max_length=50, verbose_name='app_id')),
                ('customer_id_no', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='customer_id_no')),
                ('ref_name', models.CharField(blank=True, max_length=200, null=True, verbose_name='ref_name')),
                ('ref_rel', models.CharField(blank=True, max_length=50, null=True, verbose_name='ref_rel')),
                ('ref_phone1', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='ref_phone1')),
                ('ref_phone2', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='ref_phone2')),
                ('ref_stdisd', models.CharField(blank=True, db_index=True, max_length=50, null=True, verbose_name='ref_stdisd')),
                ('dpd', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='issue_date')),
                ('status', models.CharField(blank=True, default=None, max_length=100, null=True, verbose_name='status')),
            ],
        ),
    ]
