# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

from dedupe_page.models import dedupe_mobile_cux_model, dedupe_mobile_ref_model

from django.db.models import Q

from django.urls import reverse



# Create your views here.
def page(request):

    if request.user.is_authenticated():
        user_groups = request.user.groups.values_list('name', flat=True)
        if "UW_POS" in user_groups or"UND" in user_groups:
            pass
        else:
            return HttpResponseRedirect(reverse("login_page"))
    else:
        return HttpResponseRedirect(reverse("login_page"))

        

    
    #get customer mobile phone and ref phone
    get_app_id_for_dedupe_phone = request.GET.get('search_app_id_for_dedupe_mobile')
    button_get_app_for_dedupe_phone = request.GET.get('button_get_app_for_dedupe_mobile')


    #search for customer mobile phone
    searched_cux_mobilephone = request.GET.get('search_cux_mobile_phone_number')
    button_mobilephone = request.GET.get('button_get_cux_mobile_phone')


    #search for ref 1 mobile phone
    searched_ref1_mobilephone = request.GET.get('search_mobile_phone_number_ref1')
    button_ref1_mobilephone = request.GET.get('button_get_ref1_mobile_phone')


    #search for ref 2 mobile phone
    searched_ref2_mobilephone = request.GET.get('search_mobile_phone_number_ref2')
    button_ref2_mobilephone = request.GET.get('button_get_ref2_mobile_phone')


    #search for ref 2 mobile phone
    searched_ref3_mobilephone = request.GET.get('search_mobile_phone_number_ref3')
    button_ref3_mobilephone = request.GET.get('button_get_ref3_mobile_phone')



    if searched_cux_mobilephone:
        lengh_of_mobilephone = len(searched_cux_mobilephone)
    else:
        lengh_of_mobilephone = 0


    if button_get_app_for_dedupe_phone and get_app_id_for_dedupe_phone:

        oracle_sql = (" SELECT   1 as id, E.APP_ID_C, A.BPID, A.MOBILE, " +
                      "          B.REFERENCE_NAME REFERENCE_NAME1, B.REF_MOBILE REF_MOBILE1, " +
                      "          C.REFERENCE_NAME REFERENCE_NAME2, C.REF_MOBILE REF_MOBILE2, " +
                      "          D.REFERENCE_NAME REFERENCE_NAME3, D.REF_MOBILE REF_MOBILE3  " +
                      "  FROM MACAS.NBFC_ADDRESS_M@LOSDR A, " +
                      "      RISK_FRAUD_TAKE_REF_MOB_1 B, " +
                      "      RISK_FRAUD_TAKE_REF_MOB_2 C, " +
                      "      RISK_FRAUD_TAKE_REF_MOB_3 D, " +
                      "      RISK_FRAUD_TAKE_CUS_ID_PROD E " +
                      "  WHERE " +
                      "      A.BPID = B.CUST_ID_N AND " +
                      "      A.BPID = C.CUST_ID_N AND " +
                      "      A.BPID = D.CUST_ID_N AND " +
                      "      A.BPID = E.CUST_ID_N AND " +
                      "      A.ADDRESSTYPE = 'CURRES' AND "+
                      "      E.APP_ID_C = %s")

        queryset_list  = dedupe_mobile_cux_model.objects.raw(oracle_sql,[get_app_id_for_dedupe_phone])

        context = {     
                    "button_get_app_for_dedupe_phone": button_get_app_for_dedupe_phone,
                    "get_app_id_for_dedupe_phone": get_app_id_for_dedupe_phone,
                    "button_mobilephone": button_mobilephone,
                    "get_infor_for_dedupe_mobile": queryset_list
                    }

        return render(request, './en/dedupe/dedupe_mobile_phone.html', context)


    #elif lengh_of_mobilephone <= 8:
    #    report_error = "Please input at least 8 digits!"
    #    return render(request, './en/dedupe/dedupe_mobile_phone.html',{  "report_error":report_error, })
    
    elif button_mobilephone and searched_cux_mobilephone:
        mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.filter( Q(cux_phone1 =searched_cux_mobilephone) | Q(cux_mobile =searched_cux_mobilephone)) 
        mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.filter( Q(ref_phone1 =searched_cux_mobilephone) | Q(ref_phone2 =searched_cux_mobilephone) | Q(ref_stdisd__contains=searched_cux_mobilephone)) 

        context = {     
                    "mobilephone_dupe_result": mobilephone_dupe_result,
                    "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                    "button_get_dedupe_mobile_phone": button_mobilephone,
                    "button_get_cux_mobile_phone": button_mobilephone,
                    }

        return render(request, './en/dedupe/dedupe_mobile_phone.html', context)

    elif button_ref1_mobilephone and searched_ref1_mobilephone:
        mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.filter( Q(cux_phone1 =searched_ref1_mobilephone) | Q(cux_mobile =searched_ref1_mobilephone)) 
        mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.filter( Q(ref_phone1 =searched_ref1_mobilephone) | Q(ref_phone2 =searched_ref1_mobilephone) | Q(ref_stdisd =searched_ref1_mobilephone)) 

        context = {     
                    "mobilephone_dupe_result": mobilephone_dupe_result,
                    "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                    "button_get_dedupe_mobile_phone": button_ref1_mobilephone,
                    "button_get_ref1_mobile_phone": button_ref1_mobilephone
                    }

        return render(request, './en/dedupe/dedupe_mobile_phone.html', context)

    elif button_ref2_mobilephone and searched_ref2_mobilephone:
        mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.filter( Q(cux_phone1 =searched_ref2_mobilephone) | Q(cux_mobile =searched_ref2_mobilephone)) 
        mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.filter( Q(ref_phone1 =searched_ref2_mobilephone) | Q(ref_phone2 =searched_ref2_mobilephone) | Q(ref_stdisd =searched_ref2_mobilephone)) 

        context = {     
                    "mobilephone_dupe_result": mobilephone_dupe_result,
                    "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                    "button_get_dedupe_mobile_phone": button_ref2_mobilephone,
                    "button_get_ref2_mobile_phone": button_ref2_mobilephone
                    }

        return render(request, './en/dedupe/dedupe_mobile_phone.html', context)

    elif button_ref3_mobilephone and searched_ref3_mobilephone:
        mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.filter( Q(cux_phone1 =searched_ref3_mobilephone) | Q(cux_mobile =searched_ref3_mobilephone)) 
        mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.filter( Q(ref_phone1 =searched_ref3_mobilephone) | Q(ref_phone2 =searched_ref3_mobilephone) | Q(ref_stdisd =searched_ref3_mobilephone)) 

        context = {     
                    "mobilephone_dupe_result": mobilephone_dupe_result,
                    "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                    "button_get_dedupe_mobile_phone": button_ref3_mobilephone,
                    "button_get_ref2_mobile_phone": button_ref3_mobilephone
                    }

        return render(request, './en/dedupe/dedupe_mobile_phone.html', context)

    else:
        report_error = "Please input mobilephone number!"
        return render(request, './en/dedupe/dedupe_mobile_phone.html',{  "report_error":report_error, })