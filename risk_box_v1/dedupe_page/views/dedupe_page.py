# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

from dedupe_page.models import dedupe_id_model, black_list_id_history_model

import re

from django.urls import reverse

import datetime


# Create your views here.

def page(request):

    if request.user.is_authenticated():
        user_groups = request.user.groups.values_list('name', flat=True)
        if "Data Checker" in user_groups or "UND" in user_groups:
            pass
        else:
            return HttpResponseRedirect(reverse("login_page"))
    else:
        return HttpResponseRedirect(reverse("login_page"))
    
    #if request.method == 'GET' and 'search_app_id' in request.GET:

    query001 = request.GET.get('search_app_id')

    query002 = request.GET.get('search_customer_name')
    query003 = request.GET.get('search_customer_dob')
    query004 = request.GET.get('search_customer_national_id')
    query005 = request.GET.get('search_spouse_customer_name')
    query006 = request.GET.get('search_customer_spouse_national_id')


    action = request.GET.get('button_get_app')

    #bellow is button for seaching dedupe
    #cux search
    action1 = request.GET.get('button_get_app_info')
    #spouse search
    action2 = request.GET.get('button_get_spouse_info')
    #id search
    action3 = request.GET.get('button_get_info_dc_style')



    if query001 is not None:
        query01 = query001.upper()
    else:
        query01 = query001

    if query002 is not None:
        query02 = query002.upper()
        query012 = re.sub('[!@#$%]', '', query02)
    else:
        query02 = query002

    if query003 is not None:
        query03 = query003.upper()
        query013 = re.sub('[!@#$%]', '', query03)
    else:
        query03 = query003

    if query004 is not None:
        query04 = query004.upper()
        query014 = re.sub('[!@#$%]', '', query04)
    else:
        query04 = query004

    if query005 is not None:
        query05 = query005.upper()
        query015 = re.sub('[!@#$%]', '', query05)
    else:
        query05 = query005

    if query006 is not None:
        query06 = query006.upper()
        query016 = re.sub('[!@#$%]', '', query06)
    else:
        query06 = query006
    

    if action and query001:
        #queryset_list  = dedupe_id_model.objects.filter(app_id=query01)
        oracle_sql = ("SELECT 1 as id, B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                      "               B.SPOUSENAME, B.SPOUSE_ID_C" + 
                      "   FROM RISK_FRAUD_TAKE_CUS_IN_PROD B WHERE B.CUST_ID_N IN " +
                      "  (SELECT A.CUST_ID_N FROM RISK_FRAUD_TAKE_CUS_ID_PROD A WHERE A.APP_ID_C = %s )")

        oracle_sql1 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUSTOMER_ID(%s)) AS BLACK_ID FROM DUAL")

        oracle_sql2 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUX_MOBILEPHONE(%s)) AS BLACK_PHONE FROM DUAL")

        oracle_sql3 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_REF_MOBILEPHONE(%s)) AS BLACK_REF_PHONE FROM DUAL")

        oracle_sql4 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_DSA(%s)) AS BLACK_DSA FROM DUAL")

        oracle_sql5 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_EMPLOYEE_ID(%s)) AS BLACK_MAFC_ID FROM DUAL")

        oracle_sql6 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_CUX_MOBI(%s)) AS BLACK_MAFC_CUX_MOBILE FROM DUAL")

        oracle_sql7 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_REF_MOBI(%s)) AS BLACK_MAFC_REF_MOBILE FROM DUAL")


        queryset_list  = dedupe_id_model.objects.raw(oracle_sql,[query001])

        queryset_list1  = dedupe_id_model.objects.raw(oracle_sql1,[query001])

        queryset_list2  = dedupe_id_model.objects.raw(oracle_sql2,[query001])

        queryset_list3  = dedupe_id_model.objects.raw(oracle_sql3,[query001])

        queryset_list4  = dedupe_id_model.objects.raw(oracle_sql4,[query001])

        queryset_list5  = dedupe_id_model.objects.raw(oracle_sql5,[query001])

        queryset_list6  = dedupe_id_model.objects.raw(oracle_sql6,[query001])

        queryset_list7  = dedupe_id_model.objects.raw(oracle_sql7,[query001])



        each_result1 = 0

        for each_result1_1 in queryset_list1:
            if each_result1_1.black_id is not None:
                each_result1 = each_result1_1.black_id
            else:
                each_result1 = 0


        each_result2 = 0

        for each_result2_1 in queryset_list2:
            if each_result2_1.black_phone is not None:
                each_result2 = each_result2_1.black_phone
            else:
                each_result2 = 0


        each_result3 = 0

        for each_result3_1 in queryset_list3:
            if each_result3_1.black_ref_phone is not None:
                each_result3 = each_result3_1.black_ref_phone
            else:
                each_result3 = 0


        each_result4 = 0

        for each_result4_1 in queryset_list4:
            if each_result4_1.black_dsa is not None:
                each_result4 = each_result4_1.black_dsa
            else:
                each_result4 = 0



        each_result5 = 0

        for each_result5_1 in queryset_list5:
            if each_result5_1.black_mafc_id is not None:
                each_result5 = each_result5_1.black_mafc_id
            else:
                each_result5 = 0



        each_result6 = 0

        for each_result6_1 in queryset_list6:
            if each_result6_1.black_mafc_cux_mobile is not None:
                each_result6 = each_result6_1.black_mafc_cux_mobile
            else:
                each_result6 = 0



        each_result7 = 0

        for each_result7_1 in queryset_list7:
            if each_result7_1.black_mafc_ref_mobile is not None:
                each_result7 = each_result7_1.black_mafc_ref_mobile
            else:
                each_result7 = 0

                

        final_result =  str(each_result1)+ str(each_result2) + str (each_result3) + str(each_result4) + str (each_result5) + str(each_result6) + str (each_result7)   


        final_result_r = 'WHITE'
        if (each_result1 + each_result2 + each_result3 + each_result4 + each_result5 + each_result6 + each_result7)>0 :
            final_result_r = 'BLACK'


        #does not save black list result when checking dedupe at dedupe screen
        #final_result_save = black_list_id_history_model.objects.create(app_id_c=query001, result_date = datetime.datetime.today(),result_detail=final_result, result_final=final_result_r, user_check = request.user)


        context = {     
                    "basic_app_infor": queryset_list,
                    "basic_app_infor1": queryset_list1,
                    "basic_app_infor2": queryset_list2,
                    "basic_app_infor3": queryset_list3,
                    "basic_app_infor4": queryset_list4,
                    "basic_app_infor5": queryset_list5,
                    "basic_app_infor6": queryset_list6,
                    "basic_app_infor7": queryset_list7,
                    "black_list_result": final_result_r,
                    "button_get_app": action,
                    "get_app_id": query001,
                    }

        return render(request, './en/dedupe/dedupe.html', context)    



    elif action1 and query02 and query03 and query04:

        digit = 0

        for i in query04:
            if i.isnumeric():
                digit+=1

        if digit <9:
            report_error = "Please input correct ID No of customer!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                                " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                                " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                                " WHERE ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR (%s),'[[:space:]]*', '') " +
                                "       AND DOB = %s) OR (CUSTOMER_ID_NUMBER = %s ) OR (SPOUSE_ID = %s) " )

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query02, query03, query04, query04])

            context = { 
                    "object_list": queryset_list,
                    "button_value": action,
                    "button_value_1": action1
                    }
                    

        return render(request, './en/dedupe/dedupe.html', context)




    elif action2 and query05 and query06:

        digit = 0

        for i in query06:
            if i.isnumeric():
                digit+=1

        if digit <3:
            report_error = "Please input at least 3 digits!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                                " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                                " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                                " WHERE (( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(SPOUSE_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND SPOUSE_ID LIKE %s ))" +
                                "       OR ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND CUSTOMER_ID_NUMBER LIKE %s)")

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query05, query06, query05, query06])

            context = { 
                        "object_list": queryset_list,
                        "button_value": action,
                        "button_value_1": action2
                        }


        return render(request, './en/dedupe/dedupe.html', context)


    elif action3:

        query04 = query04 + '%'

        digit = 0

        for i in query04:
            if i.isnumeric():
                digit+=1

        if digit <3:
            report_error = "Please input at least 5 digits!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                    "           OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                    "   FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                    "   WHERE CUSTOMER_ID_NUMBER LIKE %s AND CUSTOMER_NAME LIKE %s AND DOB LIKE %s")

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query04, query02, query03])


            context = { 
                "object_list": queryset_list,
                "button_value": action,
                "button_value_1": action1,
                "button_value_3": action3,
                }


        return render(request, './en/dedupe/dedupe.html', context)


    else:

        report_error = "Please input appid or required information for searching!"
        return render(request, './en/dedupe/dedupe.html',{  "report_error":report_error, })


