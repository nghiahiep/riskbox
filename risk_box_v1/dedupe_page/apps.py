from django.apps import AppConfig


class DedupePageConfig(AppConfig):
    name = 'dedupe_page'
