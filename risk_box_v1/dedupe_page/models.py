from django.db import models


class dedupe_id_model(models.Model):
    app_id = models.CharField(max_length=50, verbose_name="app_id", db_index=True)
    customer_name = models.CharField(max_length=200, verbose_name="customer_name", db_index=True)
    customer_id_number = models.CharField(max_length=100, verbose_name="customer_id_number", db_index=True)
    dob = models.CharField(max_length=20, verbose_name="dob" , null=True, default=None, blank=True, db_index=True)
    dpd = models.CharField(max_length=100, verbose_name="issue_date" , null=True, default=None, blank=True)
    issue_place = models.CharField(max_length=100, verbose_name="issue_place" , null=True, default=None, blank=True)
    operate_date = models.CharField(max_length=100, verbose_name="operate_date" , null=True, default=None, blank=True)
    product = models.CharField(max_length=200, verbose_name="product" , null=True, default=None, blank=True)
    status = models.CharField(max_length=100, verbose_name="status" , null=True, default=None, blank=True)
    current_province = models.CharField(max_length=300, verbose_name="current_province" , null=True, default=None, blank=True)
    fb_province = models.CharField(max_length=300, verbose_name="fb_province" , null=True, default=None, blank=True)
    spouse_name = models.CharField(max_length=300, verbose_name="spouse_name" , null=True, default=None, blank=True)
    spouse_id = models.CharField(max_length=300, verbose_name="spouse_id" , null=True, default=None, blank=True)
    on_system = models.CharField(max_length=100, verbose_name="on_system" , null=True, default=None, blank=True)
    
    def __str__ (self):
        return self.customer_name


class dedupe_mobile_cux_model(models.Model):
    app_id = models.CharField(max_length=50, verbose_name="app_id", db_index=True)
    customer_id_no = models.CharField(max_length=50, verbose_name="customer_id_no", null=True, blank=True, db_index=True)
    address_type = models.CharField(max_length=200, verbose_name="address_type")
    cux_phone1 = models.CharField(max_length=50, verbose_name="cux_phone1", null=True, blank=True, db_index=True)
    cux_mobile = models.CharField(max_length=50, verbose_name="cux_mobile", null=True, blank=True, db_index=True)
    cux_add1 = models.CharField(max_length=500, verbose_name="cux_add1", null=True, blank=True, db_index=True)
    dpd = models.CharField(max_length=100, verbose_name="issue_date" , null=True, default=None, blank=True)
    status = models.CharField(max_length=100, verbose_name="status" , null=True, default=None, blank=True)

    def __str__(self):
        return self.cux_add1


class dedupe_mobile_ref_model(models.Model):
    app_id = models.CharField(max_length=50, verbose_name="app_id", db_index=True)
    customer_id_no = models.CharField(max_length=50, verbose_name="customer_id_no", null=True, blank=True, db_index=True)
    ref_name = models.CharField(max_length=200, verbose_name="ref_name", null=True, blank=True)
    ref_rel = models.CharField(max_length=50, verbose_name="ref_rel", null=True, blank=True)
    ref_phone1 = models.CharField(max_length=50, verbose_name="ref_phone1", null=True, blank=True, db_index=True)
    ref_phone2 = models.CharField(max_length=50, verbose_name="ref_phone2", null=True, blank=True, db_index=True)
    ref_stdisd = models.CharField(max_length=50, verbose_name="ref_stdisd", null=True, blank=True, db_index=True)
    dpd = models.CharField(max_length=100, verbose_name="issue_date" , null=True, default=None, blank=True)
    status = models.CharField(max_length=100, verbose_name="status" , null=True, default=None, blank=True)

    def __str__(self):
        return self.ref_name


class black_list_on_id_dedupe_model(models.Model):
    customer_national_id = models.CharField(max_length=50, verbose_name="customer_national_id", null=True, blank=True, db_index=True)
    customer_name = models.CharField(max_length=300, verbose_name="customer_name", null=True, blank=True, db_index=True)
    company_taxcode = models.CharField(max_length=300, verbose_name="company_taxcode", null=True, blank=True, db_index=True)
    company_name = models.CharField(max_length=300, verbose_name="company_name", null=True, blank=True, db_index=True)  
    fb_number = models.CharField(max_length=100, verbose_name="fb_number", null=True, blank=True, db_index=True) 
    phone_number = models.CharField(max_length=100, verbose_name="phone_number", null=True, blank=True, db_index=True) 
    dsa_code = models.CharField(max_length=100, verbose_name="dsa_code", null=True, blank=True, db_index=True) 
    blacklist_reason = models.CharField(max_length=300, verbose_name="blacklist_reason", null=True, blank=True, db_index=True) 
    status = models.CharField(max_length=100, verbose_name="status", null=True, blank=True, db_index=True) 
    exprired_date = models.CharField(max_length=100, verbose_name="exprired_date", null=True, blank=True, db_index=True)
    active_date = models.CharField(max_length=100, verbose_name="active_date", null=True, blank=True, db_index=True)
    deactive_date = models.CharField(max_length=100, verbose_name="deactive_date", null=True, blank=True, db_index=True)

    def __str__(self):
        return self.customer_name


class black_list_on_mafc_employee_dedupe_model(models.Model):
    customer_national_id = models.CharField(max_length=50, verbose_name="customer_national_id", null=True, blank=True, db_index=True)
    customer_name = models.CharField(max_length=300, verbose_name="customer_name", null=True, blank=True, db_index=True)
    company_taxcode = models.CharField(max_length=300, verbose_name="company_taxcode", null=True, blank=True, db_index=True)
    company_name = models.CharField(max_length=300, verbose_name="company_name", null=True, blank=True, db_index=True)  
    fb_number = models.CharField(max_length=100, verbose_name="fb_number", null=True, blank=True, db_index=True) 
    phone_number = models.CharField(max_length=100, verbose_name="phone_number", null=True, blank=True, db_index=True) 
    dsa_code = models.CharField(max_length=100, verbose_name="dsa_code", null=True, blank=True, db_index=True) 
    blacklist_reason = models.CharField(max_length=300, verbose_name="blacklist_reason", null=True, blank=True, db_index=True) 
    status = models.CharField(max_length=100, verbose_name="status", null=True, blank=True, db_index=True) 
    exprired_date = models.CharField(max_length=100, verbose_name="exprired_date", null=True, blank=True, db_index=True)
    active_date = models.CharField(max_length=100, verbose_name="active_date", null=True, blank=True, db_index=True)
    deactive_date = models.CharField(max_length=100, verbose_name="deactive_date", null=True, blank=True, db_index=True)
    date_of_joining = models.CharField(max_length=100, verbose_name="date_of_joining", null=True, blank=True, db_index=True)
    end_date = models.CharField(max_length=100, verbose_name="end_date", null=True, blank=True, db_index=True)


    def __str__(self):
        return self.customer_name


class black_list_id_history_model(models.Model):
    
    app_id_c = models.CharField(max_length=50, verbose_name="app_id_c", null=True, blank=True, db_index=True)
    result_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    result_detail = models.CharField(max_length=50, verbose_name="result_detail", null=True, blank=True, db_index=True)
    result_final = models.CharField(max_length=50, verbose_name="result_final", null=True, blank=True, db_index=True)
    user_check = models.CharField(max_length=50, verbose_name="result_date", null=True, blank=True, db_index=True)
    
    #class Meta:
    #    app_label = 'storage_data'

    def __str__(self):
        return self.user_check



