﻿///////////////////////////////
// get browser version
var Sys = {};
var ua = navigator.userAgent.toLowerCase();
var s;
(s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
(s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
(s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
(s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
(s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
/////////////////////////////
// Ajax request
var AJAX;
if (AJAX && (typeof AJAX != "object" || AJAX.NAME))
    throw new Error("Namespace 'AJAX' already exists");

// Create our namespace, and specify some meta-information
AJAX = {};
AJAX.NAME = "AJAX";    // The name of this namespace
AJAX.VERSION = 1.0;    // The version of this namespace

// This is a list of XMLHttpRequest creation factory functions to try
AJAX._factories = [
    function() { return new XMLHttpRequest(); },
    function() { return new ActiveXObject("Msxml2.XMLHTTP"); },
    function() { return new ActiveXObject("Microsoft.XMLHTTP"); }
];

// When we find a factory that works, store it here
AJAX._factory = null;
AJAX.newRequest = function() {
    if (AJAX._factory != null) return AJAX._factory();

    for(var i = 0; i < AJAX._factories.length; i++) {
        try {
            var factory = AJAX._factories[i];
            var request = factory();
            if (request != null) {
                AJAX._factory = factory;
                return request;
            }
        }
        catch(e) {
            continue;
        }
    }

    // If we get here, none of the factory candidates succeeded,
    // so throw an exception now and for all future calls.
    AJAX._factory = function() {
        throw new Error("XMLHttpRequest not supported");
    }
    AJAX._factory(); // Throw an error
}
AJAX.getText = function(url, callback) {
    var request = AJAX.newRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200)
            callback(request);
    }
	url=url+"&sid="+Math.random();
    request.open("GET", url);
    request.send(null);
};
AJAX.getXML = function(url, callback) {
    var request = AJAX.newRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4 && request.status == 200)
            callback(request.responseXML);
    }
    request.open("GET", url);
    request.send(null);
};

AJAX.post = function(url, values, callback, errorHandler) {
    var request = AJAX.newRequest();
    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            if (request.status == 200) {
                callback(request);
            }
            else {
                if (errorHandler) errorHandler(request.status,
                                               request.statusText);
                else callback(null);
            }
        }
    }

    request.open("POST", url);
    request.setRequestHeader("Content-Type",
                             "application/x-www-form-urlencoded");
    request.send(values);
};

AJAX.get = function(url, callback, options) {
    var request = AJAX.newRequest();
    var n = 0;
    var timer;
    if (options.timeout)
        timer = setTimeout(function() {
                               request.abort();
                               if (options.timeoutHandler)
                                   options.timeoutHandler(url, callback, options);
                           },
                           options.timeout);

    request.onreadystatechange = function() {
        if (request.readyState == 4) {
            //if (timer) clearTimeout(timer);
            if (request.status == 200) {
				if (timer) clearTimeout(timer);
                callback(request);
            }
            else {
                if (options.errorHandler)
                    options.errorHandler(request.status,
                                         request.statusText);
                else callback(null);
            }
        }
        else if (options.progressHandler) {
            options.progressHandler(++n);
        }
    }
    var target = url+"&sid="+Math.random();
    // if (options.parameters)
    //    target += "?" + HTTP.encodeFormData(options.parameters)
    request.open("GET", target);
    request.send(null);
};

var time = 1;

function jsResponseXmlHttp()
{
	var url = "servlet?p=settings-upgrade&q=askUpgrade";
	var iTimeout = location.protocol == "https:" ? 20000 : 5000;
	var objOption = {
		timeout : iTimeout,				 //default timeout
		timeoutHandler : function(cmd,callback,opt){ 
							time = 1;
							AJAX.get(cmd, callback, opt); 
						}
	}
	function responseRequest(xhr)
	{
		if(!xhr) return;
		time++;
		if(xhr.responseText != "true" && time > 3)
		{
		    window.location = "servlet?p=settings-upgrade&q=load";
		}
		else
		{
			setTimeout(function(){ AJAX.get(url,responseRequest,objOption);}, iTimeout);
		}	
	}
	AJAX.get(url, responseRequest, objOption);
}

function jsNoticeAjaxResponse(url, successUrl, iTimeout)
{
	var objOption = {
		timeout : iTimeout,				 //default timeout
		timeoutHandler : function(cmd,callback,opt){ 
							AJAX.get(cmd, callback, opt); 
						}
	}
	function responseRequest(xhr)
	{
		if(!xhr) return;
		if(xhr.responseText != "updating")
		{
		    window.location = successUrl;
		}
		else
		{
			setTimeout(function(){ AJAX.get(url,responseRequest,objOption);}, iTimeout);
		}	
	}
	AJAX.get(url, responseRequest, objOption);
}

///////////////////////////////////////////////////////////////////
//
var DHTMLAPI = {
	browserClass : new Object(),
	init : function () 
	{
		this.browserClass.isCSS = ((document.body && document.body.style) ? true : false),
		this.browserClass.isW3C = ((this.browserClass.isCSS && document.getElementById) ? true : false),
		this.browserClass.isIE4 = ((document.body && document.body.style) ? true : false),
		this.browserClass.isW3C = ((this.browserClass.isCSS && document.all) ? true : false),
		this.browserClass.isNN4 = ((document.layers) ? true : false),
		this.browserClass.isIECSSCompat = ((document.compatMode && document.compatMode.indexof("CSS1") >= 0) ? true : false)
	},
	getStyleObject : function (elemRef)
	{
		var elem = elemRef;
		if(elem && this.browserClass.isCSS) 
		{
			elem = elem.style;
		}
		return elem;
	},
	moveBy : function (elemRef, deltaX, deltaY) {
		var elem = elemRef;
		if (elem){
			if (this.browserClass.isCSS) {
				var units = (typeof elem.left == "string") ? "px" : 0;
				if(!isNaN(this.getElementLeft(elemRef))) {
					elem.left = this.getElementLeft(elemRef) + deltaX + units;
					elem.top = this.getElementTop(elemRef) + deltaY + units;
				}
				else if (this.browserClass.isNN4){
					elem.moveBy(deltaX, deltaY);
				}
			}
		}
	},
	getComputedStyle : function (elemRef, CSSStyleProp)
	{
		var elem = elemRef;
		var styleValue, camel;
		if(elem)
		{
			if(document.defaultView && document.defaultView.getComputedStyle)
			{
				var compStyle = document.defaultView.getComputedStyle(elem, "");
				styleValue = compStyle.getPropertyValue(CSSStyleProp);
			}
			else if (elem.currentStyle)
			{
				var IEStyleProp = CSSStyleProp;
				var re = /-\D/;
				while (re.test(IEStyleProp))
				{
					camel = IEStyleProp.match(re)[0].charAt(1).toUpperCase();
					IEStyleProp = IEStyleProp.replace(re, camel);
				}
				styleValue = elem.currentStyle[IEStyleProp];
			}
		}
		return (styleValue) ? styleValue : null;
	},
	getElementLeft : function (elemRef) {
		var elem = elemRef;
		var result = null;
		if(this.browserClass.isCSS || this.browserClass.isW3C){
			result = parseInt(this.getComputedStyle(elem, "left"));
		}
		else if (this.browserClass.isNN4) {
			return elem.left;
		}
		return result;
	},
	getElementTop : function (elemRef) {
		var elem = elemRef;
		var result = null;
		if(this.browserClass.isCSS || this.browserClass.isW3C){
			result = parseInt(this.getComputedStyle(elem, "top"));
		}
		else if (this.browserClass.isNN4) {
			return elem.top;
		}
		return result;
	},
	getElementHeight : function (elemRef) {
		var result = null;
		var elem = elemRef;
		if(elem)
		{
			if(elem.offsetHeight){
			result = elem.offsetHeight;
			}
			else if(elem.clip && elem.clip.height){
				result = elem.clip.height;
			}
		}
		return result;
	}
}


///////////////////////////////////////////////////////////////////
//
var Geometry = {};

if (window.screenLeft) { // IE and others
    Geometry.getWindowX = function() { return window.screenLeft; };
    Geometry.getWindowY = function() { return window.screenTop; };
}
else if (window.screenX) { // Firefox and others
    Geometry.getWindowX = function() { return window.screenX; };
    Geometry.getWindowY = function() { return window.screenY; };
}

if (window.innerWidth) { // All browsers but IE
    Geometry.getViewportWidth = function() { return window.innerWidth; };
    Geometry.getViewportHeight = function() { return window.innerHeight; };
    Geometry.getHorizontalScroll = function() { return window.pageXOffset; };
    Geometry.getVerticalScroll = function() { return window.pageYOffset; };
}
else if (document.documentElement && document.documentElement.clientWidth) {
    // These functions are for IE6 when there is a DOCTYPE
    Geometry.getViewportWidth =
        function() { return document.documentElement.clientWidth; };
    Geometry.getViewportHeight = 
        function() { return document.documentElement.clientHeight; };
    Geometry.getHorizontalScroll = 
        function() { return document.documentElement.scrollLeft; };
    Geometry.getVerticalScroll = 
        function() { return document.documentElement.scrollTop; };
}
else if (document.body && document.body.clientWidth) {
    // These are for IE4, IE5, and IE6 without a DOCTYPE
    Geometry.getViewportWidth =
        function() { return document.body.clientWidth; };
    Geometry.getViewportHeight =
        function() { return document.body.clientHeight; };
    Geometry.getHorizontalScroll =
        function() { return document.body.scrollLeft; };
    Geometry.getVerticalScroll = 
        function() { return document.body.scrollTop; };
}

