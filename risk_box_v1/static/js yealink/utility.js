﻿var logo_link="http://www.yealink.com";
function jsFilterDoorPhoneNumber(obj, minValue, maxValue, defaultValue)
{
	if (typeof defaultValue == "undefined")
	{
		defaultValue = "";
	}
	if (typeof obj == "undefined")
	{
		obj.value = defaultValue;
		return;
	}
	if(obj.value == "")
	{
		obj.value = defaultValue;
		return;
	}
	else if(parseInt(obj.value) > maxValue)
	{
		obj.value = maxValue;
		return;
	}
	else if(parseInt(obj.value) < minValue)
	{
		obj.value = minValue;
		return;
	}
	var value = obj.value;
	var length = value.length;
	var dotNum = 0;
	var chValue;
	for (var i = 0; i < length; i++)
	{
		chValue = value.substr(i,1);
		if (chValue == ".")
		{
			dotNum++;
		}
		if ((chValue != "."
			&& (chValue > "9" || chValue < "0"))
			|| dotNum > 1)
		{
			obj.value = defaultValue;
			return;
		}
	}
	if (length > 1
		&& value.substr(0,1) == '0')
	{
		var index = 1;
		for (; index < value.length; index++)
		{
			if (value.substr(index,1) != '0')
			{
				break;
			}
		}
		value = (value.substr(index,1) == '.') ? value.substr(index-1) : value.substr(index);
	}
	value = (value.substr(0,1) == '.') ? ('0' + value) : value;
	value = (value.substr(value.length - 1,1) == '.') ? (value + '0') : value;
	for(i = 0; i < value.length; i++)
	{
		if(value.substr(i,1) == '.')
		{
			obj.value = value.substr(0,i+2);
		}
	}
}
function logo_link_jump()
{
	var obj = document.getElementById("hiddenWebLogoLink");
	if (obj) {
		window.location = obj.value;
	}
	else {
		window.location = logo_link;
	}
}

function jsFilterNumber(evnt, bSigned)
{
	if (bSigned)
	{
		return jsFilterCharInput(evnt, /[-0-9]/);
	}
	return jsFilterCharInput(evnt, /[0-9]/);
}

function jsAutoCorrecValue(objEdit, minValue, maxValue, defaultValue)
{
	if(objEdit.value.search(/^-?[0-9]+$/) != 0)
	{
		if (typeof defaultValue != "undefined")
		{
			objEdit.value = defaultValue;
		}
		else
		{
			objEdit.value = objEdit.defaultValue;	
		}
	}
	else if(parseInt(objEdit.value) > maxValue)
	{
		objEdit.value = maxValue;
	}
	else if(parseInt(objEdit.value) < minValue)
	{
		objEdit.value = minValue;
	}
}

function jsAutoCorrecDefaultValue(objEdit, minValue, maxValue, defaultValue)
{
	if(objEdit.value.search(/^-?[0-9]+$/) != 0)
	{
		if (typeof defaultValue != "undefined")
		{
			objEdit.value = defaultValue;
		}
		else
		{
			objEdit.value = objEdit.defaultValue;
		}
	}
	else if(parseInt(objEdit.value) > maxValue || parseInt(objEdit.value) < minValue)
	{
		objEdit.value = defaultValue;
	}
}

function jsAutoCorrecToNumber(objEdit)
{
	if(objEdit.value.search(/^[0-9]+$/) != 0)
	{
		alert(TR("Number Only!!"));
		jsFocusElement(objEdit);
		return false;
		//objEdit.value = "";
	}
	return true;
}

function cTrim(sInputString,iType)// iType:0 1 2
    {
		var sTmpStr = ' ';
		var i = -1;
		if(iType == 0 || iType == 1)
		{
			while(sTmpStr == ' ')
			{
				++i;
				sTmpStr = sInputString.substr(i,1);
			}
			sInputString = sInputString.substring(i);
			
		}
		if(iType == 0 || iType == 2)
		{
			sTmpStr = ' ';
			i = sInputString.length;
			while(sTmpStr == ' ')
			{
				--i;
				sTmpStr = sInputString.substr(i,1);
			}
			sInputString = sInputString.substring(0,i+1);
		}
		return sInputString;
	}
	
function isVailidAccountStr(str)
{
	var regex = /[!&]/;
	return !regex.test(str);
}

function isValidEmergencyNum(obj)
{
	if (!obj || isBlank(obj.value)) {
	     return true;
	}
	var validRE = /^[\d,]+$/;
	return validRE.test(obj.value);
}

function IsViliadAndSafeXMLString(objEdit)
{
	var str = cTrim(objEdit.value,0);
	var invalidchar = /['"]/;
	if (!invalidchar.test(str) && jsIsSafety(str))
	{
		return true;
	}
	else
	{
		alert(TR("Input contains invalid character like <>'\"\\"));
		jsFocusElement(objEdit);
		return false;
	}
}

function IsViliadContactString(objEdit)
{
	var str = cTrim(objEdit.value,0);
	var invalidchar = /['"\\]/;
	if (!invalidchar.test(str))
	{
		return true;
	}
	else
	{
		alert(TR("Input contains invalid character like <>'\"\\"));
		jsFocusElement(objEdit);
		return false;
	}
}

function IsViliadAndSafeContactString(objEdit)
{
	var str = cTrim(objEdit.value,0);
	var invalidchar = /['"\\]/;
	if (!invalidchar.test(str) && jsIsSafety(str))
	{
		return true;
	}
	else
	{
		alert(TR("Input contains invalid character like <>'\"\\"));
		jsFocusElement(objEdit);
		return false;
	}
}

function jsFilterCharInput(evnt, reValidChars)
{
	if(evnt.which != null && (evnt.which == 0 || evnt.which == 8 || evnt.which == 9))
	{// skip Netscape/Firefox/Opera function keys
		return true;
	}
	var keyCode = window.event ? evnt.keyCode : evnt.which;
	var keyChar = String.fromCharCode(keyCode);
	return reValidChars.test(keyChar);
}

function isBlank(str)
{
	if(!str) return true;
	
	var regex = /[\S]/;
	return !regex.test(str);
}

function isValidDialupNumber(strNumber)
{
	var validRE = /[^A-Za-z0-9\-\(\),\*#\.:@+\$]/;
	return !validRE.test(strNumber);
}

function isValidName(name)
{
//	var regex = /[$*%`()&:^<>!]/;
	var regex = /[$*%&:!]/;
	if(	regex.test(name))
		return false;
	else 
		return true;
}

function isValidAddress(domain_str)
{
	if (isBlank(domain_str))
	{
		return true;
	}
/*	
c = domain_str.charAt(0);
	
	if ((c >= '0') && (c <= '9'))
	{
		if (!(isValidIP(domain_str)))
		{
			if (isValidDomain(domain_str))
			{
				//alert('If you are attempting to enter an IP Address, this could be an Invalid IP Address.');
				return true;
			}
		}	
		else
			return true;
	}
	else
		return isValidDomain(domain_str);
	return false;
*/
	var reg = /^[\d.]+$/;
	if (reg.test(domain_str))
		return isValidIP(domain_str);
	else
		return isValidDomain(domain_str);
}

function isValidIP(s) 
{
	tempstr = s;
	ip_array = (0,0,0,0);
	
	if (isBlank(s))
		return false;
	else
	{
		var IParray = s.split(".");
	
	//	if ((IParray.length>4)||(IParray.length<2))
	//		return false;
		if (IParray.length != 4)
			return false;
	
		for (i=0;i<IParray.length;i++)
		{
			st = IParray[i];
			if (isNaN(st)||isBlank(st)){
				return false;
			}
			if ((st<0)||(st>255))
				return false;
			cutleadingzeros(st);
			IParray[i] = st;
		}
		if (IParray.length==2)
		{
			IParray[3] = IParray[1];
			IParray[1] = 0;
			IParray[2] = 0;
		}
		else if (IParray.length==3)
		{
			IParray[3] = IParray[2];
			IParray[2] = 0;
		}
		ip_array = IParray;
		tempstr = IParray.join(".");
	}
	return true;
}

function cutleadingzeros(st){
	x = 0;
	while (st.charAt(x) == '0')
		x++;
	if (st.length != 1)
	st = st.substring(x, st.length);
	return st;
}

function isValidDomain(domain_str)
{
	if(domain_str.length > 99)
	{
		return false;
	}
/*	var domainArray = domain_str.split('@');
	if(domainArray.length > 2)
	{
		return false;
	}
	else if(domainArray.length == 2)
	{
		var regexFirst = /^[a-zA-Z0-9][-a-zA-Z0-9]{0,44}[a-zA-Z0-9]$/;
		if(regexFirst.test(domainArray[0]) == false)
			return false;
		else
			domain_str = domainArray[1];
	}
	var regex = /^[a-zA-Z0-9][-a-zA-Z0-9]{0,44}[a-zA-Z0-9](\.[a-zA-Z0-9][-a-zA-Z0-9]{0,44}[a-zA-Z0-9])+$/;
*/
	var regex = /^([a-zA-Z0-9-]+\.)+[a-zA-Z0-9-]+\.?$/;
	return regex.test(domain_str);
}

function isValidPort(s)
{
	if (isBlank(s) || !onlyNumbers(s) || (parseInt(s)<1 || parseInt(s)>65535))
	{	
		return false; 
	}
	return true;
}

function onlyNumbers(strNumber)
{
	var validRE = /^\d+$/;
	return validRE.test(strNumber);
}

function onlyNumbersORblank(strNumber)
{
	return isBlank(strNumber) || onlyNumbers(strNumber)
}

function isValidAccountString(name)
{
//	var regex = /[&:!]/;
	var regex = /[&!]/;
	if(	regex.test(name))
		return false;
	else 
		return true;
}

function checkSeparationChar(str)
{
	var regex = /[&]/;
	return regex.test(str);
}

function isValidURL(str)
{
//var regex = /^([a-zA-Z0-9@.-\.\-\/:]+\.)+[a-zA-Z0-9 .-\.\-\/\?\=_:]{2,}$/;
var regex = /(.*?):\/\/(?:([^:@]+)(?::([^@]*))?@)?([^:@\/]+)(?::(\d+))?(?:(\/.*?)?(?:\?(.*?))?(?:#(.*?))?)$/;
return regex.test(str);
} 

function isValidIPv6(addr)
{
	if (addr.match(/::/g) && addr.match(/::/g).length > 1)
	{
		return false;
	}
	if (addr.match(/:/g) == null)
	{
		return false;
	}
	var arrayIPData = addr.split("/");
	if (arrayIPData.length == 2)
	{
		var regex = /[0-9]+/;
		if (!regex.test(arrayIPData[1]))
		{
			return false;
		}
		var prefix = parseInt(arrayIPData[1]);
		if (prefix <= 0 || prefix > 128)
		{
			return false;
		}
		addr = arrayIPData[0];
	}	
	return addr.match(/:/g).length<=7&&/::/.test(addr)?/^([\da-f]{1,4}(:|::)){1,6}[\da-f]{1,4}$/i.test(addr):/^([\da-f]{1,4}:){7}[\da-f]{1,4}$/i.test(addr);
}

function jsFocusElement(obj)
{
	if(!obj) return;
	if(typeof obj.focus != "undefined") obj.focus();
	if(typeof obj.select != "undefined") obj.select();
}

function isValidIPv6Gateway(addr)
{
	if (addr.match(/::/g) && addr.match(/::/g).length > 1)
	{
		return false;
	}
	if (addr.match(/:/g) == null)
	{
		return false;
	}
	var arrayIPData = addr.split("/");
	if (arrayIPData.length == 2)
	{
		var regex = /[0-9]+/;
		if (!regex.test(arrayIPData[1]))
		{
			return false;
		}
		var prefix = parseInt(arrayIPData[1]);
		if (prefix <= 0 || prefix > 128)
		{
			return false;
		}
		addr = arrayIPData[0];
	}
	
	return addr.match(/:/g).length<=7&&/::/.test(addr)?/^([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$|^:((:[0-9a-fA-F]{1,4}){1,6}|:)$|^[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,5}|:)$|^([0-9a-fA-F]{1,4}:){2}((:[0-9a-fA-F]{1,4}){1,4}|:)$|^([0-9a-fA-F]{1,4}:){3}((:[0-9a-fA-F]{1,4}){1,3}|:)$|^([0-9a-fA-F]{1,4}:){4}((:[0-9a-fA-F]{1,4}){1,2}|:)$|^([0-9a-fA-F]{1,4}:){5}:([0-9a-fA-F]{1,4})?$|^([0-9a-fA-F]{1,4}:){6}:$/i.test(addr):/^([\da-f]{1,4}:){7}[\da-f]{1,4}$/i.test(addr);
}


//是否是ipv6相等
function isIPv6Equals(src,des)
{
	var srcArr=convert2CompleteIpV6(src).split(":");
	var desArr=convert2CompleteIpV6(des).split(":");
	for(var i=0;i<8;i++){
		if(parseInt(srcArr[i],16)!=parseInt(desArr[i],16)){
			return false;
		}
	}
	return true;
}

//将压缩格式的转换为常规的IPv6地址	
function convert2CompleteIpV6(ip)
{
	var ipV6=ip;
	var index=ip.indexOf("::");
	if(index>0){
		var size=8-(ip.split(":").length-1);
		var tmp="";
		for(var i=0;i<size;i++){
			tmp+=":0";
		}
		tmp+=":";
		ipV6=ip.replace("::",tmp);
	}else if(index==0){
		ipV6=ip.replace("::","0:0:0:0:0:0:0:");
	}
	return ipV6;
}

function isValidIPV6Prefix(s)
{
	if (s <= 128 && s >= 0)
	{
		return true;
	}
	return false;
}

function isValidMask(s) {
 tempstr = s;
 ip_array = (0,0,0,0);
 if (isBlank(s))
    return false;
 else
 	{
		var IParray = s.split(".");
	 	var bin = new Array();

		if (IParray.length!=4)
			return false;

		for (i=0;i<4;i++)
		{
			st = IParray[i];
			if (isNaN(st)||isBlank(st)){
				return false;
			}
			if ((st<0)||(st>255))
				return false;

			cutleadingzeros(st);
			temp = st;
			for (j=0;j<8;j++)
			{
				bin[8*i+7-j] = temp%2;
				temp = (temp - temp%2)/2;
			}

			IParray[i] = st;
		}
		for (k=0; k<31; k++)
		{
			if(bin[k]<bin[k+1])
				return false;
		}
		ip_array = IParray;
		tempstr = IParray.join(".");
	}
  return true;
}

function isValidDate(uYear, uMonth, uDay)
{
	if(uYear<1 || uMonth<1 || uDay<1 )
	{
		return false;
	}
	switch(uMonth)
	{
		case 1:	case 3:	case 5:	case 7:	case 8:	case 10: case 12:
		{
			if(uDay > 31)
			{
				return false;
			}
			break;
		}
		case 4:	case 6:	case 9:	case 11:
		{
			if(uDay > 30)
			{
				return false;
			}
			break;
		}
		case 2:
		{
			if(uYear%400 == 0 || (uYear%4 == 0 && uYear%100 != 0))
			{
				if(uDay > 29)
				{
					return false;
				}
			}
			else
			{
				if(uDay > 28)
				{
					return false;
				}
			}
			break;
		}
		default:
		{
			return false;
		}
	}
	return true;
}

function jsCheckSepCharInObj(obj, strWarningText)
{
	if(checkSeparationChar(obj.value))
	{
		strWarningText = strWarningText == null ? TR("Warning,the character '&' could not be contain!") : strWarningText;
		alert(strWarningText);
		jsFocusElement(obj);
		return true;
	}
	return false;
}
function trim(stringToTrim) 
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function isValidAccountGroup(strAcGroup)
{
	var invalidRE = /[^0-9,\s]/;
	return !invalidRE.test(strAcGroup);
}

function checkForCharacters(inputString, checkString, startingIndex)
{
	if (!startingIndex)
	startingIndex = 0;
	return inputString.indexOf(checkString);
}

function jsIsValidRange(value, minRg, maxRg, strType)
{
	if((!strType || strType == "number") && typeof value == "string")
	{
		value = parseInt(value, 10);
	}
	return (value >= minRg && value <= maxRg);
}

function setBodyDivHeight(absoluteHeight, bMiniHeight)
{	
	var height = 560;
	
	if (typeof absoluteHeight != "undefined")
	{
		height = absoluteHeight;
	}
	
	if (!absoluteHeight || bMiniHeight)
	{
		var middleHeight = document.getElementById("body-middle").scrollHeight;
		var noteHeight = document.getElementById("note-body").scrollHeight + 66;
		var leftHeight = document.getElementById("body-left").scrollHeight;
		//var bIsIE8 = jsGetExploreVersion() == "ie8";
		//var middleHeight = bIsIE8 ? document.getElementById("body-middle").scrollHeight : document.getElementById("body-middle").offsetHeight;
		//var noteHeight = bIsIE8 ? document.getElementById("note-body").scrollHeight : document.getElementById("note-body").offsetHeight;
		//var leftHeight = bIsIE8 ? document.getElementById("note-body").scrollHeight : document.getElementById("body-left").offsetHeight;
		middleHeight = middleHeight > noteHeight ? middleHeight : noteHeight;
		middleHeight = middleHeight > leftHeight ? middleHeight : leftHeight;
		height = height > middleHeight ? height : middleHeight;
	}
	
	document.getElementById("body-left").style.height = "" + height + "px";
	document.getElementById("body-middle").style.height = "" + height + "px";
	document.getElementById("note-body").style.height = "" + (height - 66) + "px";
	if (document.getElementById("body-operator") != null)
	{
		document.getElementById("body-operator").style.height = "" + height + "px";
	}
}

function jsAddEvent(elem, evtType, func, capture)
{
	capture = (capture) ? capture : false;
	if( elem.addEventListener)
	{
		elem.addEventListener(evtType, func, capture);
	}
	else if(elem.attachEvent)
	{
		elem.attachEvent("on" + evtType, func);
	}
	else
	{
		elem["on"+evtType] = func;
	}
} 

function jsGetExploreVersion()
{
	var explorer = window.navigator.userAgent;
	if (document.all)
	{
		if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.match(/7./i)=="7.")
		{
			return "ie7";
		}
		else if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.match(/8./i)=="8.")
		{
			return "ie8";
		}
		else if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.match(/9./i)=="9.")
		{
			return "ie9";
		}
		else if(navigator.appName == "Microsoft Internet Explorer" && navigator.appVersion.match(/10./i)=="10.")
		{
			return "ie10";
		}
		else if(navigator.appName == "Microsoft Internet Explorer")
		{
			return "ie6";
		}
	}
	else
	{
		if (explorer.indexOf("Firefox") >= 0)
		{
			return "firefox";
		}
		else if (explorer.indexOf("Chrome") >= 0)
		{
			return "chrome";
		}
		else if(explorer.indexOf("Opera") >= 0)
		{
			return "Opera";
		}
		else if(explorer.indexOf("Safari") >= 0)
		{
			return "Safari";
		}
		else if(explorer.toUpperCase().indexOf("MAXTHON") >= 0)
		{
			return "Maxthon";
		}
		else
		{
			return "";
		}
	}
}

function jsTrim(stringToTrim) 
{
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function jsStrimBothWithspace(string)
{
	string = string.replace(/^\s+/g, "");
	string = string.replace(/\s+$/g, "");
	return string;
}

function jsIsValidSipString(value)
{
	var validRE = /[:;<>[\]?%]/;
	return !validRE.test(value);
}

/* 获取上传文件的名字，去除路径 */
function getUploadFileName(fileName)
{
	/* 将文件名截取出来(有些浏览器会将上传的文件名加上路径) */
	var arrBuffer = new Array();
	// 适用两种路径格式
	arrBuffer = fileName.split('\\');
	if (arrBuffer.length <= 0)
	{
		return "";
	}
	
	arrBuffer = arrBuffer[arrBuffer.length - 1].split('/');
	if (arrBuffer.length <= 0)
	{
		return "";
	}
	return arrBuffer[arrBuffer.length - 1];
}

/* 判断上传的文件是否为指定类型的文件, 不合法返回true */
function jsIsInvalidUploadFile(fileName, suffix)
{
	/* 将文件名截取出来(有些浏览器会将上传的文件名加上路径) */
	var name = getUploadFileName(fileName);
	
	// 获取文件名后缀
	var arrBuffer = new Array();
	arrBuffer = name.split('.');
	if (arrBuffer.length <= 0)
	{
		return true;
	}
	var tmpSuffix = arrBuffer[arrBuffer.length - 1];
	
	// 将后缀统一转换成大写作比较
	suffix = suffix.toUpperCase();
	tmpSuffix = tmpSuffix.toUpperCase();
	
	return !(tmpSuffix == suffix);
}

function jsAutoCorrecTime(objEdit, minValue, maxValue, defaultValue)
{
	if(objEdit.value.search(/^[0-9]+$/) != 0)
	{
		objEdit.value = (isNaN(defaultValue) ? objEdit.defaultValue : defaultValue);
	}
	else if(objEdit.value > maxValue)
	{
		objEdit.value = maxValue;
	}
	else if(objEdit.value < minValue)
	{
		objEdit.value = minValue;
	}
	
	if(objEdit.value.length == 1)
	{
		objEdit.value = "0" + objEdit.value;
	}
}

function isValidDsskeyValue(value)
{
	var regex = /[<>"]/;
	if(regex.test(value))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function isValidPickupCode(value)
{
	var regex = /[:;<>[\]?%^]/;
	if(regex.test(value))
	{
		return false;
	}
	else
	{
		return true;
	}
}

/* 对安全漏洞的检测,如果包含容易被JS攻击的字符则返回false */
function jsIsSafety(value)
{
	var regex = /[<>]/;
	if(regex.test(value))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function jsCheckSafety(obj)
{
	if(obj && !jsIsSafety(obj.value))
	{
		alert(TR("Input contains invalid character like '<>'"));
		jsFocusElement(obj);
		return false;
	}
	return true;
}

function isValidCode(obj)
{
	var regex = /[<>!]/;
	if (obj && regex.test(obj.value))
	{
		alert(TR("Input contains invalid character like ") + "'!<>'");
		jsFocusElement(obj);
		return false;
	}
	return true;
}

/* 判断输入是否为数字（浮点数也可） */
function jsCheckFloat(input)
{
	if (input.length == 0)
	{
		return true;
	}
	var re = /^[0-9]+.?[0-9]*$/;   //判断字符串是否为数字     //判断正整数 /^[1-9]+[0-9]*]*$/   
     if (!re.test(input))
    {
        return false;
     }
	return true;
}

function delSpecifiedOption(selObj, sValue)
{
	for(var j=0; j<selObj.options.length; j++)
	{
		if(selObj.options[j].value == sValue)
		{
			selObj.remove(j--);
			break;
		}
	}
}


// 上传文件成功的处理（供iframe页面调用）
function uploadSuccess()
{
	alert(TR("Upload success!"));
	onPageRefresh();
}

function uploadFailed(info)
{
	alert(TR("Upload failed!") + "\n" + TR(info));
	onPageRefresh();	
}

// Ajax请求的回调中去刷新页面，当前的location会有一个#，造成刷新不成功
function ajaxRefreshPage()
{
	var strLocation = ""+location;
	var length = strLocation.length;
	var chLast = strLocation.substr(length-1);
	if (length > 0 && '#' == chLast)
	{
		window.location = strLocation.substr(0, length - 1);
	}
	else
	{
		onPageRefresh();
	}
}

/* 翻译项通过innerHTML获取的值含有<script>T("...")</script> 所以在比较时，需要去除 */
function isEqualTranseInnerHtml(valueHTML, valueTR)
{
	if (typeof valueHTML == "undefined"
		|| typeof valueTR == "undefined")
	{
		return false;
	}
	
	var index = valueHTML.lastIndexOf("</script>");
	if (index != -1)
	{
		return (valueHTML.substring(index + 9) == valueTR);
	}
	
	return false;
}

function getQueryString(name)
{
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); 
	return null;
}
function replacePagename(p)
{
	if (p == "account-register" || p == "account-register-lync" || p == "account-basic" || p == "account-codec" || p == "account-adv")
	return "Account";
	if (p == "settings-preference" || p == "settings-datetime")
	return "Preference";
	if (p == "features-forward" || p == "features-general" || p == "features-audio" || p == "features-intercom" 
		|| p == "features-transfer" || p == "features-remotecontrl" || p == "features-callpickup" || p == "features-phonelock"
		|| p == "features-acd" || p == "features-bluetooth")
	return "Features";
	if (p == "settings-voice")
	return "Voice";
	if (p == "settings-autop" || p == "settings-config")
	return "Upgrade_Advanced";
	
	return p;
}

//'chr' joined  every the length 'len' set
function stringJoinChar(str, len, chr){
	var sResult = ""
	str=trim(str);
	var allLen = str.length;
	var pos=0;
	while(pos < allLen)
	{
		sResult += ( (pos+len < allLen) ? (str.substring(pos,pos+len) + chr) : str.substring(pos) );
		pos += len;
	}
	return sResult;
}
function divOverWord(words, len)
{
	words = trim(words);
	var reg = /([^\u4E00-\u9FA5\uFE30-\uFFA0\s])+/g ;
	return words.replace(reg, function(word) {
                           return stringJoinChar(word, len, ' ');
                         });
}

//create and display note tooltip
function setTooltip(xmldoc)
{
	//var xmldoc = XML.load("../note/english_note.xml");  // Read the XML data
	//alert(xmldoc);
	if (typeof xmldoc == "string") xmldoc = XML.parse(xmldoc);
	if(!xmldoc) return;
	var g_strPageName = getQueryString("p");
	g_strPageName =  replacePagename(g_strPageName);
	var strPageName = g_strPageName ? g_strPageName : 0;
	if(!strPageName) return;
	var xmlPageNote = xmldoc.getElementsByTagName(strPageName)[0];
	if(!xmlPageNote) return;
	var xmlnotes = xmlPageNote.getElementsByTagName("note");
	//alert(xmlnotes.length);
	// Loop through these elements. Each one contains a div which elem named by .
    for(var r=0; r < xmlnotes.length; r++) {
        // This is the XML element that holds the data
        var xmlnote = xmlnotes[r];
		if(!xmlnote.getAttribute("name")) continue;
		var noteId = xmlnote.getAttribute("name")+"-note";
		var objNode = document.getElementById(noteId);
		if(!objNode  || objNode.hasChildNodes()) continue;
		//get note content
		var noteContent = "";
		for(var c=0; c<xmlnote.childNodes.length; c++)
		{
			var rawData = xmlnote.childNodes[c];
			if(rawData.nodeType == 1 && rawData.firstChild)
			{
				var tmpStr = rawData.firstChild.data;
				var tagName = rawData.nodeName;
				if(tagName == "head"){
					noteContent += "<div class=\"note_head\">"+tmpStr+"</div>";
				}
				else if(tagName == "text"){
					noteContent += "<div class=\"note_text\">"+tmpStr+"</div>";
				}
				else if(tagName == "key"){
					noteContent += "<div class=\"note_key_content\">"+tmpStr+"</div>";
				}
			}
		}

		if(!noteContent)
		{
			if(!xmlnote.firstChild || isBlank(xmlnote.firstChild.data)) 
			{
				continue;
			}
			else 
			{
				noteContent = xmlnote.firstChild.data;   //compatible with no head/text tag
				objNode.style.cursor = "pointer";
			}
		}
		
		if(objNode.getAttribute("class") != "tooltip") 
			objNode.className = "tooltip"; 						 //objNode.setAttribute("class", "tooltip");
		objNode.setAttribute("tooltip",noteContent);
		if (g_IF_V73 || g_IF_V80)
		{
			objNode.onclick = function(evt) { evt = (evt) ? evt : ((window.event) ? event : null); Tooltip.schedule(this, evt); };
		}
		else
		{
			objNode.onmouseover = function(evt) { evt = (evt) ? evt : ((window.event) ? event : null); Tooltip.schedule(this, evt); };
		}
		
		var imgNode = document.createElement("div");
		//imgNode.setAttribute("src", "../img/help.png");//img
		imgNode.setAttribute("id", "imgPictureName");
		
		objNode.appendChild(imgNode);
    }
}

function HideTooltip(event)
{
	var objSrc = event.target ? event.target : event.srcElement;
	if (!IsChildElement(objSrc, Tooltip.tooltip.tooltip)
		&& objSrc != Tooltip.tooltip.src)
	{
		Tooltip.tooltip.hide();
	}
}

function IsChildElement(child, parent)
{
	if(!child || !parent) {return false;}
	parentElem = child;
	while (parentElem && parentElem != document.body)
	{
		if (parentElem == parent) {return true;}
		parentElem = parentElem.parentNode;
	}
	return false;
}

// 删除下拉框指定value的选项, 参数bReverse为逆序查找
function removeSelectItemByValue(objSelect, value, bReverse)
{
	if (typeof objSelect == "undefined"
		|| objSelect.nodeName.toUpperCase() != "SELECT"
		|| typeof value == "undefined")	
	{
		return;
	}
	
	var itemCount = objSelect.options.length;
	// 逆序
	if (typeof bReverse != "undefined"
		&& bReverse)
	{
		for (var i = itemCount - 1; i >= 0; i--)
		{
			if (objSelect.options[i].value == value)
			{
				objSelect.options.remove(i);
				break;
			}
		}
	}
	else
	{
		for (var i = 0; i < itemCount; i++)
		{
			if (objSelect.options[i].value == value)
			{
				objSelect.options.remove(i);
				break;
			}
		}
	}
}

function CheckNumber(object,num)
{
	if(!object)
	{
		return;
	}

    var txtinput = object.value;
	if(txtinput.length >num)
	{
		txtinput = txtinput.substring(0, num);
		object.value = txtinput;		
		
	}
}

function SetSelectValue(objSel, value, defValue)
{
	if (!objSel || typeof value == "undefined"
		|| objSel.length == 0 || objSel.nodeName.toUpperCase() != "SELECT")
		return;
	for (var i=0; i<objSel.length; i++)
	{
		if (objSel.options[i].value == value)
		{
			objSel.value = value;
			return;
		}
	}
	if (!defValue)
		objSel.selectedIndex = 0;
	else
		objSel.value = defValue;
}

function isValidAESPassword(strNumber)
{
	if (g_IF_V73 || g_IF_V80)
	{
		var validRE = /[^\w~@#$%^*[\]{},.:?=+-]/;
	}
	else
	{
		var validRE = /[^A-Za-z0-9]/;
	}
	return !validRE.test(strNumber);
}

function ShowCallNumber(obj,str,id)
{
	var objTips = document.getElementById(str);
	if (obj == null
		|| objTips == null)
	{
		return;
	}
    var objValueId = document.getElementById(id);
	if (objValueId == null) 
	{
		return;
	}
	var objValue = typeof document.getElementById(id).value == "undefined" ? document.getElementById(id).innerHTML : document.getElementById(id).value;
	if(objValue == "")
	{
		return;
	}
	
	var iLeft = obj.clientWidth + 5;
	var iTop = obj.clientHeight/2 - 10;
	while(obj)
	{
		iTop += obj.offsetTop;
		iLeft += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	objTips.style.top = iTop + "px";
	objTips.style.left = iLeft + "px";
	objTips.innerHTML = "call: " + objValue;
	objTips.style.display = "";
}
function HideCallNumber(str)
{
	var objTips = document.getElementById(str);
	if (objTips == null)	return;
	objTips.style.display = "none";
	objTips.innerHTML = "";
}



function jsTransInputButton()
{
	var arrInputs = document.getElementsByTagName("input");

	for(var i=0; i<arrInputs.length; i++) {
		if(arrInputs[i].getAttribute("type") == "button"
			|| arrInputs[i].getAttribute("type") == "reset"
			|| arrInputs[i].getAttribute("type") == "submit") {
			arrInputs[i].value = TR(arrInputs[i].value);
		}
	}
}

function SortFile(selId, selectedValue)
{
	var sel = document.getElementById(selId);
	if (!sel || sel.length <= 0)
		return;
	var filenames = new Array();
	for (var i=0; i<sel.length; i++)
	{
		filenames[i] = sel.options[i].value;
	}
	filenames.sort(function(a,b){
			var aNum = parseInt(a);
			var bNum = parseInt(b);
			if (!isNaN(aNum) && !isNaN(bNum))
				return aNum - bNum;
			else if (a < b)
				return -1;
			else if (a == b)
				return 0;
			else if (a > b)
				return 1;
		});
	selectedValue = selectedValue.replace(/^(\w+\/)?(.*)$/, "$2");
	selectedValue = selectedValue.replace(/^[0-9]+\.(.*)$/, "$1");
	sel.options.length = 0;
	var objfiles = {};
	for (var i=0; i<filenames.length; i++)
	{
		var val = filenames[i].replace(/^[0-9]+\.(.*)$/, "$1");
		if(objfiles[val]){continue;}
		else {objfiles[val] = val;}
		var opt = document.createElement('option');
		opt.value = val;
		opt.text = TR(opt.value);
		if (opt.value == selectedValue){opt.selected = true;}

		if(Sys.ie){sel.add(opt);}
		else{sel.add(opt, null);}
	}
}

function TransBuiltInResource(sel)
{
	if (!sel || sel.options.length <= 0)
		return;
	
	
	if (sel.options[0].value == "Common" || sel.options[0].value == "Auto")
	{
		sel.options[0].text = TR(sel.options[0].text);
	}
	
	for (var i=0; i<sel.length; i++)
	{
		if (sel.options[i].value.indexOf("Default:") == 0 || sel.options[i].value.indexOf("Resource:") == 0)
		{
			sel.options[i].text = TR(sel.options[i].text);
		}
	}
}

function showLoading()
{
	var coverlayer = document.getElementById("coverlayer");
	if (coverlayer)
	{
		coverlayer.style.display = "block";
	}
	/*
	var coverlayer = document.createElement("div");
	coverlayer.id = "coverlayer";
	coverlayer.className = "coverlayer";
	
	var loadingimg = document.createElement("img");
	loadingimg.src = "/img/loading.gif";
	loadingimg.style.width = "68px";
	loadingimg.style.height = "68px";
	loadingimg.style.position = "absolute";
	loadingimg.style.top = "50%";
	loadingimg.style.left = "50%";
	loadingimg.style.marginTop = "-34px";
	loadingimg.style.marginLeft = "-34px";
	coverlayer.appendChild(loadingimg);
	
	document.body.appendChild(coverlayer);
	*/
	if (jsGetExploreVersion() == "ie6")
	{
		var objSel = document.getElementById("group");
		if (objSel) {objSel.disabled = true;}
		objSel = document.getElementById("selectBlackPage");
		if (objSel) {objSel.disabled = true;}
	}
}

function removeLoading()
{
	var coverlayer = document.getElementById("coverlayer");
	if (coverlayer)
	{
		coverlayer.style.display = "none";
	}
	/*
	if (!coverlayer) {return;}
	document.body.removeChild(coverlayer);
	*/
	if (jsGetExploreVersion() == "ie6")
	{
		var objSel = document.getElementById("group");
		if (objSel) {objSel.disabled = false;}
		objSel = document.getElementById("selectBlackPage");
		if (objSel) {objSel.disabled = false;}
	}
}

function SetCheckboxStatus(objName, iIndex, bChecked)
{
	if (!objName || !iIndex) {return;}
	var obj = document.getElementsByName(objName)[iIndex];
	if (!obj) {return;}
	if (bChecked)
	{
		obj.checked = true;
	}
	else
	{
		obj.checked = false;
	}
}

function jsToggle(obj, bHide)
{
	if(typeof obj === "string") {obj = document.getElementById(obj);}
	if(!obj) {return;}
	if(!bHide)
	{
		var objParent = obj;
		while (objParent && objParent.tagName.toLowerCase() != "body")
		{
			var level = objParent.getAttribute("showlevel");
			if (level && level.search(/^2.*/) != -1) {
				return;
			}
			objParent = objParent.parentNode;
		}
		obj.style.display = "";
	}
	else
	{
		obj.style.display = "none";
	}
}

function jsAddOption(objSel, objOpt)
{
	if (!objSel || !objOpt) {return;}
	try
	{
		objSel.add(objOpt, null);
	}
	catch(e)
	{
		objSel.add(objOpt);
	}
}

function SyncConfirm(msg)
{
	if (g_ConfirmFlag)
	{
		return false;
	}
	else
	{
		g_ConfirmFlag = true;
	}
	var ret = window.confirm(msg);
	g_ConfirmFlag = false;
	return ret;
}

function CheckComplexity(value, similar)
{
	if (typeof value != "string") {return false;}
	if (value == "")
	{
		return !window.confirm(TR("Are you sure to set a blank password?"));
	}
	
	var rev = value.split("");
	rev.reverse();
	rev = rev.join("");
	if (value == similar
		|| rev == similar)
	{
		return !window.confirm(TR("Password is similar to username, are you sure to set it?"));
	}
	
	var regChar = /[a-zA-Z]/;
	var hasChar = regChar.test(value);
	var regDig = /[0-9]/;
	var hasDig = regDig.test(value);
	if (value.length < 6 || !hasChar || !hasDig)
	{
		return !window.confirm(TR("Password is too simple, are you sure to set it?"));
	}
	return false;
}
function jsCorrectDefaultTimeFloat(obj)
{
	if (typeof obj == "undefined") return;
	
	var value = obj.value;
	var length = value.length;
	var dotNum = 0;
	var chValue;
	for (var i = 0; i < length; i++)
	{
		chValue = value.substr(i,1);
		if (chValue == ".")
		{
			dotNum++;
		}
		if ((chValue != "."
			&& (chValue > "9" || chValue < "0"))
			|| dotNum > 1)
		{
			obj.value = obj.defaultValue;
			return;
		}
		
		
	}
	
	if (length > 1
		&& value.substr(0,1) == '0')
	{
		var index = 1;
		for (; index < value.length; index++)
		{
			if (value.substr(index,1) != '0')
			{
				break;
			}
		}
		
		value = (value.substr(index,1) == '.') ? value.substr(index-1) : value.substr(index);
	}
	value = (value.substr(0,1) == '.') ? ('0' + value) : value;
	obj.value = (value.substr(value.length - 1,1) == '.') ? (value + '0') : value;
}

function jsSetInputValue(id, val)
{
	var obj = document.getElementById(id);
	if(!obj) {return;}
	obj.value = val;
}

function jsSetSelectValue(id, value, defValue)
{
	var objSel = document.getElementById(id);
	if(!objSel) {return;}
	for (var i=0; i<objSel.length; i++)
	{
		if (objSel.options[i].value == value)
		{
			objSel.value = value;
			return;
		}
	}
	if (!defValue)
		objSel.selectedIndex = 0;
	else
		objSel.value = defValue;
}

function weblog(text)
{
	var obj = document.getElementById("hiddenWeblog");
	if (obj == null) {
		return;
	}
	obj.value += text + "; ";
}

function $(id)
{
	return document.getElementById(id);
}

function RemoveNode(e) {
	if(typeof e === "string") {e = document.getElementById(e);}
	if(!e) {return false;}
	var parent = e.parentNode;
	if(!parent) {return false;}
	parent.removeChild(e);
	return true;
}

function DoExport(action)
{
	document.formExport.action = action;
	document.formExport.submit();
}

function UpdateToken(token)
{
	var forms = document.getElementsByTagName("form");
	for(i=0; i<forms.length; i++) {
		var e = document.createElement("input");
		e.name = "token";
		e.value = token;
		e.type = "hidden";
		forms[i].appendChild(e);
	} 
}
