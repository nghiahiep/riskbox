﻿function Ajax() {
	// properties
	var reqList = [];
	// methods
	this.getXmlHttp = function() {
		var xmlHttp = false;
		var arrSignatures = [ "MSXML2.XMLHTTP.5.0", "MSXML2.XMLHTTP.4.0",
				"MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP" ];
		for ( var i = 0; i < arrSignatures.length; i++) {
			try {
				xmlHttp = new ActiveXObject(arrSignatures[i]);
				return xmlHttp;
			} catch (oError) {
				xmlHttp = false; // ignore
			}
		}
		// throw new Error("MSXML is not installed on your system.");
		if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
			xmlHttp = new XMLHttpRequest();
		}
		return xmlHttp;
	}
	this.clearReqList = function() {
		var len = reqList.length ;
		for ( var i = 0; i < len; i++) {
			var req = reqList[i];
			if (req) {
				try {
					delete req;
				} catch (e) {}
			}
		}
		reqList = [];
	}
	this.send = function(url, method, callback, data, urlencoded,isClear,cached,callBackError) {
		if(isClear==undefined){
			isClear = true;
		}
		if(cached == undefined){
			isCached = false;
		}
		if (isClear) {
			this.clearReqList();
		}
		
		var req = this.getXmlHttp();
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				// success
				if (req.status == 200) { // req.status < 400
					if (callback)
						callback(req, req.responseText);
				}else {
					if (callBackError)
						callBackError(req, req.responseText);
				}
				// release resource
				try {
					delete req;
					req = null;
				} catch (e) {}
			}
		}

		// POST request
		if (method.toUpperCase() == "POST") {
			req.open("POST", url, true);
			// 
			if (cached)
				req.setRequestHeader("If-Modified-Since", "0");
			// url encode data
			if (urlencoded)
				req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			req.send(data);
			reqList.push(req);
		}
		// GET request
		else {
			req.open("GET", url, true);
			// is cache
			if (cached)
				req.setRequestHeader("If-Modified-Since", "0");
			req.send(null);
			reqList.push(req);
		}
		return req;
	}//end send function
	
	this.get = function(url, callback,callBackError,isClear,isCached) {
		this.send(url, "GET", callback, null, false,isClear,isCached, callBackError);
		//(url, method, callback, data, urlencoded, cached,callBackError)
	}
	this.post=function(url, data, callback, callBackError,isClear, isCached){
		this.send(url, "POST", callback, data, true,isClear, isCached, callBackError);
	}
	
	this.get_E = function(url, callback,callBackError,isClear,isCached) {
		var num = Math.random();
		url += "&random=" + num;
		this.send(url, "GET", callback, null, false,isClear,isCached, callBackError);
		//(url, method, callback, data, urlencoded, cached,callBackError)
	}

}