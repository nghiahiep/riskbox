from django import forms

class fi_extract_request_form(forms.Form):
    request_form_date = forms.CharField(max_length=250, help_text='Format: dd/mm/yyyy')
    request_form_hour = forms.CharField(max_length=250, help_text='Format: hh:mm:ss')
    request_to_date = forms.CharField(max_length=250, help_text='Format: dd/mm/yyyy')
    request_to_hour = forms.CharField(max_length=250, help_text='Format: hh:mm:ss')