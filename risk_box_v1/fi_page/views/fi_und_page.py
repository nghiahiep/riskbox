# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect


from django.shortcuts import render


from django.contrib.auth.models import User


from dedupe_page.models import dedupe_id_model, dedupe_mobile_cux_model, dedupe_mobile_ref_model
from dedupe_page.models import black_list_id_history_model


from checklist.models import record_datachecker_result, record_datachecker_pending_defer_history
from checklist.models import record_und_newest_note_for_company, record_und_note_history_for_company
from checklist.models import record_phv_sending_fiv_request


from dedupe_page.models import black_list_on_id_dedupe_model, black_list_on_mafc_employee_dedupe_model


import re

from django.urls import reverse
from django.core import serializers

import datetime
import json
import xlsxwriter
import xlwt

from io import BytesIO

from django.db.models import Max
from django import db

from django.utils import timezone

from django.template import loader
from django.http import JsonResponse





# Có 2 def kiểm tra loại user là page và get_app_data_from_finnone_for_dedupe_and_blacklist

def page(request):

    if request.user.is_authenticated:

        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
        #1 là DC, 21 là UW POS
        if 1 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        elif "FI_UND" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))


    else:
        return HttpResponseRedirect(reverse("login_page"))

    import datetime
    from fi_page.forms import fi_extract_request_form


    form = fi_extract_request_form(request.POST)

    request_from_date = request.POST.get('request_form_date')
    request_from_hour = request.POST.get('request_form_hour')

    request_to_date = request.POST.get('request_to_date')
    request_to_hour = request.POST.get('request_to_hour')

    if request_from_date != None and request_from_hour != None and request_to_date !=None and request_to_hour != None:
        request_from_datetime = str(request_from_date) + str (" ") + str (request_from_hour)
        request_from_datetime_type = datetime.datetime.strptime(request_from_datetime, "%d/%m/%Y %H:%M:%S")


        request_to_datetime = str(request_to_date) + str (" ") + str (request_to_hour)
        request_to_datetime_type = datetime.datetime.strptime(request_to_datetime, "%d/%m/%Y %H:%M:%S")

        


    if 'extract fi request' in request.POST:

        now = datetime.datetime.now()

        output = BytesIO()
        # Feed a buffer to workbook
        workbook = xlsxwriter.Workbook(output)
        worksheet = workbook.add_worksheet("Request Field %d_%d_%d %d_%d" % (now.year, now.month, now.day, now.hour, now.minute))
        worksheet.set_column(1, 31, 32)

        from checklist.models import record_phv_sending_fiv_request
        from django.db.models import Q
        fi_request = record_phv_sending_fiv_request.objects.all().values_list(  'app_id',
                                                                                'house_number' ,
                                                                                'ward3',
                                                                                'dictrict3' ,
                                                                                'city3' ,
                                                                                'type_of_address' ,
                                                                                'pending_date' ,
                                                                                'date_request' ,
                                                                                'requested_by' ,
                                                                                'loan_amount' ,
                                                                                'client_name' ,
                                                                                'year_of_birth' ,
                                                                                'id_card' ,
                                                                                'gender' ,
                                                                                'marial_status' ,
                                                                                'spouse_name' ,
                                                                                'spouse_company' ,
                                                                                'client_phone_number' ,
                                                                                'spouse_phone_number' ,
                                                                                'owning_status' ,
                                                                                'householder' ,
                                                                                'relationship' ,
                                                                                'householder1' ,
                                                                                'relationship1' ,
                                                                                'room_number' ,
                                                                                'company_name' ,
                                                                                'partner_other' ,
                                                                                'describe_the_ways' ,
                                                                                'note' ,
                                                                                'send_fiv' ,
                                                                                'update_date' ).filter(date_request__range=[request_from_datetime_type, request_to_datetime_type], send_fiv = 'true')



        bold = workbook.add_format({'bold': True})


        datetime_format = workbook.add_format()
        datetime_format.set_num_format('dd/mm/yyyy hh:mm:ss')


        columns = [     'app_id',
                        'house_number' ,
                        'ward3',
                        'dictrict3' ,
                        'city3' ,
                        'type_of_address' ,
                        'pending_date' ,
                        'date_request' ,
                        'requested_by' ,
                        'loan_amount' ,
                        'client_name' ,
                        'year_of_birth' ,
                        'id_card' ,
                        'gender' ,
                        'marial_status' ,
                        'spouse_name' ,
                        'spouse_company' ,
                        'client_phone_number' ,
                        'spouse_phone_number' ,
                        'owning_status' ,
                        'householder' ,
                        'relationship' ,
                        'householder1' ,
                        'relationship1' ,
                        'room_number' ,
                        'company_name' ,
                        'partner_other' ,
                        'describe_the_ways' ,
                        'note' ,
                        'send_fiv' ,
                        'update_date']


        # Fill first row with columns
        row = 0
        for i,elem in enumerate(columns):
            worksheet.write(row, i, elem, bold)
        row += 1


        # Now fill other rows with columns
        for request in fi_request:
            worksheet.write(row, 0, request[0])
            worksheet.write(row, 1, request[1])
            worksheet.write(row, 2, request[2])
            worksheet.write(row, 3, request[3])
            worksheet.write(row, 4, request[4])
            worksheet.write(row, 5, request[5])
            worksheet.write(row, 6, request[6], datetime_format)
            worksheet.write(row, 7, request[7], datetime_format)
            worksheet.write(row, 8, request[8])
            worksheet.write(row, 9, request[9])
            worksheet.write(row, 10, request[10])
            worksheet.write(row, 11, request[11])
            worksheet.write(row, 12, request[12])
            worksheet.write(row, 13, request[13])
            worksheet.write(row, 14, request[14])
            worksheet.write(row, 15, request[15])
            worksheet.write(row, 16, request[16])
            worksheet.write(row, 17, request[17])
            worksheet.write(row, 18, request[18])
            worksheet.write(row, 19, request[19])
            worksheet.write(row, 20, request[20])
            worksheet.write(row, 21, request[21])
            worksheet.write(row, 22, request[22])
            worksheet.write(row, 23, request[23])
            worksheet.write(row, 24, request[24])
            worksheet.write(row, 25, request[25])
            worksheet.write(row, 26, request[26])
            worksheet.write(row, 27, request[27])
            worksheet.write(row, 28, request[28])
            worksheet.write(row, 29, request[29])
            worksheet.write(row, 30, request[30], datetime_format)
            row += 1

        # Close workbook for building file
        workbook.close()
        output.seek(0)
        response = HttpResponse(output.read(), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")

        return response


    
    return render(request, './en/fi_und/fi_und_index.html',{  "form":form, })




# def export_fi_request_to_xls(request):
#     response = HttpResponse(content_type='application/ms-excel')
#     response['Content-Disposition'] = 'attachment; filename="users.xls"'

#     wb = xlwt.Workbook(encoding='utf-8')
#     ws = wb.add_sheet('Users')

#     # Sheet header, first row
#     row_num = 0

#     font_style = xlwt.XFStyle()
#     font_style.font.bold = True

#     columns = ['Username', 'First name', 'Last name', 'Email address', ]

#     for col_num in range(len(columns)):
#         ws.write(row_num, col_num, columns[col_num], font_style)

#     # Sheet body, remaining rows
#     font_style = xlwt.XFStyle()

#     rows = User.objects.all().values_list('username', 'first_name', 'last_name', 'email')
#     for row in rows:
#         row_num += 1
#         for col_num in range(len(row)):
#             ws.write(row_num, col_num, row[col_num], font_style)

#     wb.save(response)
#     return response



def export_fi_request_to_xls(request):

    pass