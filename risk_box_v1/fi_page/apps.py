from django.apps import AppConfig


class FiPageConfig(AppConfig):
    name = 'fi_page'
