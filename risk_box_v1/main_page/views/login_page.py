# - * - Coding: utf -8 - * -
from django.shortcuts import render
from django.shortcuts import render_to_response

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login

from django.contrib.auth.signals import user_logged_in

from django.utils import timezone

from checklist.models import LoginUpdate

from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect

# Create your views here.
#def page(request):
#    my_variable = "Hello!"
#    years_old = 2017
#    array_city_capitale = [ "Paris", "London", "Washington" ]
#    return render(request, './en/index.html', {     "my_var":my_variable, 
#                                                    "years":years_old, 
#                                                    "array_city":array_city_capitale }) 


def update_user_login(sender, request, user, **kwargs):
    #user = kwargs.pop('user', None)
    LoginUpdate.objects.create( date_updated = timezone.now(),action_type="Login", action_user=user, action_username=user.username)




@csrf_protect
def page(request):

	logout(request)

	if request.method == "POST":
		domainuser = request.POST.get('inputDomainName')
		password = request.POST.get('inputPassword')
		user = authenticate(username=domainuser, password=password)

		if user:
			login(request,user)
			user_groups = request.user.groups.values_list('name', flat=True).order_by('name')
			user_groups_id = request.user.groups.values_list('id', flat=True)


			#1 là PL DC, 2 là PL PHV, 22 là PL UND, 21 là UW POS
			if request.user.is_superuser:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("admin:index"))

			elif 1 in user_groups_id:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_dc_page"))

			elif 2 in user_groups_id:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_phv_page"))

			elif 22 in user_groups_id:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_und_page"))

			elif 21 in user_groups_id:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_dc_page"))

			elif "SUPER_ADMIN" in user_groups:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_dc_page"))

			elif "UW_SUP_TL" in user_groups:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("checklist_und_page"))

			elif "FI_UND" in user_groups:
				user_logged_in.connect(update_user_login)
				return HttpResponseRedirect(reverse("fi_und_page"))

			else:

				raise Http404("You do not have permission to access anything")




		else:
			error = " Sorry! Domain and Password didn't match, Please try again ! "
			return render(request, 'en/index.html',{'error':error})
	else:
		return render(request, 'en/index.html')

	return render_to_response ('./en/index.html', { 'username': request.user.username, 
													'user_groups': user_groups,
													})  