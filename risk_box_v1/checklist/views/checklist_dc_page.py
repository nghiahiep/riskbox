# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

from dedupe_page.models import dedupe_id_model, dedupe_mobile_cux_model, dedupe_mobile_ref_model
from dedupe_page.models import black_list_id_history_model

from checklist.models import record_datachecker_result, record_datachecker_pending_defer_history
from checklist.models import record_und_newest_note_for_company, record_und_note_history_for_company

from dedupe_page.models import black_list_on_id_dedupe_model, black_list_on_mafc_employee_dedupe_model

import re

from django.urls import reverse
from django.core import serializers

import datetime

import json

from django.db.models import Max
from django import db

from django.utils import timezone

from django.template import loader
from django.http import JsonResponse

from django.views.generic.base import View



# Có 2 def kiểm tra loại user là page và get_app_data_from_finnone_for_dedupe_and_blacklist

def page(request):

    if request.user.is_authenticated:

        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
        #1 là DC, 21 là UW POS
        if 1 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))


    else:
        return HttpResponseRedirect(reverse("login_page"))



    #if request.user.is_authenticated():
    #    user_groups = request.user.groups.values_list('name', flat=True)
    #    if "Data Checker" in user_groups or "UW_POS" in user_groups:
    #        pass
            #return HttpResponseRedirect(reverse("login_page"))
    #    else:
    #        return HttpResponseRedirect(reverse("login_page"))
    #else:
    #    return HttpResponseRedirect(reverse("login_page"))
    

    #if request.method == 'GET' and 'search_app_id' in request.GET:

    query001 = request.GET.get('search_app_id')

    query002 = request.GET.get('search_customer_name')
    query003 = request.GET.get('search_customer_dob')
    query004 = request.GET.get('search_customer_national_id')
    query005 = request.GET.get('search_spouse_customer_name')
    query006 = request.GET.get('search_customer_spouse_national_id')




    action = request.GET.get('button_get_app')

    #bellow is button for seaching dedupe
    #cux search
    action1 = request.GET.get('button_get_app_info')
    #spouse search
    action2 = request.GET.get('button_get_spouse_info')
    #id search
    action3 = request.GET.get('button_get_info_dc_style')


    if query001 is not None:
        query01 = query001.upper()
    else:
        query01 = query001

    if query002 is not None:
        query02 = query002.upper()
        query012 = re.sub('[!@#$%]', '', query02)
    else:
        query02 = query002

    if query003 is not None:
        query03 = query003.upper()
        query013 = re.sub('[!@#$%]', '', query03)
    else:
        query03 = query003

    if query004 is not None:
        query04 = query004.upper()
        query014 = re.sub('[!@#$%]', '', query04)
    else:
        query04 = query004

    if query005 is not None:
        query05 = query005.upper()
        query015 = re.sub('[!@#$%]', '', query05)
    else:
        query05 = query005

    if query006 is not None:
        query06 = query006.upper()
        query016 = re.sub('[!@#$%]', '', query06)
    else:
        query06 = query006
    

    if action and query001:
        #queryset_list  = dedupe_id_model.objects.filter(app_id=query01)
        oracle_sql = ("SELECT 1 as id, %s as APP_ID_C, B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                      "               B.SPOUSENAME, B.SPOUSE_ID_C" + 
                      "   FROM RISK_FRAUD_TAKE_CUS_IN_PROD B WHERE B.CUST_ID_N IN " +
                      "  (SELECT A.CUST_ID_N FROM RISK_FRAUD_TAKE_CUS_ID_PROD A WHERE A.APP_ID_C = %s )")

        oracle_sql1 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUSTOMER_ID(%s)) AS BLACK_ID FROM DUAL")

        oracle_sql2 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUX_MOBILEPHONE(%s)) AS BLACK_PHONE FROM DUAL")

        oracle_sql3 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_REF_MOBILEPHONE(%s)) AS BLACK_REF_PHONE FROM DUAL")

        oracle_sql4 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_DSA(%s)) AS BLACK_DSA FROM DUAL")

        oracle_sql5 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_EMPLOYEE_ID(%s)) AS BLACK_MAFC_ID FROM DUAL")

        oracle_sql6 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_CUX_MOBI(%s)) AS BLACK_MAFC_CUX_MOBILE FROM DUAL")

        oracle_sql7 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_REF_MOBI(%s)) AS BLACK_MAFC_REF_MOBILE FROM DUAL")


        queryset_list  = dedupe_id_model.objects.raw(oracle_sql,[query001, query001])

        queryset_list1  = dedupe_id_model.objects.raw(oracle_sql1,[query001])

        queryset_list2  = dedupe_id_model.objects.raw(oracle_sql2,[query001])

        queryset_list3  = dedupe_id_model.objects.raw(oracle_sql3,[query001])

        queryset_list4  = dedupe_id_model.objects.raw(oracle_sql4,[query001])

        queryset_list5  = dedupe_id_model.objects.raw(oracle_sql5,[query001])

        queryset_list6  = dedupe_id_model.objects.raw(oracle_sql6,[query001])

        queryset_list7  = dedupe_id_model.objects.raw(oracle_sql7,[query001])


        posts = record_datachecker_result.objects.filter(app_id=query001)


        each_result1 = 0

        for each_result1_1 in queryset_list1:
            if each_result1_1.black_id is not None:
                each_result1 = each_result1_1.black_id
            else:
                each_result1 = 0


        each_result2 = 0

        for each_result2_1 in queryset_list2:
            if each_result2_1.black_phone is not None:
                each_result2 = each_result2_1.black_phone
            else:
                each_result2 = 0


        each_result3 = 0

        for each_result3_1 in queryset_list3:
            if each_result3_1.black_ref_phone is not None:
                each_result3 = each_result3_1.black_ref_phone
            else:
                each_result3 = 0


        each_result4 = 0

        for each_result4_1 in queryset_list4:
            if each_result4_1.black_dsa is not None:
                each_result4 = each_result4_1.black_dsa
            else:
                each_result4 = 0



        each_result5 = 0

        for each_result5_1 in queryset_list5:
            if each_result5_1.black_mafc_id is not None:
                each_result5 = each_result5_1.black_mafc_id
            else:
                each_result5 = 0



        each_result6 = 0

        for each_result6_1 in queryset_list6:
            if each_result6_1.black_mafc_cux_mobile is not None:
                each_result6 = each_result6_1.black_mafc_cux_mobile
            else:
                each_result6 = 0



        each_result7 = 0

        for each_result7_1 in queryset_list7:
            if each_result7_1.black_mafc_ref_mobile is not None:
                each_result7 = each_result7_1.black_mafc_ref_mobile
            else:
                each_result7 = 0
                

        final_result =  str(each_result1)+ str(each_result2) + str (each_result3) + str(each_result4) + str (each_result5) + str(each_result6) + str (each_result7)   


        final_result_r = 'WHITE'
        if (each_result1 + each_result2 + each_result3 + each_result4 + each_result5 + each_result6 + each_result7)>0 :
            final_result_r = 'BLACK'



        final_result_save = black_list_id_history_model.objects.create(app_id_c=query001, result_date = timezone.now(), result_detail=final_result, result_final=final_result_r, user_check = request.user)



        context = {     
                    "basic_app_infor": queryset_list,
                    "basic_app_infor1": queryset_list1,
                    "basic_app_infor2": queryset_list2,
                    "basic_app_infor3": queryset_list3,
                    "basic_app_infor4": queryset_list4,
                    "basic_app_infor5": queryset_list5,
                    "basic_app_infor6": queryset_list6,
                    "basic_app_infor7": queryset_list7,
                    "black_list_result": final_result_r,
                    "button_get_app": action,
                    "get_app_id": query001,
                    "posts" : posts,
                    }

        return render(request, './en/checklist/checklist_dc.html', context)    



    elif action1 and query02 and query03 and query04:

        digit = 0

        for i in query04:
            if i.isnumeric():
                digit+=1

        if digit <9:
            report_error = "Please input correct ID No of customer!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                                " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                                " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                                " WHERE ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR (%s),'[[:space:]]*', '') " +
                                "       AND DOB = %s) OR (CUSTOMER_ID_NUMBER = %s ) OR (SPOUSE_ID = %s) " )

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query02, query03, query04, query04])

            context = { 
                    "object_list": queryset_list,
                    "button_value": action,
                    "button_value_1": action1
                    }


        return render(request, './en/checklist/checklist_dc.html', context)
        #data = serializers.serialize('json', queryset_list)
        #return HttpResponse(data, content_type="application/json")




    elif action2 and query05 and query06:

        digit = 0

        for i in query06:
            if i.isnumeric():
                digit+=1

        if digit <3:
            report_error = "Please input at least 3 digits!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                                " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                                " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                                " WHERE (( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(SPOUSE_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND SPOUSE_ID LIKE %s ))" +
                                "       OR ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND CUSTOMER_ID_NUMBER LIKE %s)")

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query05, query06, query05, query06])

            context = { 
                        "object_list": queryset_list,
                        "button_value": action,
                        "button_value_1": action2
                        }


        return render(request, './en/checklist/checklist_dc.html', context)


    elif action3:

        query04 = query04 + '%'

        digit = 0

        for i in query04:
            if i.isnumeric():
                digit+=1

        if digit <3:
            report_error = "Please input at least 5 digits!"
            context = { 
                    "report_error": report_error
                    }

        else:
            oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                    "           OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                    "   FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                    "   WHERE CUSTOMER_ID_NUMBER LIKE %s AND CUSTOMER_NAME LIKE %s AND DOB LIKE %s")

            queryset_list  = dedupe_id_model.objects.raw(oracle_sql, [query04, query02, query03])


            context = { 
                "object_list": queryset_list,
                "button_value": action,
                "button_value_1": action1,
                "button_value_3": action3,
                }


        return render(request, './en/checklist/checklist_dc.html', context)


    else:

        report_error = "Please input appid or required information for searching!"
        return render(request, './en/checklist/checklist_dc.html',{  "report_error":report_error, })








def get_app_data_from_finnone_for_dedupe_and_blacklist(request):

    if request.user.is_authenticated:

        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
                
        #1 là DC, 21 là UW POS
        if 1 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))


    else:
        return HttpResponseRedirect(reverse("login_page"))



    #if request.user.is_authenticated():
    #        user_groups = request.user.groups.values_list('name', flat=True)
    #        if "Data Checker" in user_groups or "UW_POS" in user_groups:
    #            pass
                #return HttpResponseRedirect(reverse("login_page"))
    #        else:
    #            return HttpResponseRedirect(reverse("login_page"))
    #else:
    #    return HttpResponseRedirect(reverse("login_page"))



    search_app_id = request.GET.get('app_id_c')
    

    oracle_sql = ("SELECT 1 as id, %s as APP_ID_C, B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                      "   B.CUSTOMERNAME AS CUSTOMERNAME_WO_MARK, B.SPOUSENAME, B.SPOUSE_ID_C, " + 
                      "   B.OTHER_EMPLOYER_NAME_C AS COMPANY_NAME, B.NCM_LABOUR_CARDNO_C AS TAX_CODE, B.CUST_ID_N, " +
                      "   C.LAA_PRODUCT_ID_C "
                      "   FROM MAMAS.NBFC_CUSTOMER_M B  " +
                      "   LEFT JOIN (SELECT A.CUST_ID_N, A.LAA_PRODUCT_ID_C FROM MACAS.LOS_APP_APPLICATIONS A) C ON C.CUST_ID_N = B.CUST_ID_N "
                      "   WHERE B.CUST_ID_N IN (SELECT A.CUST_ID_N FROM MACAS.LOS_APP_APPLICATIONS A WHERE A.APP_ID_C = TO_CHAR(%s) )")

    queryset_list  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql,[search_app_id, search_app_id])

    customer_id_number = ''
    customer_full_name = ''
    customer_cus_id = ''
    product_id=''
    for app_infor in queryset_list:
        customer_id_number = app_infor.cif_no
        customer_cus_id = app_infor.cust_id_n
        product_id = app_infor.laa_product_id_c
        customer_full_name = app_infor.customername_wo_mark



    #check black list id number
    #oracle_sql1 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUSTOMER_ID(%s)) AS BLACK_ID FROM DUAL")
    mysql_sql1 = ("SELECT 1 AS id, CAST(BLACK_LIST_CUSTOMER_ID(%s) AS UNSIGNED ) AS black_id")

    queryset_list1  = dedupe_id_model.objects.using('default').raw(mysql_sql1,[customer_id_number])



    #check black list cux mobile phone
    oracle_cux_mobile = ("SELECT B.id, B.mobile FROM ( " +
                         "SELECT 1 as id, A.MOBILE FROM MACAS.NBFC_ADDRESS_M A WHERE A.BPID = %s AND A.MOBILE IS NOT NULL " +
                         "UNION ALL " +
                         "SELECT 2 as id, A.PHONE1 FROM MACAS.NBFC_ADDRESS_M A WHERE A.BPID = %s AND A.PHONE1 IS NOT NULL " +
                         "UNION ALL " +
                         "SELECT 3 as id, A.PHONE2 FROM MACAS.NBFC_ADDRESS_M A WHERE A.BPID = %s AND A.PHONE2 IS NOT NULL " +
                         " )B "
                         )

    
    queryset_list0  = dedupe_mobile_cux_model.objects.using('finnonedb').raw(oracle_cux_mobile,[customer_cus_id, customer_cus_id, customer_cus_id])


    
    count_blacklist_cux_mobilephone = 0
    for f1_customer_mobile in queryset_list0:
        print (f1_customer_mobile.mobile)
        #oracle_sql2 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUX_MOBILEPHONE(%s)) AS BLACK_PHONE FROM DUAL")
        mysql_sql2 = ("SELECT 1 AS id, CAST(BLACK_LIST_CUX_MOBILEPHONE(%s) AS UNSIGNED ) AS black_phone")
        cux_mobile = f1_customer_mobile.mobile
        queryset_temp = dedupe_mobile_cux_model.objects.using('default').raw(mysql_sql2,[cux_mobile])
        for result in queryset_temp:
            count_blacklist_cux_mobilephone = count_blacklist_cux_mobilephone + result.black_phone

    





    #check black list ref mobile phone
    oracle_ref_mobile = ("SELECT B.id, B.mobile FROM ( " +
                         "SELECT 1 as id, A.MOBILE FROM MACAS.NBFC_REFERENCE_M A WHERE A.CUST_ID_N = %s AND A.MOBILE IS NOT NULL " +
                         "UNION ALL " +
                         "SELECT 2 as id, A.PHONE1 FROM MACAS.NBFC_REFERENCE_M A WHERE A.CUST_ID_N = %s AND A.PHONE1 IS NOT NULL " +
                         "UNION ALL " +
                         "SELECT 3 as id, A.PHONE2 FROM MACAS.NBFC_REFERENCE_M A WHERE A.CUST_ID_N = %s AND A.PHONE2 IS NOT NULL )B "
                         )

    queryset_list00  = dedupe_mobile_ref_model.objects.using('finnonedb').raw(oracle_ref_mobile,[customer_cus_id, customer_cus_id, customer_cus_id])

    count_blacklist_ref_mobilephone = 0
    for f1_ref_mobile in queryset_list00:
        
        #oracle_sql3 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_REF_MOBILEPHONE(%s)) AS BLACK_REF_PHONE FROM DUAL")
        mysql_sql3 = ("SELECT 1 AS id, CAST(BLACK_LIST_REF_MOBILEPHONE(%s) AS UNSIGNED ) AS black_ref_phone")
        ref_mobile = f1_ref_mobile.mobile
        queryset_temp = dedupe_mobile_ref_model.objects.using('default').raw(mysql_sql3,[cux_mobile])
        for result in queryset_temp:
            count_blacklist_ref_mobilephone = count_blacklist_ref_mobilephone + result.black_ref_phone








    #check black list DSA
    oracle_dsa_code = ("SELECT 1 as id, B.INSPECTORNAME FROM MACAS.LOS_APP_APPLICATIONS A, MAMAS.LEA_INSPECTOR_M B " + 
                       "WHERE A.LAA_SOURCE_INSPECTORID_N = B.INSPECTORID AND A.APP_ID_C = %s ")

    queryset_list000  = dedupe_id_model.objects.using('finnonedb').raw(oracle_dsa_code,[search_app_id])

    count_blacklist_dsa_code = 0
    for oracle_dsa_code in queryset_list000:
        print (oracle_dsa_code.inspectorname)
        #oracle_sql4 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_DSA(%s)) AS BLACK_DSA FROM DUAL")
        mysql_sql4 = ("SELECT 1 AS id, CAST(BLACK_LIST_DSA(%s) AS UNSIGNED ) AS black_dsa")
        code_dsa = oracle_dsa_code.inspectorname
        queryset_temp = dedupe_mobile_ref_model.objects.using('default').raw(mysql_sql4,[code_dsa])
        for result in queryset_temp:
            count_blacklist_dsa_code = count_blacklist_dsa_code + result.black_dsa





    #check black list MA Employee
    mysql_sql5 = ("SELECT 1 AS id, CAST(BLACK_LIST_MAFC_EMPLOYEE_ID(%s, %s) AS UNSIGNED ) AS black_mafc_id")

    queryset_list5  = dedupe_id_model.objects.using('default').raw(mysql_sql5,[product_id,customer_id_number])

    count_blacklist_mafc = 0
    for item in queryset_list5:
        print (item.black_mafc_id)
        count_blacklist_mafc = item.black_mafc_id







    #check black list customer in MA Employee list
    count_blacklist_mafc_cux_mobilephone = 0
    for f1_customer_mobile in queryset_list0:
        print (f1_customer_mobile.mobile)
        #oracle_sql6 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_CUX_MOBI(%s)) AS BLACK_MAFC_CUX_MOBILE FROM DUAL")
        mysql_sql6 = ("SELECT 1 AS id, CAST(BLACK_LIST_MAFC_CUX_MOBI(%s) AS UNSIGNED ) AS black_mafc_cux_mobile")
        cux_mobile = f1_customer_mobile.mobile
        queryset_temp = dedupe_mobile_cux_model.objects.using('default').raw(mysql_sql6,[cux_mobile])
        for result in queryset_temp:
            count_blacklist_mafc_cux_mobilephone = count_blacklist_cux_mobilephone + result.black_mafc_cux_mobile







    #check black list ref in MA Employee list
    count_blacklist_mafc_ref_mobilephone = 0
    for f1_ref_mobile in queryset_list00:
        #oracle_sql7 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_REF_MOBI(%s)) AS BLACK_MAFC_REF_MOBILE FROM DUAL")
        mysql_sql7 = ("SELECT 1 AS id, CAST(BLACK_LIST_MAFC_REF_MOBI(%s) AS UNSIGNED ) AS black_mafc_ref_mobile")
        ref_mobile = f1_ref_mobile.mobile
        queryset_temp = dedupe_mobile_ref_model.objects.using('default').raw(mysql_sql7,[ref_mobile])
        for result in queryset_temp:
            count_blacklist_mafc_ref_mobilephone = count_blacklist_mafc_ref_mobilephone + result.black_mafc_ref_mobile


    #oracle_sql2 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_CUX_MOBILEPHONE(%s)) AS BLACK_PHONE FROM DUAL")

    #oracle_sql3 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_REF_MOBILEPHONE(%s)) AS BLACK_REF_PHONE FROM DUAL")

    #oracle_sql4 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_DSA(%s)) AS BLACK_DSA FROM DUAL")

    #oracle_sql5 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_EMPLOYEE_ID(%s)) AS BLACK_MAFC_ID FROM DUAL")

    #oracle_sql6 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_CUX_MOBI(%s)) AS BLACK_MAFC_CUX_MOBILE FROM DUAL")

    #oracle_sql7 = ("SELECT 1 AS ID, TO_NUMBER (BLACK_LIST_MAFC_REF_MOBI(%s)) AS BLACK_MAFC_REF_MOBILE FROM DUAL")





    #queryset_list1  = dedupe_id_model.objects.using('datadb').raw(oracle_sql1,[search_app_id])

    #queryset_list2  = dedupe_id_model.objects.using('datadb').raw(oracle_sql2,[search_app_id])

    #queryset_list3  = dedupe_id_model.objects.using('datadb').raw(oracle_sql3,[search_app_id])

    #queryset_list4  = dedupe_id_model.objects.using('datadb').raw(oracle_sql4,[search_app_id])

    #queryset_list5  = dedupe_id_model.objects.using('datadb').raw(oracle_sql5,[search_app_id])

    #queryset_list6  = dedupe_id_model.objects.using('datadb').raw(oracle_sql6,[search_app_id])

    #queryset_list7  = dedupe_id_model.objects.using('datadb').raw(oracle_sql7,[search_app_id])

    posts = record_datachecker_result.objects.filter(app_id=search_app_id)

    
    #10JAN2017 Nguyen - use django check blacklist customer id
    each_result1 = 0

    for each_result1_1 in queryset_list1:
        if each_result1_1.black_id is not None:
            each_result1 = each_result1_1.black_id
        else:
            each_result1 = 0


    #10JAN2017 Nguyen - use django check blacklist customer mobile phone
    each_result2 = count_blacklist_cux_mobilephone

    #for each_result2_1 in queryset_list2:
    #    if each_result2_1.black_phone is not None:
    #        each_result2 = each_result2_1.black_phone
    #    else:
    #        each_result2 = 0


    #10JAN2017 Nguyen - use django check blacklist customer mobile phone
    each_result3 = count_blacklist_ref_mobilephone

    #for each_result3_1 in queryset_list3:
    #    if each_result3_1.black_ref_phone is not None:
    #        each_result3 = each_result3_1.black_ref_phone
    #    else:
    #        each_result3 = 0


    #10JAN2017 Nguyen - use django check blacklist DSA
    each_result4 = count_blacklist_dsa_code

    #for each_result4_1 in queryset_list4:
    #    if each_result4_1.black_dsa is not None:
    #        each_result4 = each_result4_1.black_dsa
    #    else:
    #        each_result4 = 0


    #10JAN2017 Nguyen - use django check blacklist MA
    each_result5 = count_blacklist_mafc

    #for each_result5_1 in queryset_list5:
    #    if each_result5_1.black_mafc_id is not None:
    #        each_result5 = each_result5_1.black_mafc_id
    #    else:
    #        each_result5 = 0



    each_result6 = count_blacklist_mafc_cux_mobilephone

    #for each_result6_1 in queryset_list6:
    #    if each_result6_1.black_mafc_cux_mobile is not None:
    #        each_result6 = each_result6_1.black_mafc_cux_mobile
    #    else:
    #        each_result6 = 0



    each_result7 = count_blacklist_mafc_ref_mobilephone

    #for each_result7_1 in queryset_list7:
    #    if each_result7_1.black_mafc_ref_mobile is not None:
    #        each_result7 = each_result7_1.black_mafc_ref_mobile
    #    else:
    #        each_result7 = 0
            

    final_result =  str(each_result1)+ str(each_result2) + str (each_result3) + str(each_result4) + str (each_result5) + str(each_result6) + str (each_result7)   


    final_result_r = 'WHITE'
    if (each_result1 + each_result2 + each_result3 + each_result4 + each_result5 + each_result6 + each_result7)>0 :
        final_result_r = 'BLACK'


    final_result_save = black_list_id_history_model.objects.create(app_id_c=search_app_id, result_date = timezone.now(), result_detail=final_result, result_final=final_result_r, user_check = request.user)

    #post_id_card_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='ID CARD').count()
    #posts_family_book_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='FAMILY BOOK').count()
    #posts_work_proof_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='WORK PROOF').count()
    #posts_income_proof_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='INCOME PROOF').count()
    #posts_current_address_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='CURRENT ADDRESS').count()
    #posts_cib_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='CIB').count()
    #posts_tax_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='TAX').count()
    #posts_dedupe_count = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='DUPE').count()

    posts_id_card = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='ID CARD').order_by('paper_type', '-date_created')
    posts_family_book = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
    posts_work_proof = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
    posts_income_proof = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
    posts_current_address = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
    posts_cib = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='CIB').order_by('paper_type', '-date_created')
    posts_tax = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='TAX').order_by('paper_type', '-date_created')
    posts_dedupe = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='DUPE').order_by('paper_type', '-date_created')
    posts_defer_note = record_datachecker_result.objects.filter(app_id=search_app_id, paper_type='DEFER').order_by('paper_type', '-date_created')

    dc_result_table = {
                        "posts":posts,
                        "posts_id_card":posts_id_card,
                        "posts_family_book":posts_family_book,
                        "posts_work_proof":posts_work_proof,
                        "posts_income_proof":posts_income_proof,
                        "posts_current_address":posts_current_address,
                        "posts_cib":posts_cib,
                        "posts_tax":posts_tax,
                        "posts_dedupe":posts_dedupe,
                        "posts_defer_note":posts_defer_note,
                        }

    checklist_result_html = loader.render_to_string('./en/checklist/module_show_data_checker_result.html', dc_result_table)





    save_complete = True

    if len(search_app_id) >=5:
        button_get_app = True
    else:
        button_get_app=''




    mysql_name_remove_vn_mark = ("SELECT 1 AS id, %s as app_id, risk_remove_vn_mark_char(%s) as customername_wo_mark")
    queryset_name_remove_vn_mark = dedupe_id_model.objects.using('default').raw(mysql_name_remove_vn_mark,[search_app_id, customer_full_name])



    context = {     
                "basic_app_infor": queryset_list,
                "basic_app_infor1": queryset_list1,
                "basic_app_infor2": count_blacklist_cux_mobilephone,
                "basic_app_infor3": count_blacklist_ref_mobilephone,
                "basic_app_infor4": count_blacklist_dsa_code,
                "basic_app_infor5": count_blacklist_mafc,
                "basic_app_infor6": count_blacklist_mafc_cux_mobilephone,
                "basic_app_infor7": count_blacklist_mafc_ref_mobilephone,
                "black_list_result": final_result_r,
                "get_app_id": search_app_id,
                "button_get_app": button_get_app,
                "queryset_name_remove_vn_mark": queryset_name_remove_vn_mark,
                "posts" : posts,
                }


     # build a html posts list with the paginated posts
    posts_html = loader.render_to_string('en/blacklist/module_blacklist.html', context)

    posts_html1 = loader.render_to_string('en/stage_dc/module_dedupeid_search_items_cux.html', context)

    posts_html2 = loader.render_to_string('en/stage_dc/module_dedupeid_search_items_spouse.html', context)

    # load module này chủ yếu chỉ để hiển thị giao diện, không có giá trị
    posts_html3 = loader.render_to_string('en/stage_dc/module_dedupeid_search_result.html', context)

    posts_html4 = loader.render_to_string('en/checklist/module_tab_data_checker_cl.html', context)

    posts_html5 = loader.render_to_string('en/checklist/module_show_home_doc_link.html', context)


    # phần này nhằm lấy lại trạng thái cuối cùng của app do DC note
    dc_note_status = record_datachecker_pending_defer_history.objects.filter(app_id=search_app_id).order_by('-date_created')[:1]

    dc_final_note_status = {
                            "dc_note_status":dc_note_status,
                            }

    dc_final_note_status_html = loader.render_to_string('./en/checklist/module_gui_dccl_dc_note_status.html', dc_final_note_status)


    #check_dc_note_exist = dc_note_status.count()





    #phần này là giải thuật quyết định khi nào lấy data của f1 khi nào lấy data của risk box cho mục thông tin công ty
    v_f1taxcode = ''
    for tax in queryset_list:
        v_f1taxcode = tax.tax_code
        
    company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = v_f1taxcode)

    dc_final_note_company = {
                "riskox_company_infor":company_data_in_risk_box,
                "basic_app_infor": queryset_list,
                }


    if company_data_in_risk_box.exists():

        posts_html8 = loader.render_to_string('./en/checklist/module_gui_show_company_info_update.html', dc_final_note_company)

    else:

        posts_html8 = loader.render_to_string('./en/checklist/module_gui_show_company_info.html', context)





    # package output data and return it as a JSON object
    output_data = { 'posts_html': posts_html,
                    'posts_html1': posts_html1,
                    'posts_html2': posts_html2,
                    'posts_html3': posts_html3,
                    'posts_html4': posts_html4,
                    'posts_html5' : posts_html5,
                    'posts_html8' : posts_html8,
                    'save_complete': save_complete,
                    'checklist_result_html': checklist_result_html,
                    'dc_final_note_status_html' : dc_final_note_status_html}

    return JsonResponse(output_data)











def search_customer_information_with_full_information_id_dob_name(request):

    query02 = request.GET.get('customer_name')
    query03 = request.GET.get('customer_dob')
    query04 = request.GET.get('customer_id')


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <9:
        report_error = "Please input correct ID No of customer!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR (%s),'[[:space:]]*', '') " +
                            "       AND DOB = %s) OR (CUSTOMER_ID_NUMBER = %s ) OR (SPOUSE_ID = %s) " )

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query02, query03, query04, query04])

        context = { 
                "object_list": queryset_list
                }


    save_complete = True

    # build a html posts list
    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)


    # package output data and return it as a JSON object
    output_data = {'posts_html': posts_html,'save_complete': save_complete}
    
    return JsonResponse(output_data)






def search_customer_information_with_home_directory_style(request):

    query002 = request.GET.get('customer_name')
    query003 = request.GET.get('customer_dob')
    query004 = request.GET.get('customer_id')

    

    if query002 is not None:
        query02 = query002.upper()
        query012 = re.sub('[!@#$%]', '', query02)
    else:
        query02 = query002

    if query003 is not None:
        query03 = query003.upper()
        query013 = re.sub('[!@#$%]', '', query03)
    else:
        query03 = query003

    if query004 is not None:
        query04 = query004.upper()
        query014 = re.sub('[!@#$%]', '', query04)
    else:
        query04 = query004


    if query04 is None:
        query04 = '%'
    else:        
        query04 = query04 + '%'


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 5 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                "           OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                "   FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                "   WHERE CUSTOMER_ID_NUMBER LIKE %s AND CUSTOMER_NAME LIKE %s AND DOB LIKE %s")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query04, query02, query03])


        context = { 
            "object_list": queryset_list,
            }



    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)







def search_customer_spouse_information_with_free_style(request):

    query005 = request.GET.get('spouse_name')
    query006 = request.GET.get('spouse_id')


    if query005 is not None:
        query05 = query005.upper()
        query015 = re.sub('[!@#$%]', '', query05)
    else:
        query05 = query005

    if query006 is not None:
        query06 = query006.upper()
        query016 = re.sub('[!@#$%]', '', query06)
    else:
        query06 = query006



    digit = 0

    if query06 is None:
        query06 = '%'


    for i in query06:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 3 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE (( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(SPOUSE_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND SPOUSE_ID LIKE %s ))" +
                            "       OR ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND CUSTOMER_ID_NUMBER LIKE %s)")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query05, query06, query05, query06])

        context = { 
                    "object_list": queryset_list,
                    }


    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)







def dc_cl_super_total_comment(request):

    app_id_c = request.POST.get('app_id_c')

    #app_id_c = app_id_c.replace(" ", "")

    #app_id_c = app_id_c.split("\n",1)[1]

    if app_id_c != None:
        print(app_id_c)
        app_id_c = app_id_c.strip()
    else:
        app_id_c

    #id card

    id_card_doc_type = request.POST.get('id_card_doc_type')
    id_card_selectedVal = request.POST.get('id_card_selectedVal')
    id_card_comment = request.POST.get('id_card_comment')


    #FB
    family_book_doc_type = request.POST.get('family_book_doc_type')
    family_book_selectedVal = request.POST.get('family_book_selectedVal')
    family_book_comment = request.POST.get('family_book_comment')


    #work proof
    work_proof_doc_type = request.POST.get('work_proof_doc_type')
    work_proof_selectedVal = request.POST.get('work_proof_selectedVal')
    work_proof_paper_type = request.POST.get('work_proof_paper_type')
    work_proof_comment = request.POST.get('work_proof_comment')


    #income proof
    income_proof_doc_type = request.POST.get('income_proof_doc_type')
    income_proof_selectedVal = request.POST.get('income_proof_selectedVal')
    income_proof_paper_type = request.POST.get('income_proof_paper_type')
    income_proof_comment = request.POST.get('income_proof_comment')


    #current address
    current_address_doc_type = request.POST.get('current_address_doc_type')
    current_address_selectedVal = request.POST.get('current_address_selectedVal')
    current_address_paper_type = request.POST.get('current_address_paper_type')
    current_address_comment = request.POST.get('current_address_comment')


    #cib
    cib_doc_type = request.POST.get('cib_doc_type')
    cib_selectedVal = request.POST.get('cib_selectedVal')
    cib_result_type = request.POST.get('cib_result_type')
    cib_result_id_card = request.POST.get('cib_result_id_card')
    cib_comment = request.POST.get('cib_comment')


    #tax  
    check_tax_doc_type = request.POST.get('check_tax_doc_type')
    check_tax_selectedVal = request.POST.get('check_tax_selectedVal')
    check_tax_type = request.POST.get('check_tax_type')
    check_tax_id_card = request.POST.get('check_tax_id_card')
    tax_comment = request.POST.get('tax_comment')


    #dedupe
    check_dedupe_doc_type = request.POST.get('check_dedupe_doc_type')
    check_dedupe_selectedVal = request.POST.get('check_dedupe_selectedVal')
    check_dedupe_type = request.POST.get('check_dedupe_type')
    dedupe_comment = request.POST.get('dedupe_comment')

    #defer note
    defer_note_doc_type = request.POST.get('defer_note_doc_type')
    check_defer_pending_selectedVal = request.POST.get('check_defer_pending_selectedVal')
    defer_note = request.POST.get('defer_note')






    #if app_id_c != "" and id_card_doc_type != "" and id_card_selectedVal != "":
    if app_id_c != None and id_card_doc_type != None and id_card_selectedVal != None and app_id_c != "" and id_card_doc_type != "" and id_card_selectedVal != "":
        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = id_card_doc_type, checked_result = id_card_selectedVal, checked_comment = id_card_comment, action_username = request.user)

    else:

        pass


    #if app_id_c != "" and family_book_doc_type != "" and family_book_selectedVal != "":
    if app_id_c != None and family_book_doc_type != None and family_book_selectedVal != None and app_id_c != "" and family_book_doc_type != "" and family_book_selectedVal != "":
            post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = family_book_doc_type, checked_result = family_book_selectedVal, checked_comment = family_book_comment, action_username = request.user)

    else:

        pass

    #if app_id_c != "" and work_proof_doc_type != "" and work_proof_paper_type != "" and work_proof_selectedVal != "":
    if app_id_c != None and work_proof_doc_type != None and work_proof_paper_type != None and work_proof_selectedVal != None and app_id_c != "" and work_proof_doc_type != "" and work_proof_paper_type != "" and work_proof_selectedVal != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = work_proof_doc_type, paper_type_item = work_proof_paper_type, checked_result = work_proof_selectedVal, checked_comment = work_proof_comment, action_username = request.user)

    else:

        pass

    #if app_id_c != "" and income_proof_doc_type != "" and income_proof_paper_type != "" and income_proof_selectedVal != "":
    if app_id_c != None and income_proof_doc_type != None and income_proof_paper_type != None and income_proof_selectedVal != None and app_id_c != "" and income_proof_doc_type != "" and income_proof_paper_type != "" and income_proof_selectedVal != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = income_proof_doc_type, paper_type_item = income_proof_paper_type, checked_result = income_proof_selectedVal, checked_comment = income_proof_comment, action_username = request.user)

    else:

        pass

    # app_id_c != "" and current_address_doc_type != "" and current_address_paper_type != "" and current_address_selectedVal != ""
    if app_id_c != None and current_address_doc_type != None and current_address_paper_type != None and current_address_selectedVal != None and app_id_c != "" and current_address_doc_type != "" and current_address_paper_type != "" and current_address_selectedVal != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = current_address_doc_type, paper_type_item = current_address_paper_type, checked_result = current_address_selectedVal, checked_comment = current_address_comment, action_username = request.user)

    else:

        pass

    # app_id_c != "" and cib_doc_type != "" and cib_selectedVal != "" and cib_result_type != ""
    if app_id_c != None and cib_doc_type != None and cib_selectedVal != None and cib_result_type != None and app_id_c != "" and cib_doc_type != "" and cib_selectedVal != "" and cib_result_type != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = cib_doc_type, paper_type_item = cib_result_type, checked_result = cib_selectedVal, checked_code = cib_result_id_card, checked_comment = cib_comment, action_username = request.user)

    else:

        pass

    # app_id_c != "" and check_tax_doc_type != "" and check_tax_selectedVal != "" and check_tax_type != ""
    if app_id_c != None and check_tax_doc_type != None and check_tax_selectedVal != None and check_tax_type != None  and app_id_c != "" and check_tax_doc_type != "" and check_tax_selectedVal != "" and check_tax_type != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = check_tax_doc_type, paper_type_item = check_tax_type, checked_result = check_tax_selectedVal, checked_code = check_tax_id_card, checked_comment = tax_comment, action_username = request.user)

    else:

        pass

    # app_id_c != "" and check_dedupe_doc_type != "" and check_dedupe_selectedVal != "" and check_dedupe_type != ""
    if app_id_c != None and check_dedupe_doc_type != None and check_dedupe_selectedVal != None and check_dedupe_type != None and app_id_c != "" and check_dedupe_doc_type != "" and check_dedupe_selectedVal != "" and check_dedupe_type != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, date_created = timezone.now(), paper_type = check_dedupe_doc_type, paper_type_item = check_dedupe_type, checked_result = check_dedupe_selectedVal, checked_comment = dedupe_comment, action_username = request.user)

    else:

        pass


    #
    if app_id_c != None and app_id_c != "" and check_defer_pending_selectedVal == 'UW_DC_DEFER' and defer_note != None and defer_note != "":

        post_instance = record_datachecker_result.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = defer_note_doc_type,  
                                                                checked_comment = defer_note, 
                                                                action_username = request.user
                                                                )

        post_instance = record_datachecker_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = defer_note_doc_type,
                                                                action_type_detail = check_defer_pending_selectedVal,  
                                                                action_comment = defer_note, 
                                                                action_username = request.user
                                                                )
    #

    elif app_id_c != "" and app_id_c != None and check_defer_pending_selectedVal != 'UW_DC_DEFER' and defer_note != None and defer_note != "":

        post_instance = record_datachecker_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = defer_note_doc_type,
                                                                action_type_detail = check_defer_pending_selectedVal,  
                                                                action_comment = defer_note, 
                                                                action_username = request.user
                                                                )

    else:

        pass



    # build a html posts list
    posts_id_card = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='ID CARD').order_by('paper_type', '-date_created')
    posts_family_book = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
    posts_work_proof = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
    posts_income_proof = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
    posts_current_address = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
    posts_cib = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='CIB').order_by('paper_type', '-date_created')
    posts_tax = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='TAX').order_by('paper_type', '-date_created')
    posts_dedupe = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='DUPE').order_by('paper_type', '-date_created')
    posts_defer_note = record_datachecker_result.objects.filter(app_id=app_id_c, paper_type='DEFER').order_by('paper_type', '-date_created')


    dc_result_table = {
                        "posts_id_card":posts_id_card,
                        "posts_family_book":posts_family_book,
                        "posts_work_proof":posts_work_proof,
                        "posts_income_proof":posts_income_proof,
                        "posts_current_address":posts_current_address,
                        "posts_cib":posts_cib,
                        "posts_tax":posts_tax,
                        "posts_dedupe":posts_dedupe,
                        "posts_defer_note":posts_defer_note,

                        }


    checklist_result_html = loader.render_to_string('./en/checklist/module_show_data_checker_result.html', dc_result_table)




    #phần này để thống kê pending
    #tuy nhiên cần xem xét việc sử dụng api thay vì sử dụng câu lệnh query trực tiếp

    dc_user_id = request.user

    mysql_sql_code = ("select final_stage.id, final_stage.id_max, " +
                      "  final_stage.app_id, final_stage.date_created, " +
                      "  final_stage.action_type, final_stage.action_type_detail, final_stage.action_comment " +
                      "  from " +
                      "      ( " +
                      "      select raw.id, max_id.id_max, raw.app_id, raw.date_created, " +
                      "              raw.action_type, raw.action_type_detail, raw.action_comment " +
                      "      from checklist_record_datachecker_pending_defer_history raw " +
                      "      left join ( " +
                      "                  select max(id) id_max, app_id " +
                      "                  from checklist_record_datachecker_pending_defer_history " +
                      "                  group by app_id " +
                      "                  ) max_id on max_id.app_id = raw.app_id " +
                      "      where action_username = %s " +
                      "      )final_stage " +
                      "   where  final_stage.id =  final_stage.id_max ")

    posts_dc_pending_case = record_datachecker_pending_defer_history.objects.raw(mysql_sql_code,[dc_user_id])



    dc_pending_table = {

                        "posts_dc_pending_case":posts_dc_pending_case,

                        }

    dc_pending_case_html = loader.render_to_string('./en/stage_dc/module_statistic_pending_case_at_dc.html', dc_pending_table)


    save_complete = True

    # package output data and return it as a JSON object
    output_data = { 'checklist_result_html': checklist_result_html,
                    'save_complete': save_complete,
                    'dc_pending_case_html' : dc_pending_case_html,}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)














def dc_create_update_company_information(request):

    f1_company_tax_code = request.POST.get('f1_company_tax_code')
    f1_company_name = request.POST.get('f1_company_name')
    riskbox_company_tax_code = request.POST.get('riskbox_company_tax_code')
    und_company_name = request.POST.get('und_company_name')
    dc_company_note = request.POST.get('dc_company_note')
    update_user = request.user.username



    company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code)

    if company_data_in_risk_box.exists():

        post_instance = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code).update(
                                                                                                                    f1employername = f1_company_name,
                                                                                                                    riskboxtaxcode = riskbox_company_tax_code, 
                                                                                                                    employername = und_company_name, 
                                                                                                                    dcnote = dc_company_note,
                                                                                                                    #und_date_created = timezone.now(),
                                                                                                                    dc_date_updated = timezone.now(), 
                                                                                                                    action_username = update_user
                                                                                                                    )

        post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                            f1employername = f1_company_name,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name,
                                                                            dcnote = dc_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            date_created = timezone.now(), 
                                                                            action_username = request.user)


    else:

        post_instance = record_und_newest_note_for_company.objects.create(  f1taxcode = f1_company_tax_code,
                                                                            f1employername = f1_company_name,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name, 
                                                                            dcnote = dc_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            dc_date_updated = timezone.now(), 
                                                                            action_username = request.user)

        post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name,
                                                                            dcnote = dc_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            date_created = timezone.now(), 
                                                                            action_username = request.user)

    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)










class ChecklistDedupeIDFinnoneDLView(View):


    def dispatch(request, *args, **kwargs):

        sql = '''

        select  rownum as id, a.app_id_c, b.customername, b.cif_no, 
                to_char(b.dob, 'dd/mm/yyyy') as dob, g.dpd, b.mother_fmaiden_name as issue_place,
                a.laa_app_signed_date_d, a.laa_product_id_c, g.app_status as status, c.address1 as current_province, d.address1 as fb_province, 
                b.spouse_id_c, b.spousename, 'finnone' as on_system 
        from macas.los_app_applications a
        left join macas.nbfc_customer_m b on a.cust_id_n = b.cust_id_n
        left join (select a.bpid, a.address1 from macas.nbfc_address_m a where a.addresstype = 'CURRES') c on c.bpid = a.cust_id_n
        left join (select a.bpid, a.address1 from macas.nbfc_address_m a where a.addresstype = 'PERMNENT') d on d.bpid = a.cust_id_n
        left join (
                    select t.app_id_c,
                               case when t.agreement_status = 'A' then 'ACTIVE'
                                        when t.agreement_status = 'C' then 'CLOSED'
                                            when t.agreement_status = 'X' then 'CANCELED'
                                                when t.agreement_status is Null then t.history_status
                                                    end as app_status, t.dpd
                    from
                    (
                        select  a.app_id_c,
                                case when a.las_activity_id in ('BDE', 'FII', 'FIV', 'SRR', 'DUR', 
                                                            'FIC', 'QDE', 'DOV', 'CBC', 'POR', 'UND', 'DUP') 
                                                                then 'IN-PROGRESS'
                                        when a.las_activity_id is Null then 'NO_STATUS'
                                            when a.las_activity_id in ('DISBDTL', 'FINISH', 'PDOC', 'DII', 'POSSTG') then 'APPROVED'
                                                when a.las_activity_id in ('CAN') then 'CANCELED'
                                                    when a.las_activity_id in ('REJ') then 'REJECTED'
                                                        end as history_status
                                , c.status agreement_status, c.dpd
                        from
                            (
                            
                            select e.*,
                                    max(e.row_no) over (partition by e.app_id_c) as max_row_no
                            from
                                (
                                select 
                                        a.*, 
                                        max(a.las_id_n) over (partition by a.app_id_c) max_las_id_n,
                                        min(a.las_edit_d) over (partition by a.app_id_c) create_date,
                                        row_number () over (partition by a.app_id_c order by las_edit_d asc) as row_no
                                from macas.los_app_status a
                                )e
                            )a
                        left join macas.los_app_applications b on a.app_id_c = b.app_id_c
                        left join java_leama.lea_agreement_dtl c on c.applid = a.app_id_c
                        where a.max_las_id_n = a.las_id_n and a.max_row_no > 1
                    )t
                )g on g.app_id_c = a.app_id_c

        '''

        from checklist.models import dedupe_id_model_data_finnone
        from checklist.functions.function_get_finnone_data import finnonedb_tbl_dl
        finnonedb_tbl_dl(   "finnonedb",
                            dedupe_id_model_data_finnone, 
                            "dedupe_id_model_data_finnone", "0",1000,sql)




        #phần update mobile của khách hàng và tham chiếu cần app id, như vậy cần phải thêm giai đoạn add app id
        #lấy app id ở bảng trên cũng ok 



        import textwrap
        response_text = textwrap.dedent('''\
            <html>
            <head>
                <title>Risk Box</title>
            </head>
            <body>
                <p>Finish update CL</p>
            </body>
            </html> ''')

        return HttpResponse (response_text)
