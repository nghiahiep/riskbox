# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

from checklist.models import record_datachecker_result, record_phv_result_cl, record_phv_comment_for_each_mobile
from checklist.models import record_datachecker_pending_defer_history, record_phv_pending_defer_history, record_phv_dti_calculation
from checklist.models import record_und_newest_note_for_company, record_und_note_history_for_company
from checklist.models import record_phv_sending_fiv_request, record_phv_sending_fiv_request_hist
from checklist.models import record_nice_score_history_for_app
from dedupe_page.models import dedupe_id_model, black_list_id_history_model, black_list_on_mafc_employee_dedupe_model
from dedupe_page.models import dedupe_mobile_cux_model, dedupe_mobile_ref_model



from django.db.models import Q

from django.db import connections
import cx_Oracle

from django.urls import reverse
from django.core import serializers

import datetime
import json
import re

from django.utils import timezone

from django.template import loader
from django.http import JsonResponse



# Create your views here.
def page(request):

    if request.user.is_authenticated:
        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
        
        
        #2 là PHV, 21 là UW POS
        if 2 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))

    else:
        return HttpResponseRedirect(reverse("login_page"))




    report_error = "Please input mobilephone number!"
    return render(request, './en/checklist/checklist_phv.html',{  "report_error":report_error, })








def get_app_data_from_finnone_before_using_funtion_of_dedupe_mobi(request):

  if request.user.is_authenticated:
      user_groups_id = request.user.groups.values_list('id', flat=True)
      user_groups = request.user.groups.values_list('name', flat=True)
      
      #2 là PHV, 21 là UW POS
      if 2 in user_groups_id or 21 in user_groups_id:
          pass

      elif "SUPER_ADMIN" in user_groups:
          pass
          
      else:
          return HttpResponseRedirect(reverse("login_page"))

  else:
      return HttpResponseRedirect(reverse("login_page"))



  #if request.user.is_authenticated():
  #    user_groups = request.user.groups.values_list('name', flat=True)
  #    if "UW_POS" in user_groups or"UND" in user_groups:
  #        pass
          #return HttpResponseRedirect(reverse("login_page"))
  #    else:
  #        return HttpResponseRedirect(reverse("login_page"))
  #else:
  #    return HttpResponseRedirect(reverse("login_page"))




  get_app_id_for_dedupe_phone = request.GET.get('app_id_c')


  #lấy các số điện thoại cần search cho phv check list

  oracle_sql = ("SELECT C.id, E.APP_ID_C, C.BPID, C.NAME, C.RELATION, C.MOBILE " +
                "FROM (" +
                "SELECT " +
                "      1 as id, A.BPID, B.CUSTOMERNAME AS NAME, 'CUX' AS RELATION, A.MOBILE "+
                "FROM "+
                "      MACAS.NBFC_ADDRESS_M A, "+
                "      MACAS.NBFC_CUSTOMER_M B "+
                "WHERE "+
                "      A.BPID = B.CUST_ID_N AND "+
                "      A.ADDRESSTYPE = 'CURRES' "+    
                "UNION ALL "+
                "SELECT "+
                "      2 AS ID, B.CUST_ID_N, B.REFERENCE_NAME, B.REF_RELATION, "+
                "      CASE WHEN B.STDISD IS NULL THEN B.PHONE1 ELSE B.STDISD END AS REF_MOBILE "+
                "FROM "+
                "      MACAS.NBFC_REFERENCE_M B )C "+
                "LEFT JOIN MACAS.LOS_APP_APPLICATIONS E ON E.CUST_ID_N = C.BPID "+
                "WHERE E.APP_ID_C = TO_CHAR (%s) ")


  queryset_list  = dedupe_mobile_cux_model.objects.using('finnonedb').raw(oracle_sql,[get_app_id_for_dedupe_phone])


  #bên cạnh lấy thông tin các số điện thoại, cần phải lấy cả note của số điện thoại
  #các câu lệnh sau để lấy comment cho từng số điện thoại
  phone_list = []
  f1_cust_id = ''


  for item in queryset_list:      

      phone_list.append(item.mobile)
      f1_cust_id = item.bpid



  posts_phv_note = record_phv_comment_for_each_mobile.objects.filter(called_mobi__in = phone_list ).order_by('app_id', '-date_created')       



  context = {     
              "get_infor_for_dedupe_mobile": queryset_list,
              "posts_phv_note": posts_phv_note,
              }

  save_complete = True

  # build a html posts list
  posts_html00 = loader.render_to_string('./en/checklist/module1_phv_get_all_mobile_phone.html', context)

  posts_html01 = loader.render_to_string('./en/checklist/module1_gui_phvcl_mobi_comment.html', context)




  if len(get_app_id_for_dedupe_phone) >=5:
      button_get_app = True
  else:
      button_get_app=''



  #lấy thông tin cho các trường thuộc dedupe id trên check list của phv



  #oracle_sql1 = (" SELECT DISTINCT 1 as id, %s as APP_ID_C, B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
  #               "        B.SPOUSENAME, B.SPOUSE_ID_C, B.OTHER_EMPLOYER_NAME_C AS COMPANY_NAME, B.NCM_LABOUR_CARDNO_C AS TAX_CODE " +
  #               " FROM MACAS.NBFC_CUSTOMER_M@LOSDR B WHERE B.CUST_ID_N IN " +
  #               "      (SELECT A.CUST_ID_N FROM MACAS.LOS_APP_APPLICATIONS@LOSDR A WHERE A.APP_ID_C = TO_CHAR (%s) )" )



  oracle_sql2 = ("SELECT B.id, TRIM (TO_CHAR (B.LAA_EMI_N,'999,999,999,999')) AS LAA_EMI_N, " +
                 "       TRIM (TO_CHAR (B.LAA_APP_REQ_AMTFIN_N,'999,999,999,999')) AS LAA_APP_REQ_AMTFIN_N, " +
                 "       B.LAA_APP_REQ_EFFRATE_N, B.N_INTEREST_RATE, " +
                 "       B.LAA_APP_LOANDTL_TERM_N, B.CIC_COMMENT, " +
                 "       NVL (B.TOTAL_CIC_AMT,0) AS TOTAL_CIC_AMT, " +
                 "       TRIM(TO_CHAR (NVL(B.TOTAL_CIC_EMI,0), '999,999,999,999')) AS TOTAL_CIC_EMI, " +
                 "       TRIM(TO_CHAR(B.MONTHLY_INCOME,'999,999,999,999')) AS MONTHLY_INCOME, B.DTI, " +
                 "       B.PRIORITY, B.INSPECTORNAME, B.LAA_PRODUCT_ID_C, B.APP_ID_C," +
                 "       B.CUSTOMERNAME, B.DOB, B.CIF_NO, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                 "       B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE "
                 "FROM " +
                 "       (SELECT DISTINCT 1 as id, A.LAA_EMI_N, A.LAA_APP_REQ_AMTFIN_N, A.LAA_APP_REQ_EFFRATE_N, A.N_INTEREST_RATE, " +
                 "               A.LAA_APP_LOANDTL_TERM_N, A.CIC_COMMENT, A.TOTAL_CIC_AMT, A.TOTAL_CIC_EMI, " +
                 "               B.MONTHLY_INCOME, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                 "               CASE WHEN B.MONTHLY_INCOME = 0 OR NVL(B.MONTHLY_INCOME,0) = 0 THEN 0 " +
                 "                      ELSE A.LAA_EMI_N / (B.MONTHLY_INCOME - NVL(A.TOTAL_CIC_EMI,0)) END AS DTI, " + 
                 "               A.PRIORITY, A.INSPECTORNAME, A.LAA_PRODUCT_ID_C, A.APP_ID_C, " +
                 "               B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                 "               B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE "
                 "        FROM   " +
                 "        (      " +
                 "               " +
                 "        SELECT " +   
                 "              LOS_APP_APPLICATIONS.APP_ID_C, LOS_APP_APPLICATIONS.CUST_ID_N, " +
                 "              LOS_APP_APPLICATIONS.LAA_EMI_N, " +
                 "              LOS_APP_APPLICATIONS.LAA_APP_REQ_AMTFIN_N, LOS_APP_APPLICATIONS.LAA_APP_REQ_EFFRATE_N, " +
                 "              LOS_APP_APPLICATIONS.N_INTEREST_RATE, LOS_APP_APPLICATIONS.LAA_APP_LOANDTL_TERM_N, " +
                 "              FIELD_DATA_TXN.FIELD8 AS CIC_COMMENT, FIELD_DATA_TXN.FIELD39 AS TOTAL_CIC_AMT, " +
                 "              TO_NUMBER(REGEXP_REPLACE(TO_CHAR(FIELD_DATA_TXN.FIELD40), '[,]+','')) AS TOTAL_CIC_EMI, " +
                 "              CASE WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'H' THEN 'BANK STATEMENT' " +
                 "                  WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'BL' THEN 'BUSINESS LICENSE' " +
                 "                    WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NBL' THEN 'NO BUSINESS LICENSE' " +
                 "                      WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NO' THEN 'NONE' " +
                 "                        WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'N' THEN 'PAY SLIP' " +
                 "                          END AS PRIORITY, " +
                 "              LOS_APP_APPLICATIONS.LAA_PRODUCT_ID_C, LEA_INSPECTOR_M.INSPECTORNAME " +
                 "        FROM  " +
                 "              MACAS.LOS_APP_APPLICATIONS LOS_APP_APPLICATIONS " +
                 "        LEFT JOIN " +
                 "              MACAS.FIELD_DATA_TXN FIELD_DATA_TXN ON FIELD_DATA_TXN.APP_ID = LOS_APP_APPLICATIONS.APP_ID_C " +
                 "        LEFT JOIN ( " +
                 "                    SELECT A.INSPECTORID, A.INSPECTORNAME " +
                 "                    FROM MAMAS.LEA_INSPECTOR_M A "+
                 "                  )LEA_INSPECTOR_M ON LOS_APP_APPLICATIONS.LAA_SOURCE_INSPECTORID_N = LEA_INSPECTOR_M.INSPECTORID " +
                 "        WHERE LOS_APP_APPLICATIONS.APP_ID_C = TO_CHAR (%s) " +
                 "        )A        " +
                 "                  " +
                 "        LEFT JOIN " +
                 "            (     " +
                 "              SELECT  A.CUST_ID_N, A.CUSTOMERNAME, A.DOB, A.AGE, A.CIF_NO, " +
                 "                      CASE WHEN A.SEX = 'F' THEN 'FEMALE' WHEN A.SEX = 'M' THEN 'MALE' END AS GENDER ," +
                 "                      A.MARITAL_STATUS, "
                 "                      A.SPOUSENAME, A.SPOUSE_ID_C, A.MONTHLY_INCOME, " +
                 "                      A.OTHER_EMPLOYER_NAME_C AS COMPANY_NAME, A.NCM_LABOUR_CARDNO_C AS TAX_CODE " +
                 "              FROM MACAS.NBFC_CUSTOMER_M A " +
                 "            )B ON B.CUST_ID_N = A.CUST_ID_N " +
                 "        WHERE A.APP_ID_C = TO_CHAR (%s))B " ) 



  #queryset_list1  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql1,[get_app_id_for_dedupe_phone, get_app_id_for_dedupe_phone])
  queryset_list2  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql2,[get_app_id_for_dedupe_phone, get_app_id_for_dedupe_phone])



  oracle_sql3 = ( "SELECT 1 as id," +
                  "      A.address1, B.zipdesc, A.city_lms, A.statedesc, A.addresstype, A.mobile " +
                  "FROM " +
                  "      MACAS.NBFC_ADDRESS_M A " +
                  "LEFT JOIN " +
                  "    (SELECT zipcode, zipdesc FROM macas.nbfc_city_zipcode_m) B ON A.zipcode = B.zipcode " +  
                  "WHERE A.BPID = to_char (%s) " )

  queryset_list3  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql3,[f1_cust_id])


  

  phv_final_request_fiv_note_curres = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'CURRES').order_by('-date_request')[:1]
  phv_final_request_fiv_note_curres_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'CURRES', send_fiv ='true').order_by('-date_request')[:1]

  phv_final_request_fiv_note_permnent = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'PERMNENT').order_by('-date_request')[:1]
  phv_final_request_fiv_note_permnent_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'PERMNENT', send_fiv ='true').order_by('-date_request')[:1]


  phv_final_request_fiv_note_headoff = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'HEADOFF').order_by('-date_request')[:1]
  phv_final_request_fiv_note_headoff_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'HEADOFF', send_fiv ='true').order_by('-date_request')[:1]


  phv_final_request_fiv_note_bchoff = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'BCHOFF').order_by('-date_request')[:1]
  phv_final_request_fiv_note_bchoff_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'BCHOFF', send_fiv ='true').order_by('-date_request')[:1]


  phv_final_request_fiv_note_office = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'OFFICE').order_by('-date_request')[:1]
  phv_final_request_fiv_note_office_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_dedupe_phone, type_of_address= 'OFFICE', send_fiv ='true').order_by('-date_request')[:1]



  oracle_sql4 = (
                  "SELECT 1 as id," + 
                  "       A.mobile, B.spouse_mobile " +
                  "FROM  " +
                  "       MACAS.NBFC_ADDRESS_M A " +
                  "LEFT JOIN " +
                  "     ( " +
                  "       SELECT A.CUST_ID_N, MAX(A.PHONE1) KEEP (DENSE_RANK FIRST ORDER BY rownum) AS spouse_mobile  " +
                  "       FROM MACAS.NBFC_REFERENCE_M A " +
                  "       WHERE A.CUST_ID_N = to_char(%s) AND A.REF_RELATION = 'WH' " +
                  "       GROUP BY A.CUST_ID_N " +
                  "     )B ON B.CUST_ID_N = A.BPID " +
                  "WHERE A.BPID = to_char(%s) AND A.ADDRESSTYPE = 'CURRES' "
                )

  queryset_list4  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql4,[f1_cust_id,f1_cust_id])



  from checklist.functions.function_get_nice_score import get_and_store_nice_score, get_nice_score
  from django.contrib.auth.models import Group 

  nice_score_last_try = get_and_store_nice_score( get_app_id_for_dedupe_phone = get_app_id_for_dedupe_phone, 
                                                  username = request.user.username,
                                                  #record_team = request.user.groups.values_list('name', flat=True))
                                                  record_team = Group.objects.get(user=request.user).name)

  #nice_score_last_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-record_date')[:1]
  nice_score_first_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('record_date')[:1]



  context1 = {     
              "basic_app_infor": queryset_list2,
              "basic_loan_infor": queryset_list2,
              "basic_mobile_infor": queryset_list3,
              "basic_mobile_num": queryset_list4,
              "button_get_app": button_get_app,
              "nice_score_first_try": nice_score_first_try,
              "nice_score_last_try": nice_score_last_try
              }

  if phv_final_request_fiv_note_curres.exists():
    context1.update({'phv_final_request_fiv_note_curres':phv_final_request_fiv_note_curres})
    context1.update({'phv_final_request_fiv_note_curres_fiv':phv_final_request_fiv_note_curres_fiv})


  if phv_final_request_fiv_note_permnent.exists():
    context1.update({'phv_final_request_fiv_note_permnent':phv_final_request_fiv_note_permnent})
    context1.update({'phv_final_request_fiv_note_permnent_fiv':phv_final_request_fiv_note_permnent_fiv})


  if phv_final_request_fiv_note_headoff.exists():
    context1.update({'phv_final_request_fiv_note_headoff':phv_final_request_fiv_note_headoff})
    context1.update({'phv_final_request_fiv_note_headoff_fiv':phv_final_request_fiv_note_headoff_fiv})


  if phv_final_request_fiv_note_bchoff.exists():
    context1.update({'phv_final_request_fiv_note_bchoff':phv_final_request_fiv_note_bchoff})
    context1.update({'phv_final_request_fiv_note_bchoff_fiv':phv_final_request_fiv_note_bchoff_fiv})


  if phv_final_request_fiv_note_office.exists():
    context1.update({'phv_final_request_fiv_note_office':phv_final_request_fiv_note_office})
    context1.update({'phv_final_request_fiv_note_office_fiv':phv_final_request_fiv_note_office_fiv})



  posts_html1 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_items_cux.html', context1)
  posts_html2 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_items_spouse.html', context1)
  # load module này chủ yếu chỉ để hiển thị giao diện, không có giá trị
  posts_html3 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context1)

  posts_html4 = loader.render_to_string('./en/checklist/module1_tab_phv_cl.html', context1)
  #posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
  posts_html6 = loader.render_to_string('./en/checklist/module1_gui_phvcl_show_basic_loan_infor.html', context1)
  
  posts_html9 = loader.render_to_string('./en/checklist/module1_gui_phvcl_show_fiv_form_data.html', context1)







  #phần này là giải thuật quyết định khi nào lấy data của f1 khi nào lấy data của risk box cho mục tính DTI
  dti_result_in_risk_box = record_phv_dti_calculation.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-date_created')[:1]

  riskbox_dti = {
              "basic_loan_infor": dti_result_in_risk_box,
  }

  if dti_result_in_risk_box.exists():
      posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
      posts_html7 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti_result.html', riskbox_dti)


  else:
      posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
      posts_html7 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti_result.html', context1)


  #load module thể hiện final status của dc
  dc_note_status = record_datachecker_pending_defer_history.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-date_created')[:1]

  dc_final_note_status = {
                          "dc_note_status":dc_note_status,
                          }

  dc_final_note_status_html = loader.render_to_string('./en/checklist/module_gui_dccl_dc_note_status.html', dc_final_note_status)




  posts_id_card = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='ID CARD').order_by('paper_type', '-date_created')
  posts_family_book = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
  posts_work_proof = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
  posts_income_proof = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
  posts_current_address = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
  posts_cib = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='CIB').order_by('paper_type', '-date_created')
  posts_tax = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='TAX').order_by('paper_type', '-date_created')
  posts_dedupe = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='DUPE').order_by('paper_type', '-date_created')
  posts_defer_note = record_datachecker_result.objects.filter(app_id=get_app_id_for_dedupe_phone, paper_type='DEFER').order_by('paper_type', '-date_created')

  dc_result_table = {
                      "posts_id_card":posts_id_card,
                      "posts_family_book":posts_family_book,
                      "posts_work_proof":posts_work_proof,
                      "posts_income_proof":posts_income_proof,
                      "posts_current_address":posts_current_address,
                      "posts_cib":posts_cib,
                      "posts_tax":posts_tax,
                      "posts_dedupe":posts_dedupe,
                      "posts_defer_note":posts_defer_note,
                      }

  checklist_result_html = loader.render_to_string('./en/checklist/module_show_data_checker_result.html', dc_result_table)





  # build a html posts for phv main items in check list
  posts_cic_detail = record_phv_result_cl.objects.filter(app_id=get_app_id_for_dedupe_phone, check_type='CIC DETAIL').order_by('check_type', '-date_created')
  posts_payment = record_phv_result_cl.objects.filter(app_id=get_app_id_for_dedupe_phone, check_type='PAYMENT').order_by('check_type', '-date_created')
  posts_home = record_phv_result_cl.objects.filter(app_id=get_app_id_for_dedupe_phone, check_type='HOME').order_by('check_type', '-date_created')
  posts_work = record_phv_result_cl.objects.filter(app_id=get_app_id_for_dedupe_phone, check_type='WORK').order_by('check_type', '-date_created')
  posts_defer_note = record_phv_result_cl.objects.filter(app_id=get_app_id_for_dedupe_phone, check_type='DEFER').order_by('check_type', '-date_created')

  phv_result_table = {
                      "posts_cic_detail":posts_cic_detail,
                      "posts_payment":posts_payment,
                      "posts_home":posts_home,
                      "posts_work":posts_work,
                      "posts_defer_note": posts_defer_note,
                      }


  phv_checklist_result_html = loader.render_to_string('./en/checklist/module1_show_phv_result.html', phv_result_table)




  # phần này nhằm lấy lại trạng thái cuối cùng của app do PHV note
  phv_note_status = record_phv_pending_defer_history.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-date_created')[:1]

  phv_final_note_status = {
                          "phv_note_status":phv_note_status,
                          }

  phv_final_note_status_html = loader.render_to_string('./en/checklist/module1_gui_phvcl_phv_note_status.html', phv_final_note_status)




  #phần này là giải thuật quyết định khi nào lấy data của f1 khi nào lấy data của risk box cho mục thông tin công ty
  v_f1taxcode = ''
  for tax in queryset_list2:
      v_f1taxcode = tax.tax_code
      
  company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = v_f1taxcode)

  dc_final_note_company = {
              "riskox_company_infor":company_data_in_risk_box,
              "basic_app_infor": queryset_list2,
              }


  if company_data_in_risk_box.exists():

      posts_html8 = loader.render_to_string('./en/checklist/module1_gui_show_company_info_update.html', dc_final_note_company)

  else:

      posts_html8 = loader.render_to_string('./en/checklist/module1_gui_show_company_info.html', context1)


  #phần này refresh lại giao diện check pcb

  pcb_gui = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_check.html', context=None)



  # package output data and return it as a JSON object
  output_data = { 'posts_html00' : posts_html00,
                  'posts_html01' : posts_html01,
                  'posts_html1' : posts_html1,
                  'posts_html2' : posts_html2,
                  'posts_html3' : posts_html3,
                  'posts_html4' : posts_html4,
                  'posts_html5' : posts_html5,
                  'posts_html6' : posts_html6,
                  'posts_html7' : posts_html7,
                  'posts_html8' : posts_html8,
                  'posts_html9' : posts_html9,
                  'save_complete': save_complete,
                  'pcb_gui': pcb_gui,
                  'checklist_result_html': checklist_result_html,
                  'dc_final_note_status_html' : dc_final_note_status_html,
                  'phv_checklist_result_html': phv_checklist_result_html,
                  'phv_final_note_status_html' : phv_final_note_status_html,
                  }
  
  return JsonResponse(output_data)  












def search_customer_information_with_full_information_id_dob_name_on_phv_checklist(request):

    query02 = request.GET.get('customer_name')
    query03 = request.GET.get('customer_dob')
    query04 = request.GET.get('customer_id')


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <9:
        report_error = "Please input correct ID No of customer!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR (%s),'[[:space:]]*', '') " +
                            "       AND DOB = %s) OR (CUSTOMER_ID_NUMBER = %s ) OR (SPOUSE_ID = %s) " )

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query02, query03, query04, query04])

        context = { 
                "object_list": queryset_list
                }


    save_complete = True

    # build a html posts list
    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)


    # package output data and return it as a JSON object
    output_data = {'posts_html': posts_html,'save_complete': save_complete}
    
    return JsonResponse(output_data)











def search_customer_information_with_home_directory_style_on_phv_checklist(request):

    query002 = request.GET.get('customer_name')
    query003 = request.GET.get('customer_dob')
    query004 = request.GET.get('customer_id')

    

    if query002 is not None:
        query02 = query002.upper()
        query012 = re.sub('[!@#$%]', '', query02)
    else:
        query02 = query002

    if query003 is not None:
        query03 = query003.upper()
        query013 = re.sub('[!@#$%]', '', query03)
    else:
        query03 = query003

    if query004 is not None:
        query04 = query004.upper()
        query014 = re.sub('[!@#$%]', '', query04)
    else:
        query04 = query004


    if query04 is None:
        query04 = '%'
    else:        
        query04 = query04 + '%'


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 5 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                "           OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                "   FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                "   WHERE CUSTOMER_ID_NUMBER LIKE %s AND CUSTOMER_NAME LIKE %s AND DOB LIKE %s")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query04, query02, query03])


        context = { 
            "object_list": queryset_list,
            }



    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)











def search_customer_spouse_information_with_free_style_on_phv_checklist(request):

    query005 = request.GET.get('spouse_name')
    query006 = request.GET.get('spouse_id')


    if query005 is not None:
        query05 = query005.upper()
        query015 = re.sub('[!@#$%]', '', query05)
    else:
        query05 = query005

    if query006 is not None:
        query06 = query006.upper()
        query016 = re.sub('[!@#$%]', '', query06)
    else:
        query06 = query006



    digit = 0

    if query06 is None:
        query06 = '%'


    for i in query06:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 3 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE (( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(SPOUSE_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND SPOUSE_ID LIKE %s ))" +
                            "       OR ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND CUSTOMER_ID_NUMBER LIKE %s)")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query05, query06, query05, query06])

        context = { 
                    "object_list": queryset_list,
                    }


    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)











def search_mobile_dedupe_mobile_phone_cux_and_ref_on_phv_checklist(request):

    searched_mobilephone = request.GET.get('searched_mobile')

    oracle_sql1 = ("SELECT DISTINCT 1 as id, APP_ID, ADDRESS_TYPE, CUX_PHONE1, CUX_MOBILE, CUX_ADD1, " +
                      "    DPD, STATUS, CUSTOMER_ID_NO" + 
                      "  FROM DEDUPE_PAGE_DEDUPE_MOBILE_8D55 WHERE CUX_PHONE1 = %s or CUX_MOBILE = %s")

    oracle_sql2 = ("SELECT DISTINCT 1 as id, APP_ID, REF_NAME, REF_REL, REF_PHONE1, REF_PHONE2, " +
                      "    REF_STDISD, DPD, STATUS, CUSTOMER_ID_NO" + 
                      "  FROM DEDUPE_PAGE_DEDUPE_MOBILE_3899 WHERE REF_PHONE1 = %s or REF_PHONE2 = %s or REF_STDISD = %s")

    oracle_sql3 = ( "SELECT DISTINCT 1 as id, CUSTOMER_NATIONAL_ID, CUSTOMER_NAME, DSA_CODE, PHONE_NUMBER " + 
                    "FROM DEDUPE_PAGE_BLACK_LIST_ON_31B8 "+
                    "WHERE PHONE_NUMBER = %s ")

    mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.using('datadb').raw(oracle_sql1,[searched_mobilephone, searched_mobilephone])
    mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.using('datadb').raw(oracle_sql2,[searched_mobilephone, searched_mobilephone, searched_mobilephone])
    mobilephone_dupe_result_2  = black_list_on_mafc_employee_dedupe_model.objects.using('datadb').raw(oracle_sql3,[searched_mobilephone])

    #mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.using('datadb').get( Q(cux_phone1 =searched_mobilephone) | Q(cux_mobile =searched_mobilephone)) 
    #mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.using('datadb').get( Q(ref_phone1 =searched_mobilephone) | Q(ref_phone2 =searched_mobilephone) | Q(ref_stdisd =searched_mobilephone)) 


    context = {     
                "mobilephone_dupe_result": mobilephone_dupe_result,
                "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                "mobilephone_dupe_result_2": mobilephone_dupe_result_2,
                }

    save_complete = True

    posts_html01 = loader.render_to_string('./en/checklist/module1_gui_dedupemobi_cux_mobi.html', context)
    posts_html02 = loader.render_to_string('./en/checklist/module1_gui_dedupemobi_ref_mobi.html', context)
    posts_html03 = loader.render_to_string('./en/checklist/module1_gui_alert_sale_mobi.html', context)


    output_data = { 'posts_html01': posts_html01,
                    'posts_html02': posts_html02,
                    'posts_html03' : posts_html03,
                    'save_complete': save_complete}

    return JsonResponse(output_data)















def phv_cl_super_total_comment(request):

    app_id_c = request.POST.get('app_id_c')

    app_id_c = app_id_c.replace(" ", "")

    #app_id_c = app_id_c.split("\n",1)[1]

    app_id_c = app_id_c.strip()

    #id card

    cic_detail_checked_type = request.POST.get('cic_detail_checked_type')
    cic_detail_selectedVal = request.POST.get('cic_detail_selectedVal')
    cic_detail_comment = request.POST.get('cic_detail_comment')


    payment_checked_type = request.POST.get('payment_checked_type')
    payment_selectedVal = request.POST.get('payment_selectedVal')
    payment_comment = request.POST.get('payment_comment')


    home_checked_type = request.POST.get('home_checked_type')
    home_selectedVal = request.POST.get('home_selectedVal')
    home_type = request.POST.get('home_type')
    home_type_uw_decision = request.POST.get('home_type_uw_decision')
    home_comment = request.POST.get('home_comment')


    work_checked_type = request.POST.get('work_checked_type')
    work_selectedVal = request.POST.get('work_selectedVal')
    work_check_directory = request.POST.get('work_check_directory')
    work_type_uw_decision = request.POST.get('work_type_uw_decision')
    work_comment = request.POST.get('work_comment')


    defer_note_doc_type = request.POST.get('defer_note_doc_type')
    check_defer_pending_selectedVal = request.POST.get('check_defer_pending_selectedVal')
    phv_note_status = request.POST.get('phv_note_status')




    phvcl_basic_loan_infor_priority = request.POST.get('phvcl_basic_loan_infor_priority')
    phvcl_basic_loan_infor_sale_officer = request.POST.get('phvcl_basic_loan_infor_sale_officer')
    phvcl_basic_loan_infor_pos = request.POST.get('phvcl_basic_loan_infor_pos')
    phvcl_basic_loan_infor_amount = request.POST.get('phvcl_basic_loan_infor_amount')
    phvcl_basic_loan_infor_emi = request.POST.get('phvcl_basic_loan_infor_emi')
    phvcl_basic_loan_infor_tenure = request.POST.get('phvcl_basic_loan_infor_tenure')
    phvcl_basic_loan_infor_product = request.POST.get('phvcl_basic_loan_infor_product')
    phvcl_dti_cal_financial_name = request.POST.get('phvcl_dti_cal_financial_name')
    phvcl_dti_cal_total_outstanding = request.POST.get('phvcl_dti_cal_total_outstanding')
    phvcl_dti_cal_total_emi = request.POST.get('phvcl_dti_cal_total_emi')
    phvcl_dti_cal_ir = request.POST.get('phvcl_dti_cal_ir')
    phvcl_dti_cal_emi = request.POST.get('phvcl_dti_cal_emi')
    phvcl_dti_cal_user = request.POST.get('phvcl_dti_cal_user')
    phvcl_dti_cal_la = request.POST.get('phvcl_dti_cal_la')
    phvcl_dti_cal_tenure = request.POST.get('phvcl_dti_cal_tenure')
    phvcl_dti_cal_income = request.POST.get('phvcl_dti_cal_income')
    phvcl_dti_cal_dti = request.POST.get('phvcl_dti_cal_dti')




    if app_id_c != None and cic_detail_checked_type != None and cic_detail_selectedVal != None and app_id_c != "" and cic_detail_checked_type != "" and cic_detail_selectedVal != "":

        post_instance = record_phv_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                check_type = cic_detail_checked_type, 
                                                                checked_result = cic_detail_selectedVal, 
                                                                checked_comment = cic_detail_comment, 
                                                                action_username = request.user)

    else:

        pass



    if app_id_c != None and payment_checked_type != None and payment_selectedVal != None and app_id_c != "" and payment_checked_type != "" and payment_selectedVal != "":

        post_instance = record_phv_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                check_type = payment_checked_type, 
                                                                checked_result = payment_selectedVal, 
                                                                checked_comment = payment_comment, 
                                                                action_username = request.user)

    else:

        pass



    #if app_id_c != "" and home_checked_type != "" and home_selectedVal != "" and home_type != ""  and home_type_uw_decision != "":
    if app_id_c != None and home_checked_type != None and app_id_c != "" and home_checked_type != "" and home_type != "..."  and home_type_uw_decision != "...":

        post_instance = record_phv_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    check_type = home_checked_type, 
                                                                    checked_result = home_selectedVal,
                                                                    check_type_detail = home_type,
                                                                    checked_type_uw_decision = home_type_uw_decision, 
                                                                    checked_comment = home_comment, 
                                                                    action_username = request.user)

    else:

        pass



    #if app_id_c != "" and work_checked_type != "" and work_selectedVal != "" and work_check_directory != ""  and work_type_uw_decision != "":
    if app_id_c != None and work_checked_type != None and app_id_c != "" and work_checked_type != "" and work_check_directory != "..."  and work_type_uw_decision != "...":


        post_instance = record_phv_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    check_type = work_checked_type, 
                                                                    checked_result = work_selectedVal,
                                                                    check_type_detail = work_check_directory,
                                                                    checked_type_uw_decision = work_type_uw_decision, 
                                                                    checked_comment = work_comment, 
                                                                    action_username = request.user)

    else:

        pass




    if app_id_c != None and app_id_c != "" and phv_note_status != None and phv_note_status != "" and check_defer_pending_selectedVal == 'UW_PHV_DEFER':

        post_instance = record_phv_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                check_type = defer_note_doc_type,  
                                                                checked_comment = phv_note_status, 
                                                                action_username = request.user
                                                                )

        post_instance = record_phv_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = defer_note_doc_type,
                                                                action_type_detail = check_defer_pending_selectedVal,  
                                                                action_comment = phv_note_status, 
                                                                action_username = request.user
                                                                )

    elif app_id_c != None and phv_note_status != None and app_id_c != "" and phv_note_status != "" and check_defer_pending_selectedVal != 'UW_PHV_DEFER':

        post_instance = record_phv_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = defer_note_doc_type,
                                                                action_type_detail = check_defer_pending_selectedVal,  
                                                                action_comment = phv_note_status, 
                                                                action_username = request.user
                                                                )

    else:

        pass




    #lưu việc tính toán DTI
    if phvcl_dti_cal_income != None and phvcl_dti_cal_income != '' and \
        phvcl_dti_cal_tenure!=None and phvcl_dti_cal_tenure !='' and \
        phvcl_dti_cal_la !=None and phvcl_dti_cal_la !='' and \
        phvcl_basic_loan_infor_tenure !=None and phvcl_basic_loan_infor_tenure !='' and \
        phvcl_basic_loan_infor_emi !=None and phvcl_basic_loan_infor_emi != '' and \
        phvcl_basic_loan_infor_amount !=None and phvcl_basic_loan_infor_amount != '':

        post_instance = record_phv_dti_calculation.objects.create(app_id = app_id_c,
                                                                f1_priority = phvcl_basic_loan_infor_priority,
                                                                f1_sale_officer = phvcl_basic_loan_infor_sale_officer,
                                                                f1_pos = phvcl_basic_loan_infor_pos,
                                                                f1_amount = phvcl_basic_loan_infor_amount,
                                                                f1_emi = phvcl_basic_loan_infor_emi,
                                                                f1_tenure = phvcl_basic_loan_infor_tenure,
                                                                f1_product = phvcl_basic_loan_infor_product,
                                                                f1_financial_name = phvcl_dti_cal_financial_name,
                                                                f1_total_outstanding = phvcl_dti_cal_total_outstanding,
                                                                f1_total_emi = phvcl_dti_cal_total_emi,
                                                                f1_ir = phvcl_dti_cal_ir,
                                                                phv_phv_emi = phvcl_dti_cal_emi,
                                                                laa_app_req_amtfin_n = phvcl_dti_cal_la,
                                                                laa_app_loandtl_term_n = phvcl_dti_cal_tenure,
                                                                monthly_income = phvcl_dti_cal_income,
                                                                dti = phvcl_dti_cal_dti,
                                                                action_username = request.user,
                                                                date_created = timezone.now()
                                                                    )
    else:

        pass


    # build a html posts list
    posts_cic_detail = record_phv_result_cl.objects.filter(app_id=app_id_c, check_type='CIC DETAIL').order_by('check_type', '-date_created')
    posts_payment = record_phv_result_cl.objects.filter(app_id=app_id_c, check_type='PAYMENT').order_by('check_type', '-date_created')
    posts_home = record_phv_result_cl.objects.filter(app_id=app_id_c, check_type='HOME').order_by('check_type', '-date_created')
    posts_work = record_phv_result_cl.objects.filter(app_id=app_id_c, check_type='WORK').order_by('check_type', '-date_created')
    posts_defer_note = record_phv_result_cl.objects.filter(app_id=app_id_c, check_type='DEFER').order_by('check_type', '-date_created')
   

    phv_result_table = {
                        "posts_cic_detail":posts_cic_detail,
                        "posts_payment":posts_payment,
                        "posts_home":posts_home,
                        "posts_work":posts_work,
                        "posts_defer_note":posts_defer_note,
                        }


    checklist_result_html = loader.render_to_string('./en/checklist/module1_show_phv_result.html', phv_result_table)








    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'checklist_result_html': checklist_result_html,'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)











def phv_comment_for_every_mobile_check(request):

    app_id_c = request.POST.get('app_id_c')

    #app_id_c = app_id_c.split("\n",1)[1]

    app_id_c = app_id_c.strip()


    commented_mobile = request.POST.get('commented_mobile')
    phv_decision = request.POST.get('phv_decision')
    phv_emoji = request.POST.get('phv_emoji')
    phv_note = request.POST.get('phv_note')


    try:
        phv_decision = int(phv_decision)
    except ValueError:
        #Handle the exception
        phv_decision = ''


    try:
        phv_emoji = int(phv_emoji)
    except ValueError:
        #Handle the exception
        phv_emoji = ''


    if app_id_c != "" and phv_decision != "" and phv_emoji != "":

        post_instance = record_phv_comment_for_each_mobile.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(),
                                                                called_mobi = commented_mobile, 
                                                                phv_decision_after_call = phv_decision, 
                                                                phv_emoji_after_call = phv_emoji, 
                                                                phv_comment_4_mobi = phv_note, 
                                                                action_username = request.user)
    else:

        pass



    # build a html posts list
    posts_phv_note = record_phv_comment_for_each_mobile.objects.filter(called_mobi = commented_mobile).order_by('app_id', '-date_created')    


    phv_result_table = {
                        "posts_phv_note":posts_phv_note,
                        }


    phv_note_list_html = loader.render_to_string('./en/checklist/module1_gui_phvcl_mobi_comment_list.html', phv_result_table)



    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'phv_note_list_html': phv_note_list_html,'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)








def phv_create_update_company_information(request):

    f1_company_tax_code = request.POST.get('f1_company_tax_code')
    f1_company_name = request.POST.get('f1_company_name')
    riskbox_company_tax_code = request.POST.get('riskbox_company_tax_code')
    und_company_name = request.POST.get('und_company_name')
    phv_company_note = request.POST.get('phv_company_note')
    update_user = request.user.username



    company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code)

    if company_data_in_risk_box.exists():

        post_instance = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code).update(
                                                                                                                    f1employername = f1_company_name,
                                                                                                                    riskboxtaxcode = riskbox_company_tax_code, 
                                                                                                                    employername = und_company_name, 
                                                                                                                    phvnote = phv_company_note,
                                                                                                                    #und_date_created = timezone.now(),
                                                                                                                    phv_date_updated = timezone.now(), 
                                                                                                                    action_username = update_user
                                                                                                                    )

        post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                            f1employername = f1_company_name,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name,
                                                                            phvnote = phv_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            date_created = timezone.now(), 
                                                                            action_username = request.user)


    else:

        post_instance = record_und_newest_note_for_company.objects.create(  f1taxcode = f1_company_tax_code,
                                                                            f1employername = f1_company_name,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name, 
                                                                            phvnote = phv_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            phv_date_updated = timezone.now(), 
                                                                            action_username = request.user)

        post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                            riskboxtaxcode = riskbox_company_tax_code, 
                                                                            employername = und_company_name,
                                                                            phvnote = phv_company_note,
                                                                            #und_date_created = timezone.now(),
                                                                            date_created = timezone.now(), 
                                                                            action_username = request.user)

    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)







def phv_create_send_fiv_data(request):

  app_id_c = request.POST.get('app_id_c')
  username = request.user.username
  username = username.upper()

  button_fiv_curres = request.POST.get('button_fiv_curres')
  button_fiv_permnent = request.POST.get('button_fiv_permnent')
  button_fiv_headoff = request.POST.get('button_fiv_headoff')
  button_fiv_bchoff = request.POST.get('button_fiv_bchoff')
  button_fiv_office = request.POST.get('button_fiv_office')

  exist_curess = request.POST.get('exist_curess')
  exist_headoff = request.POST.get('exist_headoff')
  exist_permnent = request.POST.get('exist_permnent')
  exist_office = request.POST.get('exist_office')
  exist_bchoff = request.POST.get('exist_bchoff')

  checked_curess = request.POST.get('checked_curess')
  checked_headoff = request.POST.get('checked_headoff')
  checked_permnent = request.POST.get('checked_permnent')
  checked_office = request.POST.get('checked_office')
  checked_bchoff = request.POST.get('checked_bchoff')


  householder1_note_curres = request.POST.get('householder1_note_curres')
  householder1_note_headoff = request.POST.get('householder1_note_headoff')
  householder1_note_permnent = request.POST.get('householder1_note_permnent')
  householder1_note_office = request.POST.get('householder1_note_office')
  householder1_note_bchoff = request.POST.get('householder1_note_bchoff')

  Relationship1_note_curres = request.POST.get('Relationship1_note_curres')
  Relationship1_note_headoff = request.POST.get('Relationship1_note_headoff')
  Relationship1_note_permnent = request.POST.get('Relationship1_note_permnent')
  Relationship1_note_office = request.POST.get('Relationship1_note_office')
  Relationship1_note_bchoff = request.POST.get('Relationship1_note_bchoff')

  Room_number_note_curres = request.POST.get('Room_number_note_curres')
  Room_number_note_headoff = request.POST.get('Room_number_note_headoff')
  Room_number_note_permnent = request.POST.get('Room_number_note_permnent')
  Room_number_note_office = request.POST.get('Room_number_note_office')
  Room_number_note_bchoff = request.POST.get('Room_number_note_bchoff')

  Partner_Other_note_curres = request.POST.get('Partner_Other_note_curres')
  Partner_Other_note_headoff = request.POST.get('Partner_Other_note_headoff')
  Partner_Other_note_permnent = request.POST.get('Partner_Other_note_permnent')
  Partner_Other_note_office = request.POST.get('Partner_Other_note_office')
  Partner_Other_note_bchoff = request.POST.get('Partner_Other_note_bchoff')

  Describe_the_ways_note_curres = request.POST.get('Describe_the_ways_note_curres')
  Describe_the_ways_note_headoff = request.POST.get('Describe_the_ways_note_headoff')
  Describe_the_ways_note_permnent = request.POST.get('Describe_the_ways_note_permnent')
  Describe_the_ways_note_office = request.POST.get('Describe_the_ways_note_office')
  Describe_the_ways_note_bchoff = request.POST.get('Describe_the_ways_note_bchoff')

  Note_note_curres = request.POST.get('Note_note_curres')
  Note_note_headoff = request.POST.get('Note_note_headoff')
  Note_note_permnent = request.POST.get('Note_note_permnent')
  Note_note_office = request.POST.get('Note_note_office')
  Note_note_bchoff = request.POST.get('Note_note_bchoff')

  curres = []
  headoff = []
  permnent = []
  office = []
  bchoff = []

  curres.append('CURRES')
  curres.append(exist_curess)
  curres.append(checked_curess)
  curres.append(householder1_note_curres)
  curres.append(Relationship1_note_curres)
  curres.append(Room_number_note_curres)
  curres.append(Partner_Other_note_curres)
  curres.append(Describe_the_ways_note_curres) 
  curres.append(Note_note_curres)
  curres.append(button_fiv_curres)


  headoff.append('HEADOFF')
  headoff.append(exist_headoff)
  headoff.append(checked_headoff)
  headoff.append(householder1_note_headoff)
  headoff.append(Relationship1_note_headoff)
  headoff.append(Room_number_note_headoff)
  headoff.append(Partner_Other_note_headoff)
  headoff.append(Describe_the_ways_note_headoff) 
  headoff.append(Note_note_headoff)
  headoff.append(button_fiv_headoff)


  permnent.append('PERMNENT')
  permnent.append(exist_permnent)
  permnent.append(checked_permnent)
  permnent.append(householder1_note_permnent)
  permnent.append(Relationship1_note_permnent)
  permnent.append(Room_number_note_permnent)
  permnent.append(Partner_Other_note_permnent)
  permnent.append(Describe_the_ways_note_permnent) 
  permnent.append(Note_note_permnent)
  permnent.append(button_fiv_permnent)


  office.append('OFFICE')
  office.append(exist_office)
  office.append(checked_office)
  office.append(householder1_note_office)
  office.append(Relationship1_note_office)
  office.append(Room_number_note_office)
  office.append(Partner_Other_note_office)
  office.append(Describe_the_ways_note_office) 
  office.append(Note_note_office)
  office.append(button_fiv_office)


  bchoff.append('BCHOFF')
  bchoff.append(exist_bchoff)
  bchoff.append(checked_bchoff)
  bchoff.append(householder1_note_bchoff)
  bchoff.append(Relationship1_note_bchoff)
  bchoff.append(Room_number_note_bchoff)
  bchoff.append(Partner_Other_note_bchoff)
  bchoff.append(Describe_the_ways_note_bchoff) 
  bchoff.append(Note_note_bchoff)
  bchoff.append(button_fiv_bchoff)


  fiv_add_type = []
  if button_fiv_curres == '1':
    fiv_add_type.append(curres)
  if button_fiv_headoff == '1':
    fiv_add_type.append(headoff)
  if button_fiv_permnent == '1':
    fiv_add_type.append(permnent)
  if button_fiv_office == '1':
    fiv_add_type.append(office)
  if button_fiv_bchoff == '1':
    fiv_add_type.append(bchoff)



  oracle_sql2 = ("SELECT B.id, TRIM (TO_CHAR (B.LAA_EMI_N,'999,999,999,999')) AS LAA_EMI_N, " +
                   "       TRIM (TO_CHAR (B.LAA_APP_REQ_AMTFIN_N,'999,999,999,999')) AS LAA_APP_REQ_AMTFIN_N, " +
                   "       B.LAA_APP_REQ_EFFRATE_N, B.N_INTEREST_RATE, " +
                   "       B.LAA_APP_LOANDTL_TERM_N, B.CIC_COMMENT, " +
                   "       NVL (B.TOTAL_CIC_AMT,0) AS TOTAL_CIC_AMT, " +
                   "       TRIM(TO_CHAR (NVL(B.TOTAL_CIC_EMI,0), '999,999,999,999')) AS TOTAL_CIC_EMI, " +
                   "       TRIM(TO_CHAR(B.MONTHLY_INCOME,'999,999,999,999')) AS MONTHLY_INCOME, B.DTI, " +
                   "       B.PRIORITY, B.INSPECTORNAME, B.LAA_PRODUCT_ID_C, B.APP_ID_C," +
                   "       B.CUSTOMERNAME, B.DOB, B.CIF_NO, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                   "       B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE, B.CUST_ID_N "
                   "FROM " +
                   "       (SELECT DISTINCT 1 as id, A.LAA_EMI_N, A.LAA_APP_REQ_AMTFIN_N, A.LAA_APP_REQ_EFFRATE_N, A.N_INTEREST_RATE, " +
                   "               A.LAA_APP_LOANDTL_TERM_N, A.CIC_COMMENT, A.TOTAL_CIC_AMT, A.TOTAL_CIC_EMI, " +
                   "               B.MONTHLY_INCOME, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                   "               CASE WHEN B.MONTHLY_INCOME = 0 OR NVL(B.MONTHLY_INCOME,0) = 0 THEN 0 " +
                   "                      ELSE A.LAA_EMI_N / (B.MONTHLY_INCOME - NVL(A.TOTAL_CIC_EMI,0)) END AS DTI, " + 
                   "               A.PRIORITY, A.INSPECTORNAME, A.LAA_PRODUCT_ID_C, A.APP_ID_C, A.CUST_ID_N," +
                   "               B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                   "               B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE "
                   "        FROM   " +
                   "        (      " +
                   "               " +
                   "        SELECT " +   
                   "              LOS_APP_APPLICATIONS.APP_ID_C, LOS_APP_APPLICATIONS.CUST_ID_N, " +
                   "              LOS_APP_APPLICATIONS.LAA_EMI_N, " +
                   "              LOS_APP_APPLICATIONS.LAA_APP_REQ_AMTFIN_N, LOS_APP_APPLICATIONS.LAA_APP_REQ_EFFRATE_N, " +
                   "              LOS_APP_APPLICATIONS.N_INTEREST_RATE, LOS_APP_APPLICATIONS.LAA_APP_LOANDTL_TERM_N, " +
                   "              FIELD_DATA_TXN.FIELD8 AS CIC_COMMENT, FIELD_DATA_TXN.FIELD39 AS TOTAL_CIC_AMT, " +
                   "              TO_NUMBER(REGEXP_REPLACE(TO_CHAR(FIELD_DATA_TXN.FIELD40), '[,]+','')) AS TOTAL_CIC_EMI, " +
                   "              CASE WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'H' THEN 'BANK STATEMENT' " +
                   "                  WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'BL' THEN 'BUSINESS LICENSE' " +
                   "                    WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NBL' THEN 'NO BUSINESS LICENSE' " +
                   "                      WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NO' THEN 'NONE' " +
                   "                        WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'N' THEN 'PAY SLIP' " +
                   "                          END AS PRIORITY, " +
                   "              LOS_APP_APPLICATIONS.LAA_PRODUCT_ID_C, LEA_INSPECTOR_M.INSPECTORNAME " +
                   "        FROM  " +
                   "              MACAS.LOS_APP_APPLICATIONS LOS_APP_APPLICATIONS " +
                   "        LEFT JOIN " +
                   "              MACAS.FIELD_DATA_TXN FIELD_DATA_TXN ON FIELD_DATA_TXN.APP_ID = LOS_APP_APPLICATIONS.APP_ID_C " +
                   "        LEFT JOIN ( " +
                   "                    SELECT A.INSPECTORID, A.INSPECTORNAME " +
                   "                    FROM MAMAS.LEA_INSPECTOR_M A "+
                   "                  )LEA_INSPECTOR_M ON LOS_APP_APPLICATIONS.LAA_SOURCE_INSPECTORID_N = LEA_INSPECTOR_M.INSPECTORID " +
                   "        WHERE LOS_APP_APPLICATIONS.APP_ID_C = TO_CHAR (%s) " +
                   "        )A        " +
                   "                  " +
                   "        LEFT JOIN " +
                   "            (     " +
                   "              SELECT  A.CUST_ID_N, A.CUSTOMERNAME, A.DOB, A.AGE, A.CIF_NO, " +
                   "                      CASE WHEN A.SEX = 'F' THEN 'FEMALE' WHEN A.SEX = 'M' THEN 'MALE' END AS GENDER ," +
                   "                      A.MARITAL_STATUS, "
                   "                      A.SPOUSENAME, A.SPOUSE_ID_C, A.MONTHLY_INCOME, " +
                   "                      A.OTHER_EMPLOYER_NAME_C AS COMPANY_NAME, A.NCM_LABOUR_CARDNO_C AS TAX_CODE " +
                   "              FROM MACAS.NBFC_CUSTOMER_M A " +
                   "            )B ON B.CUST_ID_N = A.CUST_ID_N " +
                   "        WHERE A.APP_ID_C = TO_CHAR (%s))B " ) 


  queryset_list2  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql2,[app_id_c, app_id_c])


  f1_cust_id=""


  for app_infor in queryset_list2:
    f1_cust_id = app_infor.cust_id_n
    loan_amount = app_infor.laa_app_req_amtfin_n
    client_name = app_infor.customername
    year_of_birth = app_infor.age
    id_card = app_infor.cif_no
    gender = app_infor.gender
    marial_status = app_infor.marital_status
    spouse_name = app_infor.spousename
    spouse_company = ''
    company_name = app_infor.company_name



  oracle_sql4 = (
              "SELECT 1 as id," + 
              "       A.mobile, B.spouse_mobile, A.addresstype " +
              "FROM  " +
              "       MACAS.NBFC_ADDRESS_M A " +
              "LEFT JOIN " +
              "     ( " +
              "       SELECT A.CUST_ID_N, MAX(A.PHONE1) KEEP (DENSE_RANK FIRST ORDER BY rownum) AS spouse_mobile  " +
              "       FROM MACAS.NBFC_REFERENCE_M A " +
              "       WHERE A.CUST_ID_N = to_char(%s) AND A.REF_RELATION = 'WH' " +
              "       GROUP BY A.CUST_ID_N " +
              "     )B ON B.CUST_ID_N = A.BPID " +
              "WHERE A.BPID = to_char(%s) AND A.ADDRESSTYPE = %s "
            )


  queryset_list4  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql4,[f1_cust_id, f1_cust_id, 'CURRES'])


  for mobile in queryset_list4:
    addresstype = mobile.addresstype
    client_phone_number = mobile.mobile
    spouse_phone_number = mobile.spouse_mobile
    owning_status = ''
    householder = ''
    relationship = '' 


  uw_request_note = ''
  phv_final_request_fiv_date0 = ''
  phv_final_request_fiv_day_finish0 = ''
  phv_final_request_fiv_day_finish = ''


  from checklist.functions.function_create_new_fiv_request import user_create_new_fiv_request


  for address in fiv_add_type:


    user_create_new_fiv_request(app_id_c, f1_cust_id, address[0], address[2], address[3], address[4], address[5], address[6], address[7],
                                address[8], username, loan_amount, client_name, year_of_birth, id_card, gender, marial_status, spouse_name,
                                spouse_company, client_phone_number, spouse_phone_number, owning_status, householder,relationship, company_name)



    phv_final_request_fiv_date = record_phv_sending_fiv_request.objects.filter(app_id = app_id_c, type_of_address= address[0], send_fiv= 'true').order_by('-date_request')[:1]



    for request_date in phv_final_request_fiv_date:
      phv_final_request_fiv_day_finish0 = request_date.date_request.strftime("%Y-%m-%d %I:%M%p")

      

    phv_final_request_fiv_date0 = str(phv_final_request_fiv_day_finish0)
    phv_final_request_fiv_day_finish = "<font color=""red"">" + str(phv_final_request_fiv_day_finish0) + "</font>"

    phv_final_request_fiv_content = record_phv_sending_fiv_request_hist.objects.filter(app_id = app_id_c, type_of_address= address[0], send_fiv= 'true').order_by('-date_create')[:1]


    for item in phv_final_request_fiv_content:
      fi_content =  ( "Dear Mr. Phụng," + " \n " +
                      "App ID: " + str(item.app_id) + " \n " + 
                      "Address Type: " + str(item.type_of_address) + " \n " +
                      "Requested by: " + str(item.requested_by) + " \n " +
                      "householder1: " + str(item.householder1) + " \n " +
                      "relationship1: " + str(item.relationship1) + " \n " +
                      "room_number: " + str(item.room_number) + " \n " +
                      "partner_other: " + str(item.partner_other) + " \n " +
                      "describe_the_ways: " + str(item.describe_the_ways) + " \n " +
                      "note: " + str(item.note) + " \n " +
                      "make date: " + str(item.date_create) + " \n "+
                      "Many Thanks and Best Regards" + " \n "+
                      "Risk Team --- Vui lòng bỏ qua email này" )
      if item.note:
        uw_request_note = str(item.note)





  def send_fi_email_request():

    from django.core.mail import send_mail

    email_subject = "Test message: FI request - " + str(app_id_c) + " " + str(phv_final_request_fiv_day_finish0)

    #send_list = [ 'thanhnguyen.nguyen@mafc.com.vn',
    #              'minhdat.phung@mafc.com.vn',
    #              'phuc.tran@mafc.com.vn',
    #              'vananh.vu@mafc.com.vn',
    #              'tuyet.tran@mafc.com.vn',
    #              'dao.tran@mafc.com.vn',
    #              'ngocchau.nguyen@mafc.com.vn',
    #              'phuocthinh.tran@mafc.com.vn',
    #              'thuytrang.tran@mafc.com.vn',
    #              'lamthu.nguyen@mafc.com.vn',
    #              'phung.dinh@mafc.com.vn',
    #              'hangnga.truong@mafc.com.vn']

    send_list = [ 'phung.dinh@mafc.com.vn']

    send_mail(email_subject, fi_content, 'fi.system.admin@mafc.com.vn', send_list)


  if uw_request_note == 'CAN1' or uw_request_note == 'CAN2':

    if (int(button_fiv_curres) > 0 and  checked_curess == 'true'):
      send_fi_email_request()
      phv_final_request_fiv_day_finish = "Báo HỦY đã được gửi"
    elif (int(button_fiv_permnent) > 0 and checked_permnent == 'true'):
      send_fi_email_request()
      phv_final_request_fiv_day_finish = "Báo HỦY đã được gửi"
    elif (int(button_fiv_headoff) > 0 and checked_headoff == 'true'):
      send_fi_email_request()
      phv_final_request_fiv_day_finish = "Báo HỦY đã được gửi"
    elif (int(button_fiv_bchoff) > 0 and checked_bchoff == 'true'):
      send_fi_email_request()
      phv_final_request_fiv_day_finish = "Báo HỦY đã được gửi"
    elif (int(button_fiv_office) > 0 and checked_office == 'true'):
      send_fi_email_request()
      phv_final_request_fiv_day_finish = "Báo HỦY đã được gửi"


  save_complete = True


  # package output data and return it as a JSON object
  output_data = { 'save_complete': save_complete, 
                  'phv_final_request_fiv_date': phv_final_request_fiv_date0,
                  'phv_final_request_fiv_day_finish' : phv_final_request_fiv_day_finish,
                  'button_fiv_curres': button_fiv_curres,
                  'button_fiv_permnent': button_fiv_permnent,
                  'button_fiv_headoff': button_fiv_headoff,
                  'button_fiv_bchoff': button_fiv_bchoff,
                  'button_fiv_office': button_fiv_office}

  # package output data and return it as a JSON object
  return JsonResponse(output_data)






def phv_send_request_pcb(request):

  from checklist.models import record_pcb_matched_customer_basic_infor, record_pcb_customer_loan_infor
  from checklist.models import record_pcb_customer_mobile_phone
  from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

  app_id_c = request.POST.get('app_id_c')
  first_click = request.POST.get('first_click')
  second_click = request.POST.get('second_click')


  repeatcheck=''

  record = record_pcb_matched_customer_basic_infor.objects.filter(app_id=app_id_c).exclude(found__in=("pcb error","not found")).count()


  if record == 0:
    repeatcheck = 'NOT YET'
  else:
    repeatcheck = 'YES'

  #try:
  #  record = record_pcb_matched_customer_basic_infor.objects.get(app_id=app_id_c).exclude(loantype="card")
  #except record_pcb_matched_customer_basic_infor.DoesNotExist:
  #  repeatcheck = 'NOT YET'
  #except record_pcb_matched_customer_basic_infor.MultipleObjectsReturned:
  #  repeatcheck = 'YES'


  if repeatcheck == 'NOT YET':

    from checklist.functions.function_pcb_a2a_connect import send_request_to_pcb

    pcb_return = send_request_to_pcb(app_id_c)

    if pcb_return['error'] == 'pcb error' or pcb_return['error'] == 'unknow error':

      feedback_error_code = pcb_return['error_code']
      feedback_error_desc = pcb_return['error_desc']
      pcb_status = ('''
                    <p> pcp error, please contact pcp for fixing! </p>
                    <p> error code: %s </p>
                    <p> error description: %s </p>
                    ''' % (feedback_error_code, feedback_error_desc ))

    elif pcb_return['found'] == 'not found':

      pcb_status = '<p> không tìm thấy trên pcb </p>'

    elif pcb_return['found'] == 'found':

      pcb_status = ''

    else:
      pcb_status = '<p> please contact risk for investigating! </p>'

    pcb_customer_infor = record_pcb_matched_customer_basic_infor.objects.filter(id=pcb_return.get('id'))
    pcb_customer_score = record_pcb_matched_customer_basic_infor.objects.filter(id=pcb_return.get('id'))
    pcb_customer_loan = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=pcb_return.get('id')).exclude(loantype="card")
    pcb_customer_card = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=pcb_return.get('id'), loantype="card")

    pcb_result = {

      "pcb_customer_infor": pcb_customer_infor,
      "pcb_customer_score": pcb_customer_score,
      "pcb_customer_loan": pcb_customer_loan,
      "pcb_customer_card": pcb_customer_card

    }

    pcb_customer_infor_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_cux.html', pcb_result)
    pcb_customer_score_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_score.html', pcb_result)
    pcb_customer_loan_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_loan.html', pcb_result)
    pcb_customer_card_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_card.html', pcb_result)

    pcb_check_button_reconfirm = ''

  elif repeatcheck == 'YES' and first_click == 'first_click' and second_click == '' :

    pcb_status = '<p> click thêm 1 lần để hỏi tin pcb </>'

    pcb_customer_infor = record_pcb_matched_customer_basic_infor.objects.filter(app_id=app_id_c).order_by('-id')[:1]
    pcb_customer_score = record_pcb_matched_customer_basic_infor.objects.filter(app_id=app_id_c).order_by('-id')[:1]

    for i in pcb_customer_infor:
      checkid = i.id

    pcb_customer_loan = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=checkid).exclude(loantype="card")
    pcb_customer_card = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=checkid, loantype="card")
    
    pcb_result = {

      "pcb_customer_infor": pcb_customer_infor,
      "pcb_customer_score": pcb_customer_score,
      "pcb_customer_loan": pcb_customer_loan,
      "pcb_customer_card": pcb_customer_card

    }


    pcb_customer_card_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_card.html', pcb_result)
    pcb_customer_loan_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_loan.html', pcb_result)
    pcb_customer_infor_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_cux.html', pcb_result)
    pcb_customer_score_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_score.html', pcb_result)

    pcb_check_button_reconfirm = '''

    <button type="button" class="btn btn-info" style="width: 100%" id="button_send_request_to_pcb_again">Click to confirm</button>
    <div class="progress">
        <div class="progress-bar progress-bar-striped bg-warning progress-bar-animated" role="progressbar" style="width: 100%; height: 50%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
    </div>

    '''


  elif repeatcheck == 'YES' and first_click == '' and second_click == 'second_click' :

    from checklist.functions.function_pcb_a2a_connect import send_request_to_pcb

    pcb_return = send_request_to_pcb(app_id_c)

    if pcb_return['error'] == 'pcb error' or pcb_return['error'] == 'unknow error':
      feedback_error_code = pcb_return['error_code']
      feedback_error_desc = pcb_return['error_desc']
      pcb_status = ('''
                    <p> pcp error, please contact pcp for fixing! </p>
                    <p> error code: %s </p>
                    <p> error description: %s </p>
                    ''' % (feedback_error_code, feedback_error_desc ))
    elif pcb_return['found'] == 'not found':

      pcb_status = '<p> không tìm thấy trên pcb </p>'

    elif pcb_return['found'] == 'found':

      pcb_status = ''

    else:
      pcb_status = '<p> please contact risk for investigating! </p>'


    print(pcb_return)

    pcb_customer_infor = record_pcb_matched_customer_basic_infor.objects.filter(id=pcb_return.get('id'))
    pcb_customer_score = record_pcb_matched_customer_basic_infor.objects.filter(id=pcb_return.get('id'))
    pcb_customer_loan = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=pcb_return.get('id')).exclude(loantype="card")
    pcb_customer_card = record_pcb_customer_loan_infor.objects.filter(matched_customer_id=pcb_return.get('id'), loantype="card")

    pcb_result = {

      "pcb_customer_infor": pcb_customer_infor,
      "pcb_customer_score": pcb_customer_score,
      "pcb_customer_loan": pcb_customer_loan,
      "pcb_customer_card": pcb_customer_card

    }

    pcb_customer_infor_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_cux.html', pcb_result)
    pcb_customer_score_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_score.html', pcb_result)
    pcb_customer_loan_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_loan.html', pcb_result)
    pcb_customer_card_temp = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_card.html', pcb_result)

    pcb_check_button_reconfirm = ''



  #button_check_pcb = loader.render_to_string('./en/checklist/module1_gui_phvcl_pcb_button.html', repeatcheck)
  #phv_note_list_html = loader.render_to_string('./en/checklist/module1_gui_phvcl_mobi_comment_list.html', phv_result_table)

  save_complete = True
  output_data = { 

  'save_complete': save_complete,
  'pcb_status': pcb_status,
  'pcb_customer_infor_temp': pcb_customer_infor_temp,
  'pcb_customer_score_temp': pcb_customer_score_temp,
  'pcb_customer_loan_temp': pcb_customer_loan_temp,
  'pcb_customer_card_temp': pcb_customer_card_temp,
  'pcb_check_button_reconfirm': pcb_check_button_reconfirm

  }

  # package output data and return it as a JSON object
  return JsonResponse(output_data)
