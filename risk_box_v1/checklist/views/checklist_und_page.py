# - * - Coding: utf -8 - * -
from django.http import HttpResponse, HttpResponseRedirect

from django.shortcuts import render

from checklist.models import record_datachecker_result, record_phv_result_cl, record_phv_comment_for_each_mobile
from checklist.models import record_datachecker_pending_defer_history, record_phv_pending_defer_history, record_phv_dti_calculation
from checklist.models import record_und_result_cl, record_und_pending_defer_history
from checklist.models import record_und_newest_note_for_company, record_und_note_history_for_company
from checklist.models import record_phv_sending_fiv_request
from dedupe_page.models import dedupe_id_model, black_list_id_history_model, black_list_on_mafc_employee_dedupe_model
from dedupe_page.models import dedupe_mobile_cux_model, dedupe_mobile_ref_model
from checklist.models import record_nice_score_history_for_app

from django.db.models import Q

from django.urls import reverse
from django.core import serializers

import datetime
import json
import re

from django.utils import timezone

from django.template import loader
from django.http import JsonResponse



# Create your views here.
def page(request):

    if request.user.is_authenticated:
        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
        
        #22 là UND, 21 là UW POS
        if 22 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        elif "UW_SUP_TL" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))

    else:
        return HttpResponseRedirect(reverse("login_page"))



    #if request.user.is_authenticated():
    #    user_groups = request.user.groups.values_list('name', flat=True)
    #    if "UW_POS" in user_groups or"UND" in user_groups:
    #        pass
            #return HttpResponseRedirect(reverse("login_page"))
    #    else:
    #        return HttpResponseRedirect(reverse("login_page"))
    #else:
    #    return HttpResponseRedirect(reverse("login_page"))

        

    report_error = "Please input mobilephone number!"
    return render(request, './en/checklist/checklist_und.html',{  "report_error":report_error, })








def get_app_data_from_finnone_before_using_funtion_of_checklist_und(request):

    if request.user.is_authenticated:
        user_groups_id = request.user.groups.values_list('id', flat=True)
        user_groups = request.user.groups.values_list('name', flat=True)
        
        #22 là UND, 21 là UW POS
        if 22 in user_groups_id or 21 in user_groups_id:
            pass

        elif "SUPER_ADMIN" in user_groups:
            pass

        elif "UW_SUP_TL" in user_groups:
            pass

        else:
            return HttpResponseRedirect(reverse("login_page"))

    else:
        return HttpResponseRedirect(reverse("login_page"))


    #if request.user.is_authenticated():
    #    user_groups = request.user.groups.values_list('name', flat=True)
    #    if "UW_POS" in user_groups or"UND" in user_groups:
    #        pass
            #return HttpResponseRedirect(reverse("login_page"))
    #    else:
    #        return HttpResponseRedirect(reverse("login_page"))
    #else:
    #    return HttpResponseRedirect(reverse("login_page"))




    get_app_id_for_checklist_und = request.GET.get('app_id_c')


    #lấy các số điện thoại cần search cho phv check list

    oracle_sql = ("SELECT C.id, E.APP_ID_C, C.BPID, C.NAME, C.RELATION, C.MOBILE " +
                  "FROM (" +
                  "SELECT " +
                  "      1 as id, A.BPID, B.CUSTOMERNAME AS NAME, 'CUX' AS RELATION, A.MOBILE "+
                  "FROM "+
                  "      MACAS.NBFC_ADDRESS_M A, "+
                  "      MACAS.NBFC_CUSTOMER_M B "+
                  "WHERE "+
                  "      A.BPID = B.CUST_ID_N AND "+
                  "      A.ADDRESSTYPE = 'CURRES' "+    
                  "UNION ALL "+
                  "SELECT "+
                  "      2 AS ID, B.CUST_ID_N, B.REFERENCE_NAME, B.REF_RELATION, "+
                  "      CASE WHEN B.STDISD IS NULL THEN B.PHONE1 ELSE B.STDISD END AS REF_MOBILE "+
                  "FROM "+
                  "      MACAS.NBFC_REFERENCE_M B )C "+
                  "LEFT JOIN MACAS.LOS_APP_APPLICATIONS E ON E.CUST_ID_N = C.BPID "+
                  "WHERE E.APP_ID_C = TO_CHAR (%s) ")

    queryset_list  = dedupe_mobile_cux_model.objects.using('finnonedb').raw(oracle_sql,[get_app_id_for_checklist_und])


    #bên cạnh lấy thông tin các số điện thoại, cần phải lấy cả note của số điện thoại
    #các câu lệnh sau để lấy comment cho từng số điện thoại

    phone_list = []

    for item in queryset_list:      

        phone_list.append(item.mobile)
        f1_cust_id = item.bpid

    posts_phv_note = record_phv_comment_for_each_mobile.objects.filter(called_mobi__in = phone_list ).order_by('app_id', '-date_created')       




    context = {     
                "get_infor_for_dedupe_mobile": queryset_list,
                "posts_phv_note": posts_phv_note,
                }

    save_complete = True

    # build a html posts list
    posts_html00 = loader.render_to_string('./en/checklist/module1_phv_get_all_mobile_phone.html', context)

    posts_html01 = loader.render_to_string('./en/checklist/module2_gui_undcl_view_mobi_comment.html', context)




    if len(get_app_id_for_checklist_und) >=5:
        button_get_app = True
    else:
        button_get_app=''



    #lấy thông tin cho các trường thuộc dedupe id trên check list của phv

    #oracle_sql1 = ("SELECT DISTINCT 1 as id, %s as APP_ID_C, B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
    #                  "               B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE" + 
    #                  "   FROM RISK_FRAUD_TAKE_CUS_IN_PROD B WHERE B.CUST_ID_N IN " +
    #                  "  (SELECT A.CUST_ID_N FROM RISK_FRAUD_TAKE_CUS_ID_PROD A WHERE A.APP_ID_C = TO_CHAR (%s) )")


    oracle_sql2 = ("SELECT B.id, TRIM (TO_CHAR (B.LAA_EMI_N,'999,999,999,999')) AS LAA_EMI_N, " +
                   "       TRIM (TO_CHAR (B.LAA_APP_REQ_AMTFIN_N,'999,999,999,999')) AS LAA_APP_REQ_AMTFIN_N, " +
                   "       B.LAA_APP_REQ_EFFRATE_N, B.N_INTEREST_RATE, " +
                   "       B.LAA_APP_LOANDTL_TERM_N, B.CIC_COMMENT, " +
                   "       NVL (B.TOTAL_CIC_AMT,0) AS TOTAL_CIC_AMT, " +
                   "       TRIM(TO_CHAR (NVL(B.TOTAL_CIC_EMI,0), '999,999,999,999')) AS TOTAL_CIC_EMI, " +
                   "       TRIM(TO_CHAR(B.MONTHLY_INCOME,'999,999,999,999')) AS MONTHLY_INCOME, B.DTI, " +
                   "       B.PRIORITY, B.INSPECTORNAME, B.LAA_PRODUCT_ID_C, B.APP_ID_C," +
                   "       B.CUSTOMERNAME, B.DOB, B.CIF_NO, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                   "       B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE "
                   "FROM " +
                   "       (SELECT DISTINCT 1 as id, A.LAA_EMI_N, A.LAA_APP_REQ_AMTFIN_N, A.LAA_APP_REQ_EFFRATE_N, A.N_INTEREST_RATE, " +
                   "               A.LAA_APP_LOANDTL_TERM_N, A.CIC_COMMENT, A.TOTAL_CIC_AMT, A.TOTAL_CIC_EMI, " +
                   "               B.MONTHLY_INCOME, B.AGE, B.GENDER, B.MARITAL_STATUS, " +
                   "               CASE WHEN B.MONTHLY_INCOME = 0 OR NVL(B.MONTHLY_INCOME,0) = 0 THEN 0 " +
                   "                      ELSE A.LAA_EMI_N / (B.MONTHLY_INCOME - NVL(A.TOTAL_CIC_EMI,0)) END AS DTI, " + 
                   "               A.PRIORITY, A.INSPECTORNAME, A.LAA_PRODUCT_ID_C, A.APP_ID_C, " +
                   "               B.CUSTOMERNAME, to_char (B.DOB, 'dd/mm/yyyy') as dob, B.CIF_NO, " +
                   "               B.SPOUSENAME, B.SPOUSE_ID_C, B.COMPANY_NAME, B.TAX_CODE "
                   "        FROM   " +
                   "        (      " +
                   "               " +
                   "        SELECT " +   
                   "              LOS_APP_APPLICATIONS.APP_ID_C, LOS_APP_APPLICATIONS.CUST_ID_N, " +
                   "              LOS_APP_APPLICATIONS.LAA_EMI_N, " +
                   "              LOS_APP_APPLICATIONS.LAA_APP_REQ_AMTFIN_N, LOS_APP_APPLICATIONS.LAA_APP_REQ_EFFRATE_N, " +
                   "              LOS_APP_APPLICATIONS.N_INTEREST_RATE, LOS_APP_APPLICATIONS.LAA_APP_LOANDTL_TERM_N, " +
                   "              FIELD_DATA_TXN.FIELD8 AS CIC_COMMENT, FIELD_DATA_TXN.FIELD39 AS TOTAL_CIC_AMT, " +
                   "              TO_NUMBER(REGEXP_REPLACE(TO_CHAR(FIELD_DATA_TXN.FIELD40), '[,]+','')) AS TOTAL_CIC_EMI, " +
                   "              CASE WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'H' THEN 'BANK STATEMENT' " +
                   "                  WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'BL' THEN 'BUSINESS LICENSE' " +
                   "                    WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NBL' THEN 'NO BUSINESS LICENSE' " +
                   "                      WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'NO' THEN 'NONE' " +
                   "                        WHEN LOS_APP_APPLICATIONS.LAA_APPLICATION_PRIORITY_C = 'N' THEN 'PAY SLIP' " +
                   "                          END AS PRIORITY, " +
                   "              LOS_APP_APPLICATIONS.LAA_PRODUCT_ID_C, LEA_INSPECTOR_M.INSPECTORNAME " +
                   "        FROM  " +
                   "              MACAS.LOS_APP_APPLICATIONS LOS_APP_APPLICATIONS " +
                   "        LEFT JOIN " +
                   "              MACAS.FIELD_DATA_TXN FIELD_DATA_TXN ON FIELD_DATA_TXN.APP_ID = LOS_APP_APPLICATIONS.APP_ID_C " +
                   "        LEFT JOIN ( " +
                   "                    SELECT A.INSPECTORID, A.INSPECTORNAME " +
                   "                    FROM MAMAS.LEA_INSPECTOR_M A "+
                   "                  )LEA_INSPECTOR_M ON LOS_APP_APPLICATIONS.LAA_SOURCE_INSPECTORID_N = LEA_INSPECTOR_M.INSPECTORID " +
                   "        WHERE LOS_APP_APPLICATIONS.APP_ID_C = TO_CHAR (%s) " +
                   "        )A        " +
                   "                  " +
                   "        LEFT JOIN " +
                   "            (     " +
                   "              SELECT  A.CUST_ID_N, A.CUSTOMERNAME, A.DOB, A.AGE, A.CIF_NO, " +
                   "                      CASE WHEN A.SEX = 'F' THEN 'FEMALE' WHEN A.SEX = 'M' THEN 'MALE' END AS GENDER ," +
                   "                      A.MARITAL_STATUS, "
                   "                      A.SPOUSENAME, A.SPOUSE_ID_C, A.MONTHLY_INCOME, " +
                   "                      A.OTHER_EMPLOYER_NAME_C AS COMPANY_NAME, A.NCM_LABOUR_CARDNO_C AS TAX_CODE " +
                   "              FROM MACAS.NBFC_CUSTOMER_M A " +
                   "            )B ON B.CUST_ID_N = A.CUST_ID_N " +
                   "        WHERE A.APP_ID_C = TO_CHAR (%s))B " )

    #queryset_list1  = dedupe_id_model.objects.using('datadb').raw(oracle_sql1,[get_app_id_for_checklist_und, get_app_id_for_checklist_und])
    queryset_list2  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql2,[get_app_id_for_checklist_und, get_app_id_for_checklist_und])




    oracle_sql3 = ( "SELECT 1 as id," +
                    "      A.address1, B.zipdesc, A.city_lms, A.statedesc, A.addresstype, A.mobile " +
                    "FROM " +
                    "      MACAS.NBFC_ADDRESS_M A " +
                    "LEFT JOIN " +
                    "    (SELECT zipcode, zipdesc FROM macas.nbfc_city_zipcode_m) B ON A.zipcode = B.zipcode " +  
                    "WHERE A.BPID = to_char (%s) " )

    queryset_list3  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql3,[f1_cust_id])


    

    phv_final_request_fiv_note_curres = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'CURRES').order_by('-date_request')[:1]
    phv_final_request_fiv_note_curres_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'CURRES', send_fiv ='true').order_by('-date_request')[:1]

    phv_final_request_fiv_note_permnent = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'PERMNENT').order_by('-date_request')[:1]
    phv_final_request_fiv_note_permnent_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'PERMNENT', send_fiv ='true').order_by('-date_request')[:1]


    phv_final_request_fiv_note_headoff = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'HEADOFF').order_by('-date_request')[:1]
    phv_final_request_fiv_note_headoff_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'HEADOFF', send_fiv ='true').order_by('-date_request')[:1]


    phv_final_request_fiv_note_bchoff = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'BCHOFF').order_by('-date_request')[:1]
    phv_final_request_fiv_note_bchoff_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'BCHOFF', send_fiv ='true').order_by('-date_request')[:1]


    phv_final_request_fiv_note_office = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'OFFICE').order_by('-date_request')[:1]
    phv_final_request_fiv_note_office_fiv = record_phv_sending_fiv_request.objects.filter(app_id = get_app_id_for_checklist_und, type_of_address= 'OFFICE', send_fiv ='true').order_by('-date_request')[:1]



    oracle_sql4 = (
                    "SELECT 1 as id," + 
                    "       A.mobile, B.spouse_mobile " +
                    "FROM  " +
                    "       MACAS.NBFC_ADDRESS_M A " +
                    "LEFT JOIN " +
                    "     ( " +
                    "       SELECT A.CUST_ID_N, MAX(A.PHONE1) KEEP (DENSE_RANK FIRST ORDER BY rownum) AS spouse_mobile  " +
                    "       FROM MACAS.NBFC_REFERENCE_M A " +
                    "       WHERE A.CUST_ID_N = to_char(%s) AND A.REF_RELATION = 'WH' " +
                    "       GROUP BY A.CUST_ID_N " +
                    "     )B ON B.CUST_ID_N = A.BPID " +
                    "WHERE A.BPID = to_char(%s) AND A.ADDRESSTYPE = 'CURRES' "
                  )

    queryset_list4  = dedupe_id_model.objects.using('finnonedb').raw(oracle_sql4,[f1_cust_id,f1_cust_id])





    from checklist.functions.function_get_nice_score import get_and_store_nice_score, get_nice_score

    user_group = str(request.user.groups.values_list('name', flat=True))

    nice_score_last_try = get_and_store_nice_score( get_app_id_for_dedupe_phone = get_app_id_for_checklist_und, 
                                                    username = request.user.username,
                                                    record_team = user_group)

    nice_score_first_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_checklist_und).order_by('record_date')[:1]



    context1 = {     
                "basic_app_infor": queryset_list2,
                "basic_loan_infor": queryset_list2,
                "basic_mobile_infor": queryset_list3,
                "basic_mobile_num": queryset_list4,
                "button_get_app": button_get_app,
                "nice_score_last_try": nice_score_last_try,
                "nice_score_first_try": nice_score_first_try
                }


    if phv_final_request_fiv_note_curres.exists():
      context1.update({'phv_final_request_fiv_note_curres':phv_final_request_fiv_note_curres})
      context1.update({'phv_final_request_fiv_note_curres_fiv':phv_final_request_fiv_note_curres_fiv})


    if phv_final_request_fiv_note_permnent.exists():
      context1.update({'phv_final_request_fiv_note_permnent':phv_final_request_fiv_note_permnent})
      context1.update({'phv_final_request_fiv_note_permnent_fiv':phv_final_request_fiv_note_permnent_fiv})


    if phv_final_request_fiv_note_headoff.exists():
      context1.update({'phv_final_request_fiv_note_headoff':phv_final_request_fiv_note_headoff})
      context1.update({'phv_final_request_fiv_note_headoff_fiv':phv_final_request_fiv_note_headoff_fiv})


    if phv_final_request_fiv_note_bchoff.exists():
      context1.update({'phv_final_request_fiv_note_bchoff':phv_final_request_fiv_note_bchoff})
      context1.update({'phv_final_request_fiv_note_bchoff_fiv':phv_final_request_fiv_note_bchoff_fiv})


    if phv_final_request_fiv_note_office.exists():
      context1.update({'phv_final_request_fiv_note_office':phv_final_request_fiv_note_office})
      context1.update({'phv_final_request_fiv_note_office_fiv':phv_final_request_fiv_note_office_fiv})


    posts_html1 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_items_cux.html', context1)
    posts_html2 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_items_spouse.html', context1)
    # load module này chủ yếu chỉ để hiển thị giao diện, không có giá trị
    posts_html3 = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context1)

    # module show tab check list của app tại từng bước
    posts_html4 = loader.render_to_string('./en/checklist/module2_tab_und_cl.html', context1)
    #posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
    posts_html6 = loader.render_to_string('./en/checklist/module1_gui_phvcl_show_basic_loan_infor.html', context1)

    posts_html9 = loader.render_to_string('./en/checklist/module1_gui_phvcl_show_fiv_form_data.html', context1)





    #phần này là giải thuật quyết định khi nào lấy data của f1 khi nào lấy data của risk box cho mục tính DTI
    dti_result_in_risk_box = record_phv_dti_calculation.objects.filter(app_id=get_app_id_for_checklist_und).order_by('-date_created')[:1]

    riskbox_dti = {
                "basic_loan_infor": dti_result_in_risk_box,
    }


    if dti_result_in_risk_box.exists():

        posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
        posts_html7 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti_result.html', riskbox_dti)
        #print(posts_html7)

    else:

        posts_html5 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti.html', context1)
        posts_html7 = loader.render_to_string('./en/checklist/module1_gui_phvcl_calculate_dti_result.html', context1)




    #phần này là giải thuật quyết định khi nào lấy data của f1 khi nào lấy data của risk box cho mục thông tin công ty
    v_f1taxcode = ''
    for tax in queryset_list2:
        v_f1taxcode = tax.tax_code
        
    company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = v_f1taxcode)

    und_final_note_company = {
                "riskox_company_infor":company_data_in_risk_box,
                "basic_app_infor": queryset_list2,
                }


    if company_data_in_risk_box.exists():

        posts_html8 = loader.render_to_string('./en/checklist/module2_gui_show_company_info_update.html', und_final_note_company)

    else:

        posts_html8 = loader.render_to_string('./en/checklist/module2_gui_show_company_info.html', context1)






    #load module thể hiện final status của dc
    dc_note_status = record_datachecker_pending_defer_history.objects.filter(app_id=get_app_id_for_checklist_und).order_by('-date_created')[:1]

    dc_final_note_status = {
                            "dc_note_status":dc_note_status,
                            }

    dc_final_note_status_html = loader.render_to_string('./en/checklist/module_gui_dccl_dc_note_status.html', dc_final_note_status)


    posts_id_card = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='ID CARD').order_by('paper_type', '-date_created')
    posts_family_book = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
    posts_work_proof = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
    posts_income_proof = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
    posts_current_address = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
    posts_cib = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='CIB').order_by('paper_type', '-date_created')
    posts_tax = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='TAX').order_by('paper_type', '-date_created')
    posts_dedupe = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='DUPE').order_by('paper_type', '-date_created')
    posts_defer_note = record_datachecker_result.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='DEFER').order_by('paper_type', '-date_created')

    dc_result_table = {
                        "posts_id_card":posts_id_card,
                        "posts_family_book":posts_family_book,
                        "posts_work_proof":posts_work_proof,
                        "posts_income_proof":posts_income_proof,
                        "posts_current_address":posts_current_address,
                        "posts_cib":posts_cib,
                        "posts_tax":posts_tax,
                        "posts_dedupe":posts_dedupe,
                        "posts_defer_note":posts_defer_note,
                        }

    checklist_result_html = loader.render_to_string('./en/checklist/module_show_data_checker_result.html', dc_result_table)



    # build a html posts for phv main items in check list
    posts_cic_detail = record_phv_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, check_type='CIC DETAIL').order_by('check_type', '-date_created')
    posts_payment = record_phv_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, check_type='PAYMENT').order_by('check_type', '-date_created')
    posts_home = record_phv_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, check_type='HOME').order_by('check_type', '-date_created')
    posts_work = record_phv_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, check_type='WORK').order_by('check_type', '-date_created')
    posts_defer_note = record_phv_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, check_type='DEFER').order_by('check_type', '-date_created')

    phv_result_table = {
                        "posts_cic_detail":posts_cic_detail,
                        "posts_payment":posts_payment,
                        "posts_home":posts_home,
                        "posts_work":posts_work,
                        "posts_defer_note": posts_defer_note,
                        }


    phv_checklist_result_html = loader.render_to_string('./en/checklist/module1_show_phv_result.html', phv_result_table)




    # phần này nhằm lấy lại trạng thái cuối cùng của app do phv note
    phv_note_status = record_phv_pending_defer_history.objects.filter(app_id=get_app_id_for_checklist_und).order_by('-date_created')[:1]

    phv_final_note_status = {
                            "phv_note_status":phv_note_status,
                            }

    phv_final_note_status_html = loader.render_to_string('./en/checklist/module1_gui_phvcl_phv_note_status.html', phv_final_note_status)






    # build a html posts list
    posts_und_id_card = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='ID CARD').order_by('paper_type', '-date_created')
    posts_und_family_book = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
    posts_und_resident_proof = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='RESIDENT PROOF').order_by('paper_type', '-date_created')
    posts_und_work_proof = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
    posts_und_income_proof = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
    posts_und_current_address = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
    posts_und_current_job = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='CURRENT JOB').order_by('paper_type', '-date_created')
    posts_und_current_debt = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='CURRENT DEBT').order_by('paper_type', '-date_created')
    posts_und_payment_ability = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='PAYMENT ABILITY').order_by('paper_type', '-date_created')
    posts_und_other_evaluation = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='OTHER EVALUATION').order_by('paper_type', '-date_created')
    posts_und_approve_la = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='APPROVE LOAN AMOUNT').order_by('paper_type', '-date_created')
    posts_und_final_tennor = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='FINAL TENNOR').order_by('paper_type', '-date_created')
    posts_und_decrease_la = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='REASON DECREASE LOAN AMOUNT').order_by('paper_type', '-date_created')


    posts_und_defer = record_und_result_cl.objects.filter(app_id=get_app_id_for_checklist_und, paper_type='UW_UND_DEFER').order_by('paper_type', '-date_created')

    und_result_table = {
                        "posts_und_id_card":posts_und_id_card,
                        "posts_und_family_book":posts_und_family_book,
                        "posts_und_resident_proof":posts_und_resident_proof,
                        "posts_und_work_proof":posts_und_work_proof,
                        "posts_und_income_proof":posts_und_income_proof,
                        "posts_und_current_address":posts_und_current_address,
                        "posts_und_current_job":posts_und_current_job,
                        "posts_und_current_debt":posts_und_current_debt,
                        "posts_und_payment_ability":posts_und_payment_ability,
                        "posts_und_other_evaluation":posts_und_other_evaluation,
                        "posts_und_approve_la":posts_und_approve_la,
                        "posts_und_final_tennor":posts_und_final_tennor,
                        "posts_und_decrease_la":posts_und_decrease_la,
                        "posts_und_defer" : posts_und_defer,
                        }


    und_checklist_result_html = loader.render_to_string('./en/checklist/module2_show_und_result.html', und_result_table)





    # phần này nhằm lấy lại trạng thái cuối cùng của app do und note
    und_note_status = record_und_pending_defer_history.objects.filter(app_id=get_app_id_for_checklist_und).order_by('-date_created')[:1]

    und_final_note_status = {
                            "und_note_status":und_note_status,
                            }

    und_final_note_status_html = loader.render_to_string('./en/checklist/module2_gui_undcl_und_note_status.html', und_final_note_status)


    # package output data and return it as a JSON object
    output_data = { 'posts_html00' : posts_html00,
                    'posts_html01' : posts_html01,
                    'posts_html1' : posts_html1,
                    'posts_html2' : posts_html2,
                    'posts_html3' : posts_html3,
                    'posts_html4' : posts_html4,
                    'posts_html5' : posts_html5,
                    'posts_html6' : posts_html6,
                    'posts_html7' : posts_html7,
                    'posts_html8' : posts_html8,
                    'posts_html9' : posts_html9,
                    'save_complete': save_complete,
                    'checklist_result_html': checklist_result_html,
                    'dc_final_note_status_html' : dc_final_note_status_html,
                    'phv_checklist_result_html': phv_checklist_result_html,
                    'phv_final_note_status_html' : phv_final_note_status_html,
                    'und_checklist_result_html' : und_checklist_result_html,
                    'und_final_note_status_html' : und_final_note_status_html,
                    }
    
    return JsonResponse(output_data)  









def search_customer_information_with_full_information_id_dob_name_on_und_checklist(request):

    query02 = request.GET.get('customer_name')
    query03 = request.GET.get('customer_dob')
    query04 = request.GET.get('customer_id')


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <9:
        report_error = "Please input correct ID No of customer!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR (%s),'[[:space:]]*', '') " +
                            "       AND DOB = %s) OR (CUSTOMER_ID_NUMBER = %s ) OR (SPOUSE_ID = %s) " )

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query02, query03, query04, query04])

        context = { 
                "object_list": queryset_list
                }


    save_complete = True

    # build a html posts list
    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)


    # package output data and return it as a JSON object
    output_data = {'posts_html': posts_html,'save_complete': save_complete}
    
    return JsonResponse(output_data)








def search_customer_information_with_home_directory_style_on_und_checklist(request):

    query002 = request.GET.get('customer_name')
    query003 = request.GET.get('customer_dob')
    query004 = request.GET.get('customer_id')

    

    if query002 is not None:
        query02 = query002.upper()
        query012 = re.sub('[!@#$%]', '', query02)
    else:
        query02 = query002

    if query003 is not None:
        query03 = query003.upper()
        query013 = re.sub('[!@#$%]', '', query03)
    else:
        query03 = query003

    if query004 is not None:
        query04 = query004.upper()
        query014 = re.sub('[!@#$%]', '', query04)
    else:
        query04 = query004


    if query04 is None:
        query04 = '%'
    else:        
        query04 = query04 + '%'


    digit = 0

    for i in query04:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 5 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                "           OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                "   FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                "   WHERE CUSTOMER_ID_NUMBER LIKE %s AND CUSTOMER_NAME LIKE %s AND DOB LIKE %s")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query04, query02, query03])


        context = { 
            "object_list": queryset_list,
            }



    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)











def search_customer_spouse_information_with_free_style_on_und_checklist(request):

    query005 = request.GET.get('spouse_name')
    query006 = request.GET.get('spouse_id')


    if query005 is not None:
        query05 = query005.upper()
        query015 = re.sub('[!@#$%]', '', query05)
    else:
        query05 = query005

    if query006 is not None:
        query06 = query006.upper()
        query016 = re.sub('[!@#$%]', '', query06)
    else:
        query06 = query006



    digit = 0

    if query06 is None:
        query06 = '%'


    for i in query06:
        if i.isnumeric():
            digit+=1

    if digit <3:
        report_error = "Please input at least 3 digits!"
        context = { 
                "report_error": report_error
                }

    else:
        oracle_sql = (" SELECT 1 as id, APP_ID, CUSTOMER_NAME, CUSTOMER_ID_NUMBER, DOB, DPD, ISSUE_PLACE, " +
                            " OPERATE_DATE, PRODUCT, STATUS, CURRENT_PROVINCE, FB_PROVINCE, SPOUSE_NAME, SPOUSE_ID, ON_SYSTEM" +
                            " FROM RISK_BOX.DEDUPE_PAGE_DEDUPE_ID_MODEL " +
                            " WHERE (( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(SPOUSE_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND SPOUSE_ID LIKE %s ))" +
                            "       OR ( REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(CUSTOMER_NAME),'[[:space:]]*', '') LIKE REGEXP_REPLACE(RISK_REMOVE_VN_MARK_CHAR(%s),'[[:space:]]*', '') AND CUSTOMER_ID_NUMBER LIKE %s)")

        queryset_list  = dedupe_id_model.objects.using('datadb').raw(oracle_sql, [query05, query06, query05, query06])

        context = { 
                    "object_list": queryset_list,
                    }


    save_complete = True

    posts_html = loader.render_to_string('./en/stage_dc/module_dedupeid_search_result.html', context)



    output_data = {'posts_html': posts_html,'save_complete': save_complete}

    return JsonResponse(output_data)







def search_mobile_dedupe_mobile_phone_cux_and_ref_on_und_checklist(request):

    searched_mobilephone = request.GET.get('searched_mobile')

    oracle_sql1 = ("SELECT DISTINCT 1 as id, APP_ID, ADDRESS_TYPE, CUX_PHONE1, CUX_MOBILE, CUX_ADD1, " +
                      "    DPD, STATUS, CUSTOMER_ID_NO" + 
                      "  FROM DEDUPE_PAGE_DEDUPE_MOBILE_8D55 WHERE CUX_PHONE1 = %s or CUX_MOBILE = %s")

    oracle_sql2 = ("SELECT DISTINCT 1 as id, APP_ID, REF_NAME, REF_REL, REF_PHONE1, REF_PHONE2, " +
                      "    REF_STDISD, DPD, STATUS, CUSTOMER_ID_NO" + 
                      "  FROM DEDUPE_PAGE_DEDUPE_MOBILE_3899 WHERE REF_PHONE1 = %s or REF_PHONE2 = %s or REF_STDISD = %s")

    oracle_sql3 = ( "SELECT DISTINCT 1 as id, CUSTOMER_NATIONAL_ID, CUSTOMER_NAME, DSA_CODE, PHONE_NUMBER " + 
                    "FROM DEDUPE_PAGE_BLACK_LIST_ON_31B8 "+
                    "WHERE PHONE_NUMBER = %s ")

    mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.using('datadb').raw(oracle_sql1,[searched_mobilephone, searched_mobilephone])
    mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.using('datadb').raw(oracle_sql2,[searched_mobilephone, searched_mobilephone, searched_mobilephone])
    mobilephone_dupe_result_2  = black_list_on_mafc_employee_dedupe_model.objects.using('datadb').raw(oracle_sql3,[searched_mobilephone])

    #mobilephone_dupe_result  = dedupe_mobile_cux_model.objects.using('datadb').get( Q(cux_phone1 =searched_mobilephone) | Q(cux_mobile =searched_mobilephone)) 
    #mobilephone_dupe_result_1  = dedupe_mobile_ref_model.objects.using('datadb').get( Q(ref_phone1 =searched_mobilephone) | Q(ref_phone2 =searched_mobilephone) | Q(ref_stdisd =searched_mobilephone)) 


    context = {     
                "mobilephone_dupe_result": mobilephone_dupe_result,
                "mobilephone_dupe_result_1": mobilephone_dupe_result_1,
                "mobilephone_dupe_result_2": mobilephone_dupe_result_2,
                }

    save_complete = True

    posts_html01 = loader.render_to_string('./en/checklist/module1_gui_dedupemobi_cux_mobi.html', context)
    posts_html02 = loader.render_to_string('./en/checklist/module1_gui_dedupemobi_ref_mobi.html', context)
    posts_html03 = loader.render_to_string('./en/checklist/module1_gui_alert_sale_mobi.html', context)


    output_data = { 'posts_html01': posts_html01,
                    'posts_html02': posts_html02,
                    'posts_html03' : posts_html03,
                    'save_complete': save_complete}

    return JsonResponse(output_data)











def und_cl_super_total_comment(request):

    app_id_c = request.POST.get('app_id_c')

    und_las_status_doc_type = request.POST.get('und_las_status_doc_type')
    und_final_decision_selectedVal = request.POST.get('und_final_decision_selectedVal')
    und_note_last_status = request.POST.get('und_note_last_status')

    id_card_doc_type = request.POST.get('id_card_doc_type')
    id_card_selectedVal = request.POST.get('id_card_selectedVal')
    id_card_comment = request.POST.get('id_card_comment')

    family_book_doc_type = request.POST.get('family_book_doc_type')
    family_book_selectedVal = request.POST.get('family_book_selectedVal')
    family_book_comment = request.POST.get('family_book_comment')

    resident_proof_doc_type = request.POST.get('resident_proof_doc_type')
    resident_proof_selectedVal = request.POST.get('resident_proof_selectedVal')
    resident_proof_comment = request.POST.get('resident_proof_comment')

    work_proof_doc_type = request.POST.get('work_proof_doc_type')
    work_proof_selectedVal = request.POST.get('work_proof_selectedVal')
    work_proof_comment = request.POST.get('work_proof_comment')

    income_proof_doc_type = request.POST.get('income_proof_doc_type')
    income_proof_selectedVal = request.POST.get('income_proof_selectedVal')
    income_proof_comment = request.POST.get('income_proof_comment')

    current_address_doc_type = request.POST.get('current_address_doc_type')
    current_address_selectedVal = request.POST.get('current_address_selectedVal')
    current_address_paper_type = request.POST.get('current_address_paper_type')
    current_address_comment = request.POST.get('current_address_comment')

    current_job_doc_type = request.POST.get('current_job_doc_type')
    current_job_selectedVal = request.POST.get('current_job_selectedVal')
    current_job_comment = request.POST.get('current_job_comment')

    current_debt_doc_type = request.POST.get('current_debt_doc_type')
    current_debt_selectedVal = request.POST.get('current_debt_selectedVal')
    current_debt_comment = request.POST.get('current_debt_comment')

    current_payment_doc_type = request.POST.get('current_payment_doc_type')
    current_payment_selectedVal = request.POST.get('current_payment_selectedVal')
    current_payment_comment = request.POST.get('current_payment_comment')

    other_evaluation_doc_type = request.POST.get('other_evaluation_doc_type')
    other_evaluation_comment = request.POST.get('other_evaluation_comment')

    approve_la_doc_type = request.POST.get('approve_la_doc_type')
    approve_la_comment = request.POST.get('approve_la_comment')

    approve_tennor_doc_type = request.POST.get('approve_tennor_doc_type')
    approve_tennor_comment = request.POST.get('approve_tennor_comment')
    approve_reason_doc_type = request.POST.get('approve_reason_doc_type')
    approve_reason_comment = request.POST.get('approve_reason_comment')




    if app_id_c != None and app_id_c != "" and und_note_last_status != None and und_note_last_status != "" and und_final_decision_selectedVal == 'UW_UND_DEFER':

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = und_final_decision_selectedVal,  
                                                                checked_comment = und_note_last_status, 
                                                                action_username = request.user
                                                                )

        post_instance = record_und_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = und_las_status_doc_type,
                                                                action_type_detail = und_final_decision_selectedVal,  
                                                                action_comment = und_note_last_status, 
                                                                action_username = request.user
                                                                )

    elif app_id_c != None and app_id_c != "" and und_note_last_status != None and und_note_last_status != "" and und_final_decision_selectedVal != 'UW_PHV_DEFER':

        post_instance = record_und_pending_defer_history.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                action_type = und_las_status_doc_type,
                                                                action_type_detail = und_final_decision_selectedVal,  
                                                                action_comment = und_note_last_status, 
                                                                action_username = request.user
                                                                )

    else:

        pass






    if app_id_c != "" and id_card_doc_type != "" and id_card_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = id_card_doc_type, 
                                                                checked_result = id_card_selectedVal, 
                                                                checked_comment = id_card_comment, 
                                                                action_username = request.user)

    else:

        pass



    if app_id_c != "" and family_book_doc_type != "" and family_book_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = family_book_doc_type, 
                                                                checked_result = family_book_selectedVal, 
                                                                checked_comment = family_book_comment, 
                                                                action_username = request.user)

    else:

        pass





    if app_id_c != "" and resident_proof_doc_type != "" and resident_proof_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = resident_proof_doc_type, 
                                                                checked_result = resident_proof_selectedVal, 
                                                                checked_comment = resident_proof_comment, 
                                                                action_username = request.user)

    else:

        pass



    if app_id_c != "" and work_proof_doc_type != "" and work_proof_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = work_proof_doc_type, 
                                                                checked_result = work_proof_selectedVal, 
                                                                checked_comment = work_proof_comment, 
                                                                action_username = request.user)

    else:

        pass



    if app_id_c != "" and income_proof_doc_type != "" and income_proof_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(app_id = app_id_c, 
                                                                date_created = timezone.now(), 
                                                                paper_type = income_proof_doc_type, 
                                                                checked_result = income_proof_selectedVal, 
                                                                checked_comment = income_proof_comment, 
                                                                action_username = request.user)
    else:

        pass




    if app_id_c != "" and current_address_doc_type != "" and current_address_selectedVal != ""  and current_address_paper_type != "...":

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = current_address_doc_type, 
                                                                    checked_result = current_address_selectedVal,
                                                                    paper_type_item = current_address_paper_type, 
                                                                    checked_comment = current_address_comment, 
                                                                    action_username = request.user)
    else:

        pass






    if app_id_c != "" and current_job_doc_type != "" and current_job_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = current_job_doc_type, 
                                                                    checked_result = current_job_selectedVal,
                                                                    checked_comment = current_job_comment, 
                                                                    action_username = request.user)
    else:

        pass





    if app_id_c != "" and current_debt_doc_type != "" and current_debt_selectedVal != "":

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = current_debt_doc_type, 
                                                                    checked_result = current_debt_selectedVal,
                                                                    checked_comment = current_debt_comment, 
                                                                    action_username = request.user)
    else:

        pass




    if app_id_c != "" and current_payment_doc_type != "" and current_payment_selectedVal != ""  and current_payment_comment != "":

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = current_payment_doc_type, 
                                                                    checked_result = current_payment_selectedVal,
                                                                    checked_comment = current_payment_comment, 
                                                                    action_username = request.user)
    else:

        pass






    if app_id_c != "" and other_evaluation_doc_type != "" and other_evaluation_comment != "" :

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = other_evaluation_doc_type, 
                                                                    checked_comment = other_evaluation_comment, 
                                                                    action_username = request.user)
    else:

        pass




    if app_id_c != "" and approve_la_doc_type != "" and approve_la_comment != "" :

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = approve_la_doc_type, 
                                                                    checked_comment = approve_la_comment, 
                                                                    action_username = request.user)
    else:

        pass



    if app_id_c != "" and approve_tennor_doc_type != "" and approve_tennor_comment != "" :

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = approve_tennor_doc_type, 
                                                                    checked_comment = approve_tennor_comment, 
                                                                    action_username = request.user)
    else:

        pass



    if app_id_c != "" and approve_reason_doc_type != "" and approve_reason_comment != "" :

        post_instance = record_und_result_cl.objects.create(   app_id = app_id_c,
                                                                    date_created = timezone.now(), 
                                                                    paper_type = approve_reason_doc_type, 
                                                                    checked_comment = approve_reason_comment, 
                                                                    action_username = request.user)
    else:

        pass




    # build a html posts list
    posts_und_id_card = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='ID CARD').order_by('paper_type', '-date_created')
    posts_und_family_book = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='FAMILY BOOK').order_by('paper_type', '-date_created')
    posts_und_resident_proof = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='RESIDENT PROOF').order_by('paper_type', '-date_created')
    posts_und_work_proof = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='WORK PROOF').order_by('paper_type', '-date_created')
    posts_und_income_proof = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='INCOME PROOF').order_by('paper_type', '-date_created')
    posts_und_current_address = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='CURRENT ADDRESS').order_by('paper_type', '-date_created')
    posts_und_current_job = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='CURRENT JOB').order_by('paper_type', '-date_created')
    posts_und_current_debt = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='CURRENT DEBT').order_by('paper_type', '-date_created')
    posts_und_payment_ability = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='PAYMENT ABILITY').order_by('paper_type', '-date_created')
    posts_und_other_evaluation = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='OTHER EVALUATION').order_by('paper_type', '-date_created')
    posts_und_approve_la = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='APPROVE LOAN AMOUNT').order_by('paper_type', '-date_created')
    posts_und_final_tennor = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='FINAL TENNOR').order_by('paper_type', '-date_created')
    posts_und_decrease_la = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='REASON DECREASE LOAN AMOUNT').order_by('paper_type', '-date_created')


    posts_und_defer = record_und_result_cl.objects.filter(app_id=app_id_c, paper_type='UW_UND_DEFER').order_by('paper_type', '-date_created')

    und_result_table = {
                        "posts_und_id_card":posts_und_id_card,
                        "posts_und_family_book":posts_und_family_book,
                        "posts_und_resident_proof":posts_und_resident_proof,
                        "posts_und_work_proof":posts_und_work_proof,
                        "posts_und_income_proof":posts_und_income_proof,
                        "posts_und_current_address":posts_und_current_address,
                        "posts_und_current_job":posts_und_current_job,
                        "posts_und_current_debt":posts_und_current_debt,
                        "posts_und_payment_ability":posts_und_payment_ability,
                        "posts_und_other_evaluation":posts_und_other_evaluation,
                        "posts_und_approve_la":posts_und_approve_la,
                        "posts_und_final_tennor":posts_und_final_tennor,
                        "posts_und_decrease_la":posts_und_decrease_la,
                        "posts_und_defer" : posts_und_defer,
                        }


    checklist_result_html = loader.render_to_string('./en/checklist/module2_show_und_result.html', und_result_table)



    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'checklist_result_html': checklist_result_html,'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)










def und_create_update_company_information(request):

    user_groups = request.user.groups.values_list('name', flat=True)

    f1_company_tax_code = request.POST.get('f1_company_tax_code')
    f1_company_name = request.POST.get('f1_company_name')
    riskbox_company_tax_code = request.POST.get('riskbox_company_tax_code')
    und_company_name = request.POST.get('und_company_name')
    und_company_note = request.POST.get('und_company_note')
    suptl_company_note_on_undcl = request.POST.get('suptl_company_note_on_undcl')
    get_clicked_button = request.POST.get('get_clicked_button')

    update_user = request.user.username
    update_group = request.user.groups.values_list('name', flat=True)

    
    company_data_in_risk_box = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code)



    if get_clicked_button == 'und_create_update_company_infor':

        if company_data_in_risk_box.exists():

            post_instance = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code).update(
                                                                                                                        f1employername = f1_company_name,
                                                                                                                        riskboxtaxcode = riskbox_company_tax_code, 
                                                                                                                        employername = und_company_name, 
                                                                                                                        undnote = und_company_note,
                                                                                                                        #und_date_created = timezone.now(),
                                                                                                                        und_date_updated = timezone.now(), 
                                                                                                                        action_username = update_user
                                                                                                                        )

            post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                                f1employername = f1_company_name,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name,
                                                                                undnote = und_company_note,
                                                                                #und_date_created = timezone.now(),
                                                                                date_created = timezone.now(), 
                                                                                action_username = request.user)


        else:

            post_instance = record_und_newest_note_for_company.objects.create(  f1taxcode = f1_company_tax_code,
                                                                                f1employername = f1_company_name,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name, 
                                                                                undnote = und_company_note,
                                                                                #und_date_created = timezone.now(),
                                                                                und_date_updated = timezone.now(), 
                                                                                action_username = request.user)

            post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name,
                                                                                undnote = und_company_note,
                                                                                #und_date_created = timezone.now(),
                                                                                date_created = timezone.now(), 
                                                                                action_username = request.user)


    elif get_clicked_button == 'uw_suptl_confirm_company_infor' and 'UW_SUP_TL' in update_group:

        if company_data_in_risk_box.exists():

            post_instance = record_und_newest_note_for_company.objects.filter(f1taxcode = f1_company_tax_code).update(
                                                                                                                        f1employername = f1_company_name,
                                                                                                                        riskboxtaxcode = riskbox_company_tax_code, 
                                                                                                                        employername = und_company_name, 
                                                                                                                        sup_tl_note = suptl_company_note_on_undcl,
                                                                                                                        #und_date_created = timezone.now(),
                                                                                                                        sup_tl_date_updated = timezone.now(), 
                                                                                                                        action_username = update_user
                                                                                                                        )

            post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                                f1employername = f1_company_name,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name,
                                                                                sup_tl_note = suptl_company_note_on_undcl,
                                                                                #und_date_created = timezone.now(),
                                                                                date_created = timezone.now(), 
                                                                                action_username = request.user)


        else:

            post_instance = record_und_newest_note_for_company.objects.create(  f1taxcode = f1_company_tax_code,
                                                                                f1employername = f1_company_name,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name, 
                                                                                sup_tl_note = suptl_company_note_on_undcl,
                                                                                #und_date_created = timezone.now(),
                                                                                sup_tl_date_updated = timezone.now(), 
                                                                                action_username = request.user)

            post_history= record_und_note_history_for_company.objects.create(   f1taxcode = f1_company_tax_code,
                                                                                f1employername = f1_company_name,
                                                                                riskboxtaxcode = riskbox_company_tax_code, 
                                                                                employername = und_company_name,
                                                                                sup_tl_note = suptl_company_note_on_undcl,
                                                                                #und_date_created = timezone.now(),
                                                                                date_created = timezone.now(), 
                                                                                action_username = request.user)




    save_complete = True

    # package output data and return it as a JSON object
    output_data = {'save_complete': save_complete}

    # package output data and return it as a JSON object
    return JsonResponse(output_data)