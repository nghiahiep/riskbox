from django.forms import ModelForm

from checklist.models import record_datachecker_result

# Create the form class.
class form_datacheck_id_card(ModelForm):
	class Meta:
		model = record_datachecker_result
		fields = ['app_id', 'date_created', 'paper_type', 'checked_result', 'checked_code', 'checked_comment', 'action_user', 'action_username']
