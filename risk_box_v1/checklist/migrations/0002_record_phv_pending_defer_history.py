# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-05 23:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('checklist', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='record_phv_pending_defer_history',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(null=True)),
                ('action_type', models.CharField(max_length=200)),
                ('action_type_detail', models.CharField(max_length=200)),
                ('action_comment', models.CharField(max_length=3000)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
