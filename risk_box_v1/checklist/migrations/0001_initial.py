# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-03 09:34
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LoginUpdate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_updated', models.DateTimeField(null=True)),
                ('action_type', models.CharField(max_length=5)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='record_datachecker_pending_defer_history',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(null=True)),
                ('action_type', models.CharField(max_length=200)),
                ('action_type_detail', models.CharField(max_length=200)),
                ('action_comment', models.CharField(max_length=3000)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='record_datachecker_result',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(null=True)),
                ('paper_type', models.CharField(max_length=200)),
                ('paper_type_item', models.CharField(max_length=200, null=True)),
                ('checked_result', models.CharField(max_length=200)),
                ('checked_code', models.CharField(max_length=200)),
                ('checked_comment', models.CharField(max_length=3000)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='record_phv_comment_for_each_mobile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=200)),
                ('called_mobi', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(null=True)),
                ('phv_decision_after_call', models.CharField(max_length=200)),
                ('phv_emoji_after_call', models.CharField(max_length=200)),
                ('phv_comment_4_mobi', models.CharField(max_length=3000)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='record_phv_result_cl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('app_id', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(null=True)),
                ('check_type', models.CharField(max_length=200)),
                ('checked_result', models.CharField(max_length=200)),
                ('check_type_detail', models.CharField(max_length=200, null=True)),
                ('checked_type_uw_decision', models.CharField(max_length=200)),
                ('checked_comment', models.CharField(max_length=3000)),
                ('action_username', models.CharField(max_length=50)),
                ('action_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
