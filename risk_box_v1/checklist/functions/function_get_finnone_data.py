from checklist.models import dedupe_id_model_data_finnone
from checklist.models import dedupe_mobile_cux_model_data_finnone
from checklist.models import dedupe_mobile_ref_model_data_finnone

from django.db import connections
import cx_Oracle


def finnonedb_tbl_dl(getserver, itable, stritable, gtable, irow, getsql):

  import os
  import unicodecsv as csv
  from io import BytesIO
  from itertools import islice

  print("begin fetching!")

  if gtable =='0':

    sql = getsql

  else:

    sql = (" SELECT rownum as id, a.* FROM %s a " %(gtable))
  

  cur = connections[getserver].cursor()
  cur.execute(sql)
  data = cur.fetchall()

  print("end fetching!")


  print("begin export csv!")

  column_names = [i[0] for i in cur.description]

  fp = BytesIO()
  export_file = gtable + ".csv"

  fp = open(export_file, 'wb')

  myFile = csv.writer(fp, delimiter='|', encoding='UTF-8')
  myFile.writerows(data)
  fp.close()
  print("end export csv!")



  print("star count row of %s!" % gtable)
  count_row = sum(1 for line in open(export_file, encoding="utf8"))
  print("end count!")



  print("remove data of %s!" % itable)
  app = itable.objects.all()
  app.delete()

  mysql_column_string = [field.name for field in itable._meta.get_fields()]
  column_string = ','.join(("`"+str(column)+"`")for column in mysql_column_string)

  print("finish")



  print("begin import csv to model!")

  import_table = "checklist_" + stritable

  with open(export_file,'rb') as csvfile:

    reader = csv.reader(csvfile, delimiter="|" , encoding='utf-8')


    insert_sql = """

      INSERT INTO `risk_box_uat`.`%s`
      (
      %s
      )
      VALUES

    """

    base_sql = insert_sql%(import_table,column_string)


    values_sql = []
    values_data = []
    params = []
    add_row = 0

    for i,row in enumerate(reader):


      placeholders = ['%s' for p in range(len(row))]
      values_sql.append("(%s)" % ','.join(placeholders))
      values_data.extend(row)

      sql = '%s%s' % (base_sql, ', '.join(values_sql))


      for a,b in enumerate(column_names):
        params.extend([
            row[a] or None
        ])


      if (i == add_row) or (i == (count_row-1)):
        #print("begin insert %s" % irow)
        add_row = add_row + irow
        cur = connections['default'].cursor()
        cur.execute(sql, params)
        #print("end insert %s" % irow)
        print(i)
        print(count_row)
        print("Insert %s" % (i/count_row))
        params = []
        values_sql = []
        values_data = []

  print("finish import csv to model!")



  print('%s is coppied successful!' % gtable)

