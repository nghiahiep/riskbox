    
from checklist.models import record_nice_score_history_for_app
from django.db import connections
import cx_Oracle



def get_nice_score(i_agreementid):

  conn = None
  try:
      connstr = 'Risk_Prod/U$er2017@192.168.100.20:1521/orcl'
      conn = cx_Oracle.connect(connstr)

      try:
        cur = conn.cursor()

        return_score = cur.var(cx_Oracle.STRING)
        return_score_date = cur.var(cx_Oracle.DATETIME)

        cur = conn.cursor()
        cur.callproc("SQLSERVER.GET_NICE_SCORE", (i_agreementid,return_score,return_score_date))

        nice_score = return_score.getvalue()
        nice_score_date = return_score_date.getvalue()

        current_return= []
        current_return.append(nice_score)
        current_return.append(nice_score_date)



      except cx_Oracle.DatabaseError as e:
        error, = e.args
        if error.code == 20201:
          current_return= []

      finally:
          cur.close()
  finally:
      if conn is not None:
          conn.close()

  return list(current_return)




def get_and_store_nice_score(get_app_id_for_dedupe_phone, username, record_team):

    return_f1_nice_score =[]
    return_f1_nice_score = get_nice_score(i_agreementid = get_app_id_for_dedupe_phone)



    #username = request.user.username
    username = username.upper()


    if not return_f1_nice_score:

      nice_score_last_try = []

    else:

      nice_score_last_try0 = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).values_list('nice_score', flat=True).order_by('-record_date')[:1]


      if not nice_score_last_try0:

        post_instance = record_nice_score_history_for_app.objects.create(
                                                                          app_id = get_app_id_for_dedupe_phone,
                                                                          nice_score = return_f1_nice_score[0],
                                                                          check_date = return_f1_nice_score[1],
                                                                          record_team = record_team,
                                                                          user_get_app = username
                                                                            )
        nice_score_last_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-record_date')[:1]

      elif nice_score_last_try0[0] == return_f1_nice_score[0]:

        nice_score_last_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-record_date')[:1]

      else:
        post_instance = record_nice_score_history_for_app.objects.create(
                                                                          app_id = get_app_id_for_dedupe_phone,
                                                                          nice_score = return_f1_nice_score[0],
                                                                          check_date = return_f1_nice_score[1],
                                                                          record_team = record_team,
                                                                          user_get_app = username
                                                                            )
    
        nice_score_last_try = record_nice_score_history_for_app.objects.filter(app_id=get_app_id_for_dedupe_phone).order_by('-record_date')[:1]

    return nice_score_last_try