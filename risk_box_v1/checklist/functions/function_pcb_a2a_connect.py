		
from checklist.models import record_pcb_customer_loan_infor, record_pcb_customer_mobile_phone
from checklist.models import record_pcb_matched_customer_basic_infor
from django.db import connections

import cx_Oracle
import requests
import json
import unicodedata
import re

from xml.etree import ElementTree as ET

from lxml import etree




def no_accent_vietnamese(s):
	s = str(s)
	s = re.sub(u'[àáạảãâầấậẩẫăằắặẳẵ]', 'a', s)
	s = re.sub(u'[ÀÁẠẢÃĂẰẮẶẲẴÂẦẤẬẨẪ]', 'A', s)
	s = re.sub(u'[èéẹẻẽêềếệểễ]', 'e', s)
	s = re.sub(u'[ÈÉẸẺẼÊỀẾỆỂỄ]', 'E', s)
	s = re.sub(u'[òóọỏõôồốộổỗơờớợởỡ]', 'o', s)
	s = re.sub(u'[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]', 'O', s)
	s = re.sub(u'[ìíịỉĩ]', 'i', s)
	s = re.sub(u'[ÌÍỊỈĨ]', 'I', s)
	s = re.sub(u'[ùúụủũưừứựửữ]', 'u', s)
	s = re.sub(u'[ƯỪỨỰỬỮÙÚỤỦŨ]', 'U', s)
	s = re.sub(u'[ỳýỵỷỹ]', 'y', s)
	s = re.sub(u'[ỲÝỴỶỸ]', 'Y', s)
	s = re.sub(u'[Đ]', 'D', s)
	s = re.sub(u'[đ]', 'd', s)

	norm_txt = unicodedata.normalize('NFD', str(s))
	shaved = ''.join(c for c in norm_txt if not unicodedata.combining(c))
	return unicodedata.normalize('NFC', shaved)


def try_find_xml(xmlcollection, positionaddress):

	try:
		findelement = xmlcollection.find(positionaddress)
	except KeyError:
		findelement = ''

	if findelement is None:
		findelement = ''
	elif findelement.text is None:
		findelement = ''
	else:
		findelement = findelement.text.strip()

	return findelement


def pcb_get_value(raw_input):

	if raw_input.text is None:
		return_string = ''
	else:
		return_string = raw_input.text.strip()
	return return_string



def send_request_to_pcb(app_id_c):

	oracle_sql3 = ("  SELECT 1 as id, C.APP_ID_C, " +
								 "          TO_CHAR (C.LAA_APP_SIGNED_DATE_D,'DDMMYYYY') REQUEST_DATE, " +
								 "          C.LAA_APP_REQ_AMTFIN_N, C.LAA_APP_REQ_TERM_N, D.CUSTOMERNAME, D.SEX, " +
								 "          TO_CHAR (D.DOB,'DDMMYYYY') DOB, D.CIF_NO, " +
								 "          A.ADDRESS1, A.STATEDESC, A.ADDRESSTYPE, A.MOBILE " + 
								 "  FROM " +
								 "          MACAS.NBFC_ADDRESS_M A " +
								 "  LEFT JOIN MACAS.LOS_APP_APPLICATIONS C ON C.CUST_ID_N = A.BPID " +
								 "  LEFT JOIN MACAS.NBFC_CUSTOMER_M D ON D.CUST_ID_N = A.BPID " +
								 "  WHERE C.APP_ID_C = to_char (%s) AND A.ADDRESSTYPE = 'CURRES'"
	)

	queryset_list3  = record_pcb_matched_customer_basic_infor.objects.using('finnonedb').raw(oracle_sql3,[app_id_c])


	check_time = record_pcb_matched_customer_basic_infor.objects.filter(app_id=app_id_c).count()

	request_app = ''
	request_date = ''
	request_amt = ''
	request_term = ''
	request_name = ''
	request_sex = ''
	request_dob = ''
	request_cif = ''
	request_addno = ''
	request_state = ''
	request_addtype = ''
	request_mob = ''
	for i in queryset_list3:
		request_app = i.app_id_c
		request_date = i.request_date
		request_amt = i.laa_app_req_amtfin_n
		request_term = i.laa_app_req_term_n
		request_name = no_accent_vietnamese(i.customername)
		request_sex = i.sex
		request_dob = i.dob
		request_cif = i.cif_no
		request_addno = no_accent_vietnamese(i.address1)
		request_state = no_accent_vietnamese(i.statedesc)
		request_add = i.addresstype
		request_mob = i.mobile




	FIContractCode = str(request_app) + "_" + str(check_time)


	url = "https://a2a.pcb.vn/Service.asmx?WSDL"

	headers = {'content-type' : 'text/xml',}

	import datetime

	today = datetime.datetime.today().date()

	body = ('''<?xml version="1.0" encoding="UTF-8"?>
	<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Header>
			<Message GId="PCB" MId="PCB" MTs="%sT12:09:12.0915028+01:00" xmlns="urn:crif-message:2006-08-23">
				<C UD="" UId="JOJI0834" UPwd="Xg35KCayV5"/>
				<P SId="CB" PId="RI_Req" PNs="urn:RI_Req.2011-09-19.000"/>
				<Tx TxNs="urn:crif-messagegateway:2006-08-23"/>
			</Message>
		</soap:Header>
	 <soap:Body>
	 <MGRequest xmlns="urn:crif-messagegateway:2006-08-23"><![CDATA[
		 <RI_Req_Input>
				<Contract>
				 <FIContractCode>%s</FIContractCode>
				 <DateRequestContract>%s</DateRequestContract>
				 <OperationType>10</OperationType>
				 <CodCurrency>VND</CodCurrency>
				 <Instalment>
					<AmountFinancedCapital>%s</AmountFinancedCapital>
					<NumTotalInstalment>%s</NumTotalInstalment>
					<CodPaymentPeriodicity>M</CodPaymentPeriodicity>
				 </Instalment>
				</Contract>
				<Subject>
				 <FISubjectCode />
				 <Person>
					<Name>%s</Name>
					<Gender>%s</Gender>
					<DateOfBirth>%s</DateOfBirth>
					<CountryOfBirth>VN</CountryOfBirth>
					<IDCard>%s</IDCard>
					<Address>
					 <Main>
							<FullAddress>%s</FullAddress>
					 </Main>
					 <Additional>
							<FullAddress></FullAddress>
					 </Additional>
					</Address>
					<Reference>
					 <Number>%s</Number>
					</Reference>
				 </Person>
				</Subject>
				<Role>A</Role>
		 </RI_Req_Input>
	 ]]></MGRequest>
	 </soap:Body>
	</soap:Envelope>
	''' %(today, FIContractCode, request_date, request_amt, request_term, request_name, request_sex, request_dob,
				request_cif, request_addno, request_mob))



	#for house in queryset_list3:

	#  FIContractCode = "<FIContractCode>" + str(house.app_id_c) + "</FIContractCode>"

	#  body = body.replace("<FIContractCode></FIContractCode>", FIContractCode)

	#  DateRequestContract = "<DateRequestContract>" + str(house.request_date) + "</DateRequestContract>"

	#  body = body.replace("<DateRequestContract></DateRequestContract>",DateRequestContract)

	#  AmountFinancedCapital = "<AmountFinancedCapital>" + str(house.laa_app_req_amtfin_n) + "</AmountFinancedCapital>"

	#  body = body.replace("<AmountFinancedCapital></AmountFinancedCapital>",AmountFinancedCapital)

	#  NumTotalInstalment = "<NumTotalInstalment>" + str(house.laa_app_req_term_n) + "</NumTotalInstalment>"

	#  body = body.replace("<NumTotalInstalment></NumTotalInstalment>",NumTotalInstalment)

	#  Name = "<Name>" + str(house.customername) + "</Name>"

	#  body = body.replace("<Name></Name>",Name)

	#  Gender = "<Gender>" + str(house.sex) + "</Gender>"

	#  body = body.replace("<Gender></Gender>",Gender)

	#  DateOfBirth = "<DateOfBirth>" + str(house.dob) + "</DateOfBirth>"

	#  body = body.replace("<DateOfBirth></DateOfBirth>",DateOfBirth)

	#  IDCard = "<IDCard>" + str(house.cif_no) + "</IDCard>"

	#  body = body.replace("<IDCard></IDCard>",IDCard)

	#  Main = "<Main><FullAddress>" + str(house.address1) + "</FullAddress></Main>"

	#  body = body.replace("<Main><FullAddress></FullAddress></Main>",Main)

	#  Number = "<Number>" + str(house.mobile) + "</Number>"

	#  body = body.replace("<Number></Number>",Number)

	response = requests.post(url,data=body.encode('utf-8'), headers=headers, verify=False)


	resp_cont = response.text
	resp_cont = resp_cont.replace("&lt;", "<")
	resp_cont = resp_cont.replace("&gt;", ">")



	#---- this part is used to print string into XML format ---

	from bs4 import BeautifulSoup

	tree_string= BeautifulSoup(resp_cont, "xml").prettify()

	#-----------------------------------------------------------

	tree = ET.ElementTree(ET.fromstring(tree_string))

	root = tree.getroot()



	pcb_return ={
		'error' : '',
		'error_cat':'',
		'error_code':'',
		'error_desc':'',
		'found' : '',
		'id' : ''
	}


	try:
		pcb_down = root.findall('.//faultstring')
		pcb_proc_error = root.findall('.//Error')
	except KeyError:
		pcb_down = ''
		pcb_proc_error = ''



	if pcb_down:

		pcb_return['error'] = 'pcb error'
		pcb_customer_infor = record_pcb_matched_customer_basic_infor(
				app_id = app_id_c,
				app_id_send = FIContractCode,
				found = pcb_return['error']
		)

		pcb_customer_infor.save()
		pcb_return['id'] = pcb_customer_infor.id


	elif pcb_proc_error:

		for error in  root.findall('.//Error'):
			error_cat = try_find_xml(error, './/Category')
			error_code = try_find_xml(error, './/Code')
			error_desc = try_find_xml(error, './/Description')


		if error_cat == 2:


			if error_code == 986:

				pcb_return['error'] = 'send error'
				pcb_return['error_cat'] = error_cat
				pcb_return['error_code'] = error_code
				pcb_return['error_desc'] = error_desc
				pcb_customer_infor = record_pcb_matched_customer_basic_infor(
						app_id = app_id_c,
						app_id_send = FIContractCode,
						found = pcb_return['error'],
						error_code = error_code,
						error_desc = error_desc
				)
				pcb_customer_infor.save()
				pcb_return['id'] = pcb_customer_infor.id

			else:

				pcb_return['error'] = 'unknow error'
				pcb_return['error_cat'] = error_cat
				pcb_return['error_code'] = error_code
				pcb_return['error_desc'] = error_desc
				pcb_customer_infor = record_pcb_matched_customer_basic_infor(
						app_id = app_id_c,
						app_id_send = FIContractCode,
						found = pcb_return['error'],
						error_code = error_code,
						error_desc = error_desc
				)
				pcb_customer_infor.save()
				pcb_return['id'] = pcb_customer_infor.id

		else:

			pcb_return['error'] = 'unknow error'
			pcb_return['error_cat'] = error_cat
			pcb_return['error_code'] = error_code
			pcb_return['error_desc'] = error_desc
			pcb_customer_infor = record_pcb_matched_customer_basic_infor(
					app_id = app_id_c,
					app_id_send = FIContractCode,
					found = pcb_return['error'],
					error_code = error_code,
					error_desc = error_desc
			)
			pcb_customer_infor.save()
			pcb_return['id'] = pcb_customer_infor.id


	else:

		try:
			pcb_found = root.findall('.//Matched')
		except KeyError:
			pcb_found = ''
		
		if not pcb_found:

			pcb_return['found'] = 'not found'
			pcb_customer_infor = record_pcb_matched_customer_basic_infor(
					app_id = app_id_c,
					app_id_send = FIContractCode,
					found = pcb_return['found']
			)

			pcb_customer_infor.save()
			pcb_return['id'] = pcb_customer_infor.id


		else:


			pcb_return['found'] = 'found'

			for each in root.findall('.//Matched'):

				CBSubjectCode = try_find_xml(each,'.//CBSubjectCode')
				#CBSubjectCode = each.find('.//CBSubjectCode')
				#CBSubjectCode = pcb_get_value(CBSubjectCode)
				pcb_return['CBSubjectCode'] = CBSubjectCode

				FISubjectCode = try_find_xml(each,'.//FISubjectCode')
				#FISubjectCode = each.find('.//FISubjectCode')
				#FISubjectCode = pcb_get_value(FISubjectCode)
				pcb_return['FISubjectCode'] = FISubjectCode

				Name = try_find_xml(each,'.//Person/Name')
				#Name = each.find('.//Person/Name')
				#Name = pcb_get_value(Name)
				pcb_return['Name'] = Name

				Gender = try_find_xml(each,'.//Person/Gender')
				#Gender = each.find('.//Person/Gender')
				#Gender = pcb_get_value(Gender)
				pcb_return['Gender'] = Gender

				DateOfBirth = try_find_xml(each,'.//Person/DateOfBirth')
				#DateOfBirth = each.find('.//Person/DateOfBirth')
				#DateOfBirth = pcb_get_value(DateOfBirth)
				pcb_return['DateOfBirth'] = DateOfBirth

				PlaceOfBirth = try_find_xml(each,'.//Person/PlaceOfBirth')
				#PlaceOfBirth = each.find('.//Person/PlaceOfBirth')
				#PlaceOfBirth = pcb_get_value(PlaceOfBirth)
				pcb_return['PlaceOfBirth'] = PlaceOfBirth

				IDCard = try_find_xml(each,'.//Person/IDCard')
				#IDCard = each.find('.//Person/IDCard')
				#IDCard = pcb_get_value(IDCard)
				pcb_return['IDCard'] = IDCard

				TIN = try_find_xml(each,'.//Person/TIN')
				#TIN = each.find('.//Person/TIN')
				#TIN = pcb_get_value(TIN)
				pcb_return['TIN'] = TIN

				per_add = try_find_xml(each,'.//Person/Address/Main/FullAddress')
				#per_add = each.find('.//Person/Address/Main/FullAddress')
				#per_add = pcb_get_value(per_add)
				pcb_return['per_add'] = per_add

				per_city = try_find_xml(each,'.//Person/Address/Main/City')
				#per_city = each.find('.//Person/Address/Main/City')
				#per_city = pcb_get_value(per_city)
				pcb_return['per_city'] = per_city

				per_street = try_find_xml(each,'.//Person/Address/Main/Street')
				#per_street = each.find('.//Person/Address/Main/Street')
				#per_street = pcb_get_value(per_street)
				pcb_return['per_street'] = per_street

				curress_add = try_find_xml(each,'.//Person/Address/Additional/FullAddress')
				#curress_add = each.find('.//Person/Address/Additional/FullAddress')
				#curress_add = pcb_get_value(curress_add)
				pcb_return['curress_add'] = curress_add

				curress_city = try_find_xml(each,'.//Person/Address/Additional/City')
				#curress_city = each.find('.//Person/Address/Additional/City')
				#curress_city = pcb_get_value(curress_city)
				pcb_return['curress_city'] = curress_city

				curress_street = try_find_xml(each,'.//Person/Address/Additional/Street')
				#curress_street = each.find('.//Person/Address/Additional/Street')
				#curress_street = pcb_get_value(curress_street)
				pcb_return['curress_street'] = curress_street


			for each in root.findall('.//CreditHistory/GeneralData'):

				request_time = try_find_xml(each,'.//NumberOfReportingInstitution')
				#request_time = each.find('.//NumberOfReportingInstitution')
				#request_time = pcb_get_value(request_time)
				pcb_return['request_time'] = request_time

				debt_group_3_month = try_find_xml(each,'.//WorstRecentStatus')
				#debt_group_3_month = each.find('.//WorstRecentStatus')
				#debt_group_3_month = pcb_get_value(debt_group_3_month)
				pcb_return['debt_group_3_month'] = debt_group_3_month



			find_score = root.findall('.//ScoreDetail')

			if find_score:

				for each in root.findall('.//ScoreDetail'):

					scoreraw = try_find_xml(each,'.//ScoreRaw')
					#scoreraw = each.find('.//ScoreRaw')
					#scoreraw = pcb_get_value(scoreraw)
					pcb_return['scoreraw'] = scoreraw


					code = try_find_xml(each,'.//ScoreRange/Code')
					#code = each.find('.//ScoreRange/Code')
					#code = pcb_get_value(code)
					pcb_return['code'] = code

					description = try_find_xml(each,'.//ScoreRange/Description')
					#description = each.find('.//ScoreRange/Description')
					#description = pcb_get_value(description)
					pcb_return['description'] = description

			else:
				pcb_return['scoreraw'] = ''
				pcb_return['code'] = ''
				pcb_return['description'] = ''
				


			ex_scorecode = ''
			pcb_return['ex_scorecode'] = ex_scorecode
			ex_scoredescription = ''
			pcb_return['ex_scoredescription'] = ex_scoredescription


			for each in root.findall('.//Instalments'):

				ins_numberofrequested = try_find_xml(each,'.//Summary/NumberOfRequested')
				#ins_numberofrequested = each.find('.//Summary/NumberOfRequested')
				#ins_numberofrequested = pcb_get_value(ins_numberofrequested)
				pcb_return['ins_numberofrequested'] = ins_numberofrequested

				ins_numberofliving = try_find_xml(each,'.//Summary/NumberOfLiving')
				#ins_numberofliving = each.find('.//Summary/NumberOfLiving')
				#ins_numberofliving = pcb_get_value(ins_numberofliving)
				pcb_return['ins_numberofliving'] = ins_numberofliving

				ins_numberofrefused = try_find_xml(each,'.//Summary/NumberOfRefused')
				#ins_numberofrefused = each.find('.//Summary/NumberOfRefused')
				#ins_numberofrefused = pcb_get_value(ins_numberofrefused)
				pcb_return['ins_numberofrefused'] = ins_numberofrefused

				ins_numberofrenounced = try_find_xml(each,'.//Summary/NumberOfRenounced')
				#ins_numberofrenounced = each.find('.//Summary/NumberOfRenounced')
				#ins_numberofrenounced = pcb_get_value(ins_numberofrenounced)
				pcb_return['ins_numberofrenounced'] = ins_numberofrenounced 

				ins_numberofterminated = try_find_xml(each,'.//Summary/NumberOfTerminated')
				#ins_numberofterminated = each.find('.//Summary/NumberOfTerminated')
				#ins_numberofterminated = pcb_get_value(ins_numberofterminated)
				pcb_return['ins_numberofterminated'] = ins_numberofterminated

				ins_aci_monthlyinstalmentsamount = try_find_xml(each,'.//ACInstAmounts/MonthlyInstalmentsAmount')
				#ins_aci_monthlyinstalmentsamount = each.find('.//ACInstAmounts/MonthlyInstalmentsAmount')
				#ins_aci_monthlyinstalmentsamount = pcb_get_value(ins_aci_monthlyinstalmentsamount)
				pcb_return['ins_aci_monthlyinstalmentsamount'] = ins_aci_monthlyinstalmentsamount

				ins_aci_remaininginstalmentsamount = try_find_xml(each,'.//ACInstAmounts/RemainingInstalmentsAmount')
				#ins_aci_remaininginstalmentsamount = each.find('.//ACInstAmounts/RemainingInstalmentsAmount')
				#ins_aci_remaininginstalmentsamount = pcb_get_value(ins_aci_remaininginstalmentsamount)
				pcb_return['ins_aci_remaininginstalmentsamount'] = ins_aci_remaininginstalmentsamount

				ins_aci_unpaiddueinstalmentsamount = try_find_xml(each,'.//ACInstAmounts/UnpaidDueInstalmentsAmount')
				#ins_aci_unpaiddueinstalmentsamount = each.find('.//ACInstAmounts/UnpaidDueInstalmentsAmount')
				#ins_aci_unpaiddueinstalmentsamount = pcb_get_value(ins_aci_unpaiddueinstalmentsamount)
				pcb_return['ins_aci_unpaiddueinstalmentsamount'] = ins_aci_unpaiddueinstalmentsamount

				ins_gi_monthlyinstalmentsamount = try_find_xml(each,'.//GInstAmounts/MonthlyInstalmentsAmount')
				#ins_gi_monthlyinstalmentsamount = each.find('.//GInstAmounts/MonthlyInstalmentsAmount')
				#ins_gi_monthlyinstalmentsamount = pcb_get_value(ins_gi_monthlyinstalmentsamount)
				pcb_return['ins_gi_monthlyinstalmentsamount'] = ins_gi_monthlyinstalmentsamount

				ins_gi_remaininginstalmentsamount = try_find_xml(each,'.//GInstAmounts/RemainingInstalmentsAmount')
				#ins_gi_remaininginstalmentsamount = each.find('.//GInstAmounts/RemainingInstalmentsAmount')
				#ins_gi_remaininginstalmentsamount = pcb_get_value(ins_gi_remaininginstalmentsamount)
				pcb_return['ins_gi_remaininginstalmentsamount'] = ins_gi_remaininginstalmentsamount

				ins_gi_unpaiddueinstalmentsamount = try_find_xml(each,'.//GInstAmounts/UnpaidDueInstalmentsAmount')
				#ins_gi_unpaiddueinstalmentsamount = each.find('.//GInstAmounts/UnpaidDueInstalmentsAmount')
				#ins_gi_unpaiddueinstalmentsamount = pcb_get_value(ins_gi_unpaiddueinstalmentsamount)
				pcb_return['ins_gi_unpaiddueinstalmentsamount'] = ins_gi_unpaiddueinstalmentsamount


			for each in root.findall('.//NonInstalments'):

				non_numberofrequested = try_find_xml(each,'.//Summary/NumberOfRequested')
				#non_numberofrequested = each.find('.//Summary/NumberOfRequested')
				#non_numberofrequested = pcb_get_value(non_numberofrequested)
				pcb_return['non_numberofrequested'] = non_numberofrequested

				non_numberofliving = try_find_xml(each,'.//Summary/NumberOfLiving')
				#non_numberofliving = each.find('.//Summary/NumberOfLiving')
				#non_numberofliving = pcb_get_value(non_numberofliving)
				pcb_return['non_numberofliving'] = non_numberofliving

				non_numberofrefused = try_find_xml(each,'.//Summary/NumberOfRefused')
				#non_numberofrefused = each.find('.//Summary/NumberOfRefused')
				#non_numberofrefused = pcb_get_value(non_numberofrefused)
				pcb_return['non_numberofrefused'] = non_numberofrefused

				non_numberofrenounced = try_find_xml(each,'.//Summary/NumberOfRenounced')
				#non_numberofrenounced = each.find('.//Summary/NumberOfRenounced')
				#non_numberofrenounced = pcb_get_value(non_numberofrenounced)
				pcb_return['non_numberofrenounced'] = non_numberofrenounced

				non_numberofterminated = try_find_xml(each,'.//Summary/NumberOfTerminated')
				#non_numberofterminated = each.find('.//Summary/NumberOfTerminated')
				#non_numberofterminated = pcb_get_value(non_numberofterminated)
				pcb_return['non_numberofterminated'] = non_numberofterminated

				non_acni_creditlimit = try_find_xml(each,'.//ACNoInstAmounts/CreditLimit')
				#non_acni_creditlimit = each.find('.//ACNoInstAmounts/CreditLimit')
				#non_acni_creditlimit = pcb_get_value(non_acni_creditlimit)
				pcb_return['non_acni_creditlimit'] = non_acni_creditlimit

				non_acni_utilization = try_find_xml(each,'.//ACNoInstAmounts/Utilization')
				#non_acni_utilization = each.find('.//ACNoInstAmounts/Utilization')
				#non_acni_utilization = pcb_get_value(non_acni_utilization)
				pcb_return['non_acni_utilization'] = non_acni_utilization

				non_acni_overdraft = try_find_xml(each,'.//ACNoInstAmounts/Overdraft')
				#non_acni_overdraft = each.find('.//ACNoInstAmounts/Overdraft')
				#non_acni_overdraft = pcb_get_value(non_acni_overdraft)
				pcb_return['non_acni_overdraft'] = non_acni_overdraft

				non_gni_creditlimit = try_find_xml(each,'.//GNoInstAmounts/CreditLimit')
				#non_gni_creditlimit = each.find('.//GNoInstAmounts/CreditLimit')
				#non_gni_creditlimit = pcb_get_value(non_gni_creditlimit)
				pcb_return['non_gni_creditlimit'] = non_gni_creditlimit

				non_gni_utilization = try_find_xml(each,'.//GNoInstAmounts/Utilization')
				#non_gni_utilization = each.find('.//GNoInstAmounts/Utilization')
				#non_gni_utilization = pcb_get_value(non_gni_utilization)
				pcb_return['non_gni_utilization'] = non_gni_utilization

				non_gni_overdraft = try_find_xml(each,'.//GNoInstAmounts/Overdraft')
				#non_gni_overdraft = each.find('.//GNoInstAmounts/Overdraft')
				#non_gni_overdraft = pcb_get_value(non_gni_overdraft)
				pcb_return['non_gni_overdraft'] = non_gni_overdraft


			for each in root.findall('.//Cards'):

				
				card_numberofrequested = try_find_xml(each,'.//Summary/NumberOfRequested')
				#card_numberofrequested = pcb_get_value(card_numberofrequested)
				pcb_return['card_numberofrequested'] = card_numberofrequested

				
				card_numberofliving = try_find_xml(each,'.//Summary/NumberOfLiving')
				#card_numberofliving = pcb_get_value(card_numberofliving)
				pcb_return['card_numberofliving'] = card_numberofliving

				
				card_numberofrefused = try_find_xml(each,'.//Summary/NumberOfRefused')
				#card_numberofrefused = pcb_get_value(card_numberofrefused)
				pcb_return['card_numberofrefused'] = card_numberofrefused

				
				card_numberofrenounced = try_find_xml(each,'.//Summary/NumberOfRenounced')
				#card_numberofrenounced = pcb_get_value(card_numberofrenounced)
				pcb_return['card_numberofrenounced'] = card_numberofrenounced

				
				card_numberofterminated = try_find_xml(each,'.//Summary/NumberOfTerminated')
				#card_numberofterminated = pcb_get_value(card_numberofterminated)
				pcb_return['card_numberofterminated'] = card_numberofterminated

				
				card_acc_limitofcredit = try_find_xml(each,'.//ACCardAmounts/LimitOfCredit')
				#card_acc_limitofcredit = pcb_get_value(card_acc_limitofcredit)
				pcb_return['card_acc_limitofcredit'] = card_acc_limitofcredit

				
				card_acc_residualamount = try_find_xml(each,'.//ACCardAmounts/ResidualAmount')
				#card_acc_residualamount = pcb_get_value(card_acc_residualamount)
				pcb_return['card_acc_residualamount'] = card_acc_residualamount

				
				card_acc_overdueamount = try_find_xml(each,'.//ACCardAmounts/OverDueAmount')
				#card_acc_overdueamount = pcb_get_value(card_acc_overdueamount)
				pcb_return['card_acc_overdueamount'] = card_acc_overdueamount

				
				card_gc_limitofcredit = try_find_xml(each,'.//GCardAmounts/LimitOfCredit')
				#card_gc_limitofcredit = pcb_get_value(card_gc_limitofcredit)
				pcb_return['card_gc_limitofcredit'] = card_gc_limitofcredit

				
				card_gc_residualamount = try_find_xml(each,'.//GCardAmounts/ResidualAmount')
				#card_gc_residualamount = pcb_get_value(card_gc_residualamount)
				pcb_return['card_gc_residualamount'] = card_gc_residualamount

				
				card_gc_overdueamount = try_find_xml(each,'.//GCardAmounts/OverDueAmount')
				#card_gc_overdueamount = pcb_get_value(card_gc_overdueamount)
				pcb_return['card_gc_overdueamount'] = card_gc_overdueamount


			pcb_customer_infor = record_pcb_matched_customer_basic_infor(

				app_id = app_id_c,
				app_id_send = FIContractCode,
				found = pcb_return['found'],
				full_name = pcb_return['Name'],
				gender = pcb_return['Gender'],
				dob = pcb_return['DateOfBirth'],
				pob = pcb_return['PlaceOfBirth'],
				id_no = pcb_return['IDCard'],
				tax = pcb_return['TIN'],
				per_add = pcb_return['per_add'],
				per_city = pcb_return['per_city'],
				per_street = pcb_return['per_street'],
				curress_add = pcb_return['curress_add'],
				curress_city = pcb_return['curress_city'],
				curress_street = pcb_return['curress_street'],

				request_time = pcb_return['request_time'],
				debt_group_3_month = pcb_return['debt_group_3_month'],

				cbsubjectcode = pcb_return['CBSubjectCode'],
				fisubjectcode = pcb_return['FISubjectCode'],

				scoreraw = pcb_return['scoreraw'],
				scorecode = pcb_return['code'],
				scoredescription = pcb_return['description'],

				ex_scorecode = pcb_return['ex_scorecode'],
				ex_scoredescription = pcb_return['ex_scoredescription'],

				ins_numberofrequested = pcb_return['ins_numberofrequested'],
				ins_numberofliving = pcb_return['ins_numberofliving'],
				ins_numberofrefused = pcb_return['ins_numberofrefused'],
				ins_numberofrenounced = pcb_return['ins_numberofrenounced'],
				ins_numberofterminated = pcb_return['ins_numberofterminated'],
				ins_aci_monthlyinstalmentsamount = pcb_return['ins_aci_monthlyinstalmentsamount'],
				ins_aci_remaininginstalmentsamount = pcb_return['ins_aci_remaininginstalmentsamount'],
				ins_aci_unpaiddueinstalmentsamount = pcb_return['ins_aci_unpaiddueinstalmentsamount'],
				ins_gi_monthlyinstalmentsamount = pcb_return['ins_gi_monthlyinstalmentsamount'],
				ins_gi_remaininginstalmentsamount = pcb_return['ins_gi_remaininginstalmentsamount'],
				ins_gi_unpaiddueinstalmentsamount = pcb_return['ins_gi_unpaiddueinstalmentsamount'],


				non_numberofrequested = pcb_return['non_numberofrequested'],
				non_numberofliving = pcb_return['non_numberofliving'],
				non_numberofrefused = pcb_return['non_numberofrefused'],
				non_numberofrenounced = pcb_return['non_numberofrenounced'],
				non_numberofterminated = pcb_return['non_numberofterminated'],
				non_acni_creditlimit = pcb_return['non_acni_creditlimit'],
				non_acni_utilization = pcb_return['non_acni_utilization'],
				non_acni_overdraft = pcb_return['non_acni_overdraft'],
				non_gni_creditlimit = pcb_return['non_gni_creditlimit'],
				non_gni_utilization = pcb_return['non_gni_utilization'],
				non_gni_overdraft = pcb_return['non_gni_overdraft'],


				card_numberofrequested = pcb_return['card_numberofrequested'],
				card_numberofliving = pcb_return['card_numberofliving'],
				card_numberofrefused = pcb_return['card_numberofrefused'],
				card_numberofrenounced = pcb_return['card_numberofrenounced'],
				card_numberofterminated = pcb_return['card_numberofterminated'],
				card_acc_limitofcredit = pcb_return['card_acc_limitofcredit'],
				card_acc_residualamount = pcb_return['card_acc_residualamount'],
				card_acc_overdueamount = pcb_return['card_acc_overdueamount'],
				card_gc_limitofcredit = pcb_return['card_gc_limitofcredit'],
				card_gc_residualamount = pcb_return['card_gc_residualamount'],
				card_gc_overdueamount = pcb_return['card_gc_overdueamount']

			)

			pcb_customer_infor.save()

			pcb_return['id'] = pcb_customer_infor.id


			#phần này lấy thông tin số điện thoại (lưu ý mối quan hệ many to one để chứa trong bảng riêng)
			for each in root.findall('.//Matched'):

				
				refer_type = try_find_xml(each,'.//Person/Reference/Type')
				#refer_type = pcb_get_value(refer_type)

				
				refer_number = try_find_xml(each,'.//Person/Reference/Number')
				#refer_number = pcb_get_value(refer_number)


				pcb_customer_mobile = record_pcb_customer_mobile_phone(
					app_id_send = FIContractCode,
					refer_type = refer_type,
					refer_number = refer_number,
					matched_customer_id = pcb_customer_infor
				)

				pcb_customer_mobile.save()




			# phần này lấy thông tin của các khoản vay (installment)

			for each in root.findall('.//Contract/Instalments/GrantedContract'):

				
				cbcontractcode = try_find_xml(each,'.//CommonData/CBContractCode')
				#cbcontractcode = pcb_get_value(cbcontractcode)

				
				ficontractcode = try_find_xml(each,'.//CommonData/FIContractCode')
				#ficontractcode = pcb_get_value(ficontractcode)

				
				currency = try_find_xml(each,'.//CommonData/Currency')
				#currency = pcb_get_value(currency)

				
				referencenumber = try_find_xml(each,'.//CommonData/ReferenceNumber')
				#referencenumber = pcb_get_value(referencenumber)

				
				role = try_find_xml(each,'.//CommonData/Role')
				#role = pcb_get_value(role)

				
				encryptedficode = try_find_xml(each,'.//CommonData/EncryptedFICode')
				#encryptedficode = pcb_get_value(encryptedficode)

				
				typeoffinancing = try_find_xml(each,'.//CommonData/TypeOfFinancing')
				#typeoffinancing = pcb_get_value(typeoffinancing)

				
				contractphase = try_find_xml(each,'.//CommonData/ContractPhase')
				#contractphase = pcb_get_value(contractphase)

				
				startingdate = try_find_xml(each,'.//CommonData/StartingDate')
				#startingdate = pcb_get_value(startingdate)

				
				dateoflastupdate = try_find_xml(each,'.//CommonData/DateOfLastUpdate')
				#dateoflastupdate = pcb_get_value(dateoflastupdate)

				
				enddateofcontract = try_find_xml(each,'.//EndDateOfContract')
				#enddateofcontract = pcb_get_value(enddateofcontract)

				
				monthlyinstalmentamount = try_find_xml(each,'.//MonthlyInstalmentAmount')
				#monthlyinstalmentamount = pcb_get_value(monthlyinstalmentamount)

				
				remaininginstalmentsamount = try_find_xml(each,'.//RemainingInstalmentsAmount')
				#remaininginstalmentsamount = pcb_get_value(remaininginstalmentsamount)

				
				worststatus = try_find_xml(each,'.//WorstStatus')
				#worststatus = pcb_get_value(worststatus)


				instalment_loan = record_pcb_customer_loan_infor(

					app_id_send = FIContractCode,
					matched_customer_id = pcb_customer_infor,
					
					loantype = "instalment",

					cbcontractcode = cbcontractcode,
					ficontractcode = ficontractcode,
					currency = currency,
					referencenumber = referencenumber,
					role = role,

					encryptedficode = encryptedficode,
					typeoffinancing = typeoffinancing,
					contractphase = contractphase,
					startingdate = startingdate,
					dateoflastupdate = dateoflastupdate,

					enddateofcontract = enddateofcontract,
					monthlyinstalmentamount = monthlyinstalmentamount,
					remaininginstalmentsamount = remaininginstalmentsamount,

					worststatus = worststatus

				)


				instalment_loan.save()


			# phần này lấy thông tin của các khoản vay (non-installment)

			for each in root.findall('.//Contract/NonInstalments/GrantedContract'):

				
				cbcontractcode = try_find_xml(each,'.//CommonData/CBContractCode')
				#cbcontractcode = pcb_get_value(cbcontractcode)

				
				ficontractcode = try_find_xml(each,'.//CommonData/FIContractCode')
				#ficontractcode = pcb_get_value(ficontractcode)

				
				currency = try_find_xml(each,'.//CommonData/Currency')
				#currency = pcb_get_value(currency)

				
				referencenumber = try_find_xml(each,'.//CommonData/ReferenceNumber')
				#referencenumber = pcb_get_value(referencenumber)

				
				role = try_find_xml(each,'.//CommonData/Role')
				#role = pcb_get_value(role)

				
				encryptedficode = try_find_xml(each,'.//CommonData/EncryptedFICode')
				#encryptedficode = pcb_get_value(encryptedficode)

				
				typeoffinancing = try_find_xml(each,'.//CommonData/TypeOfFinancing')
				#typeoffinancing = pcb_get_value(typeoffinancing)

				
				contractphase = try_find_xml(each,'.//CommonData/ContractPhase')
				#contractphase = pcb_get_value(contractphase)

				
				startingdate = try_find_xml(each,'.//CommonData/StartingDate')
				#startingdate = pcb_get_value(startingdate)

				
				dateoflastupdate = try_find_xml(each,'.//CommonData/DateOfLastUpdate')
				#dateoflastupdate = pcb_get_value(dateoflastupdate)

				
				enddateofcontract = try_find_xml(each,'.//EndDateOfContract')
				#enddateofcontract = pcb_get_value(enddateofcontract)

				
				worststatus = try_find_xml(each,'.//WorstStatus')
				#worststatus = pcb_get_value(worststatus)


				noninstalment_loan = record_pcb_customer_loan_infor(

					app_id_send = FIContractCode,
					matched_customer_id = pcb_customer_infor,
					loantype = "noninstalment",
					cbcontractcode = cbcontractcode,
					ficontractcode = ficontractcode,
					currency = currency,
					referencenumber = referencenumber,
					role = role,

					encryptedficode = encryptedficode,
					typeoffinancing = typeoffinancing,
					contractphase = contractphase,
					startingdate = startingdate,
					dateoflastupdate = dateoflastupdate,

					enddateofcontract = enddateofcontract,

					worststatus = worststatus

				)

				noninstalment_loan.save()



				# phần này lấy thông tin của các khoản vay (cards)


			for each in root.findall('.//Contract/Cards/GrantedContract'):

				
				cbcontractcode = try_find_xml(each,'.//CommonData/CBContractCode')
				#cbcontractcode = pcb_get_value(cbcontractcode)

				
				ficontractcode = try_find_xml(each,'.//CommonData/FIContractCode')
				#ficontractcode = pcb_get_value(ficontractcode)

				
				currency = try_find_xml(each,'.//CommonData/Currency')
				#currency = pcb_get_value(currency)

				
				referencenumber = try_find_xml(each,'.//CommonData/ReferenceNumber')
				#referencenumber = pcb_get_value(referencenumber)

				
				role = try_find_xml(each,'.//CommonData/Role')
				#role = pcb_get_value(role)

				
				encryptedficode = try_find_xml(each,'.//CommonData/EncryptedFICode')
				#encryptedficode = pcb_get_value(encryptedficode)

				
				typeoffinancing = try_find_xml(each,'.//CommonData/TypeOfFinancing')
				#typeoffinancing = pcb_get_value(typeoffinancing)

				
				contractphase = try_find_xml(each,'.//CommonData/ContractPhase')
				#contractphase = pcb_get_value(contractphase)

				
				startingdate = try_find_xml(each,'.//CommonData/StartingDate')
				#startingdate = pcb_get_value(startingdate)

				
				dateoflastupdate = try_find_xml(each,'.//CommonData/DateOfLastUpdate')
				#dateoflastupdate = pcb_get_value(dateoflastupdate)

				
				enddateofcontract = try_find_xml(each,'.//EndDateOfContract')
				#enddateofcontract = pcb_get_value(enddateofcontract)

				
				monthlyinstalmentamount = try_find_xml(each,'.//MonthlyInstalmentAmount')
				#monthlyinstalmentamount = pcb_get_value(monthlyinstalmentamount)
				#remaininginstalmentsamount = try_find_xml(each,'.//RemainingInstalmentsAmount')
				
				
				creditlimit = try_find_xml(each,'.//CreditLimit')
				#creditlimit = pcb_get_value(creditlimit)

				
				residualamount = try_find_xml(each,'.//ResidualAmount')
				#residualamount = pcb_get_value(residualamount)

				
				worststatus = try_find_xml(each,'.//WorstStatus')
				#worststatus = pcb_get_value(worststatus)

				card_loan = record_pcb_customer_loan_infor(

					app_id_send = FIContractCode,
					matched_customer_id = pcb_customer_infor,
					loantype = "card",

					cbcontractcode = cbcontractcode,
					ficontractcode = ficontractcode,
					currency = currency,
					referencenumber = referencenumber,
					role = role,

					encryptedficode = encryptedficode,
					typeoffinancing = typeoffinancing,
					contractphase = contractphase,
					startingdate = startingdate,
					dateoflastupdate = dateoflastupdate,

					enddateofcontract = enddateofcontract,         
					monthlyinstalmentamount = monthlyinstalmentamount,

					creditlimit = creditlimit,
					residualamount = residualamount,


					worststatus = worststatus

				)

				card_loan.save()

	return pcb_return