    
from checklist.models import record_phv_sending_fiv_request, record_phv_sending_fiv_request_hist
from django.db import connections
import cx_Oracle

def user_create_new_fiv_request( app_id_c, f1_cust_id, addresstype, send_fiv, 
                                householder1, relationship1, room_number, partner_other, 
                                describe_the_ways, note, 
                                username, loan_amount, client_name, year_of_birth, id_card, gender, marial_status, spouse_name,
                                spouse_company, client_phone_number, spouse_phone_number, owning_status, householder, relationship, company_name):

  oracle_sql3 = ( "SELECT 1 as id," +
                   "      A.address1, B.zipdesc, A.city_lms, A.statedesc, A.addresstype, A.mobile " +
                   "FROM " +
                   "      MACAS.NBFC_ADDRESS_M A " +
                   "LEFT JOIN " +
                   "    (SELECT zipcode, zipdesc FROM macas.nbfc_city_zipcode_m) B ON A.zipcode = B.zipcode " +  
                   "WHERE A.BPID = to_char (%s) AND A.ADDRESSTYPE = %s " )

  queryset_list3  = record_phv_sending_fiv_request.objects.using('finnonedb').raw(oracle_sql3,[f1_cust_id, addresstype])


  for house in queryset_list3:
    post_instance = record_phv_sending_fiv_request.objects.create(  app_id = app_id_c,
                                                                   house_number = house.address1,
                                                                   ward3 = house.zipdesc,
                                                                   dictrict3 = house.city_lms,
                                                                   city3 = house.statedesc,
                                                                   type_of_address = house.addresstype,
                                                                   requested_by = username,
                                                                   loan_amount = loan_amount,
                                                                   client_name = client_name,
                                                                   year_of_birth = year_of_birth,
                                                                   id_card = id_card,
                                                                   gender = gender,
                                                                   marial_status = marial_status,
                                                                   spouse_name = spouse_name,
                                                                   spouse_company = spouse_company,
                                                                   client_phone_number = client_phone_number,
                                                                   spouse_phone_number = spouse_phone_number,
                                                                   owning_status = owning_status,
                                                                   householder = householder,
                                                                   relationship = relationship,
                                                                   householder1 = householder1,
                                                                   relationship1 = relationship1,
                                                                   room_number = room_number,
                                                                   company_name = company_name,
                                                                   partner_other = partner_other,
                                                                   describe_the_ways = describe_the_ways,
                                                                   note = note,
                                                                   send_fiv = send_fiv)



    post_instance = record_phv_sending_fiv_request_hist.objects.create(
                                                                   app_id = app_id_c,
                                                                   type_of_address = house.addresstype,
                                                                   requested_by = username,
                                                                   householder1 = householder1,
                                                                   relationship1 = relationship1,
                                                                   room_number = room_number,
                                                                   partner_other = partner_other,
                                                                   describe_the_ways = describe_the_ways,
                                                                   note =note,
                                                                   send_fiv = send_fiv,
                                                                   hist_type = 'CREATE'
                                                                       )