from django.db import models, signals
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in



class LoginUpdate(models.Model):
    date_updated = models.DateTimeField(auto_now_add=False, null=True)
    action_type = models.CharField(max_length=5)
    action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
    action_username = models.CharField(max_length=50)

    def __str__(self):
        return self.action_user



class record_datachecker_result (models.Model):


	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	paper_type = models.CharField(db_index = True, max_length=200)
	paper_type_item = models.CharField(db_index = True, max_length=200, null=True)
	checked_result = models.CharField(max_length=200)
	checked_code = models.CharField(max_length=200)
	checked_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True,on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_datachecker_pending_defer_history (models.Model):


	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	action_type = models.CharField(db_index = True, max_length=200)
	action_type_detail = models.CharField(db_index = True, max_length=200)
	action_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_phv_result_cl (models.Model):


	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	check_type = models.CharField(db_index = True, max_length=200)
	checked_result = models.CharField(max_length=200)
	check_type_detail = models.CharField(db_index = True, max_length=200, null=True)
	checked_type_uw_decision = models.CharField(max_length=200)
	checked_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_phv_comment_for_each_mobile (models.Model):

	app_id = models.CharField(db_index = True, max_length=200)
	called_mobi = models.CharField(db_index = True, max_length=200)
	relationship = models.CharField(null = True, max_length=100)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	phv_decision_after_call = models.CharField(max_length=200)
	phv_emoji_after_call = models.CharField(max_length=200)
	phv_comment_4_mobi = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)




class record_phv_dti_calculation(models.Model):
	app_id = models.CharField(db_index = True, max_length=200)
	f1_priority = models.CharField(db_index = True, max_length=200)
	f1_sale_officer = models.CharField(null = True, max_length=100)
	f1_pos = models.CharField(null = True, max_length=100)
	f1_amount = models.CharField(max_length=200)
	f1_emi = models.CharField(max_length=200)
	f1_tenure = models.CharField(max_length=100)
	f1_product = models.CharField(max_length=100, null=True)
	f1_financial_name = models.CharField(max_length=1000)
	f1_total_outstanding = models.CharField(max_length=100)
	f1_total_emi = models.CharField(max_length=100)
	f1_ir = models.CharField(max_length=100)
	phv_phv_emi = models.CharField(max_length=100)
	laa_app_req_amtfin_n = models.CharField(max_length=100)
	laa_app_loandtl_term_n = models.CharField(max_length=100)
	monthly_income = models.CharField(max_length=100)
	dti = models.CharField(max_length=100)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50, )
	date_created = models.DateTimeField(auto_now_add=False, null=True)




class record_phv_sending_fiv_request (models.Model):
	app_id = models.CharField(db_index = True, max_length=200)
	house_number = models.CharField(max_length=500, null=True)
	ward3 = models.CharField(max_length=200, null=True)
	dictrict3 = models.CharField(max_length=200, null=True)
	city3 = models.CharField(max_length=100, null=True)
	type_of_address = models.CharField(max_length=200, null=True)
	pending_date = models.DateTimeField(auto_now_add=True)
	date_request = models.DateTimeField(auto_now_add=True)
	requested_by = models.CharField(max_length=200, null=True)
	loan_amount = models.CharField(max_length=200, null=True)
	client_name = models.CharField(max_length=200, null=True)
	year_of_birth = models.CharField(max_length=100, null=True)
	id_card = models.CharField(db_index = True, max_length=100, null=True)
	gender = models.CharField(max_length=20, null=True)
	marial_status = models.CharField(max_length=20, null=True)
	spouse_name = models.CharField(max_length=200, null=True)
	spouse_company = models.CharField(max_length=500, null=True)
	client_phone_number = models.CharField(max_length=100, null=True)
	spouse_phone_number = models.CharField(max_length=100, null=True)
	owning_status = models.CharField(max_length=100, null=True)
	householder = models.CharField(max_length=200, null=True)
	relationship = models.CharField(max_length=100, null=True)
	householder1 = models.CharField(max_length=300, null=True)
	relationship1 = models.CharField(max_length=100, null=True)
	room_number = models.CharField(max_length=100, null=True)
	company_name = models.CharField(max_length=300, null=True)
	partner_other = models.CharField(max_length=100, null=True)
	describe_the_ways = models.CharField(max_length=2000, null=True)
	note = models.CharField(max_length=2000, null=True)
	send_fiv = models.CharField(max_length=20, null=True)
	update_date = models.DateTimeField(auto_now=True)




class record_phv_sending_fiv_request_hist (models.Model):
	app_id = models.CharField(db_index = True, max_length=200)
	type_of_address = models.CharField(max_length=200, null=True)
	requested_by = models.CharField(max_length=200, null=True)
	householder1 = models.CharField(max_length=300, null=True)
	relationship1 = models.CharField(max_length=100, null=True)
	room_number = models.CharField(max_length=100, null=True)
	partner_other = models.CharField(max_length=100, null=True)
	describe_the_ways = models.CharField(max_length=2000, null=True)
	note = models.CharField(max_length=2000, null=True)
	send_fiv = models.CharField(max_length=20, null=True)
	hist_type = models.CharField(max_length=200, null=True)
	date_create = models.DateTimeField(auto_now_add=True)






class record_phv_pending_defer_history (models.Model):
	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	action_type = models.CharField(db_index = True, max_length=200)
	action_type_detail = models.CharField(db_index = True, max_length=200)
	action_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_und_result_cl (models.Model):

	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	paper_type = models.CharField(db_index = True, max_length=200)
	paper_type_item = models.CharField(db_index = True, max_length=200, null=True)
	checked_result = models.CharField(max_length=200)
	checked_code = models.CharField(max_length=200)
	checked_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_und_pending_defer_history (models.Model):

	app_id = models.CharField(db_index = True, max_length=200)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	action_type = models.CharField(db_index = True, max_length=200)
	action_type_detail = models.CharField(db_index = True, max_length=200)
	action_comment = models.CharField(max_length=3000)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)




#riêng đối với phần note của company, mặc dù tên table có ghi của UND nhưng thực chất sẽ dùng chung cho UND, DC, PHV
class record_und_newest_note_for_company (models.Model):

	f1taxcode = models.CharField( db_index = True, max_length=200, unique=True)
	f1employername = models.CharField( max_length=200, null=True)
	riskboxtaxcode = models.CharField( db_index = True, max_length=200, null=True)
	employername = models.CharField( max_length=200)
	risknote = models.CharField( max_length=1000, null=True)
	dcnote = models.CharField( max_length=1000, null=True)
	phvnote = models.CharField( max_length=1000, null=True)
	undnote = models.CharField( max_length=1000, null=True)
	sup_tl_note = models.CharField( max_length=1000, null=True)
	risk_date_created = models.DateTimeField(auto_now_add=False, null=True)
	risk_date_updated = models.DateTimeField(auto_now_add=False, null=True)
	dc_date_created = models.DateTimeField(auto_now_add=False, null=True)
	dc_date_updated = models.DateTimeField(auto_now_add=False, null=True)
	phv_date_created = models.DateTimeField(auto_now_add=False, null=True)
	phv_date_updated = models.DateTimeField(auto_now_add=False, null=True)
	und_date_created = models.DateTimeField(auto_now_add=False, null=True)
	und_date_updated = models.DateTimeField(auto_now_add=False, null=True)
	sup_tl_date_created = models.DateTimeField(auto_now_add=False, null=True)
	sup_tl_date_updated = models.DateTimeField(auto_now_add=False, null=True)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)


class record_und_note_history_for_company (models.Model):

	f1taxcode = models.CharField( db_index = True, max_length=200)
	f1employername = models.CharField( max_length=200, null=True)
	riskboxtaxcode = models.CharField( db_index = True, max_length=200, null=True)
	employername = models.CharField( max_length=200)
	risknote = models.CharField( max_length=1000, null=True)
	dcnote = models.CharField( max_length=1000, null=True)
	phvnote = models.CharField( max_length=1000, null=True)
	undnote = models.CharField( max_length=1000, null=True)
	sup_tl_note = models.CharField( max_length=1000, null=True)
	date_created = models.DateTimeField(auto_now_add=False, null=True)
	action_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT)
	action_username = models.CharField(max_length=50)



class record_nice_score_history_for_app (models.Model):

	app_id = models.CharField(db_index = True, max_length=200)
	nice_score = models.CharField(max_length=200)
	check_date = models.DateTimeField(auto_now_add=False, auto_now=False, null=False)
	record_date = models.DateTimeField(auto_now_add=False, auto_now=True, null=False)
	record_team = models.CharField(max_length=200)
	user_get_app = models.CharField(max_length=200)

	class Meta:
		unique_together = ["app_id", "nice_score", "check_date"]




class record_pcb_matched_customer_basic_infor (models.Model):

	app_id = models.CharField(db_index = True, max_length=100)
	app_id_send = models.CharField(db_index = True, max_length=100, null=True)
	found =  models.CharField(max_length=50)
	error_cat = models.CharField(max_length=30, null=True)
	error_code = models.CharField(max_length=30, null=True)
	error_desc = models.CharField(max_length=500, null=True)
	full_name = models.CharField(max_length=300, null=True)
	gender = models.CharField(max_length=5, null=True)
	dob = models.CharField(max_length=12, null=True)
	pob = models.CharField(max_length=100, null=True)
	id_no = models.CharField(max_length=20, null=True)
	tax = models.CharField(max_length=20, null=True)
	per_add = models.CharField(max_length=1000, null=True)
	per_city = models.CharField(max_length=100, null=True)
	per_street = models.CharField(max_length=500, null=True)
	curress_add = models.CharField(max_length=1000, null=True)
	curress_city = models.CharField(max_length=100, null=True)
	curress_street = models.CharField(max_length=500, null=True)

	request_time = models.CharField(max_length=10, null=True)
	debt_group_3_month = models.CharField(max_length=10, null=True)

	cbsubjectcode = models.CharField(max_length=30, null=True)
	fisubjectcode = models.CharField(max_length=30, null=True)

	#person
	#individualconcern
	#company

	scoreraw = models.CharField(max_length=10, null=True)
	scorecode = models.CharField(max_length=10, null=True)
	scoredescription = models.CharField(max_length=100, null=True)

	ex_scorecode = models.CharField(max_length=10, null=True)
	ex_scoredescription = models.CharField(max_length=300, null=True)

	ins_numberofrequested = models.CharField(max_length=10, null=True)
	ins_numberofliving = models.CharField(max_length=10, null=True)
	ins_numberofrefused = models.CharField(max_length=10, null=True)
	ins_numberofrenounced = models.CharField(max_length=10, null=True)
	ins_numberofterminated = models.CharField(max_length=10, null=True)
	ins_aci_monthlyinstalmentsamount = models.CharField(max_length=20, null=True)
	ins_aci_remaininginstalmentsamount = models.CharField(max_length=10, null=True)
	ins_aci_unpaiddueinstalmentsamount = models.CharField(max_length=10, null=True)
	ins_gi_monthlyinstalmentsamount = models.CharField(max_length=20, null=True)
	ins_gi_remaininginstalmentsamount = models.CharField(max_length=10, null=True)
	ins_gi_unpaiddueinstalmentsamount = models.CharField(max_length=10, null=True)


	non_numberofrequested = models.CharField(max_length=10, null=True)
	non_numberofliving = models.CharField(max_length=10, null=True)
	non_numberofrefused = models.CharField(max_length=10, null=True)
	non_numberofrenounced = models.CharField(max_length=10, null=True)
	non_numberofterminated = models.CharField(max_length=10, null=True)
	non_acni_creditlimit = models.CharField(max_length=20, null=True)
	non_acni_utilization = models.CharField(max_length=20, null=True)
	non_acni_overdraft = models.CharField(max_length=20, null=True)
	non_gni_creditlimit = models.CharField(max_length=20, null=True)
	non_gni_utilization = models.CharField(max_length=20, null=True)
	non_gni_overdraft = models.CharField(max_length=20, null=True)


	card_numberofrequested = models.CharField(max_length=10, null=True)
	card_numberofliving = models.CharField(max_length=10, null=True)
	card_numberofrefused = models.CharField(max_length=10, null=True)
	card_numberofrenounced = models.CharField(max_length=10, null=True)
	card_numberofterminated = models.CharField(max_length=10, null=True)
	card_acc_limitofcredit = models.CharField(max_length=20, null=True)
	card_acc_residualamount = models.CharField(max_length=20, null=True)
	card_acc_overdueamount = models.CharField(max_length=20, null=True)
	card_gc_limitofcredit = models.CharField(max_length=20, null=True)
	card_gc_residualamount = models.CharField(max_length=20, null=True)
	card_gc_overdueamount = models.CharField(max_length=20, null=True)


	check_date = models.DateTimeField(auto_now_add=False, auto_now=True, null=False)



class record_pcb_customer_loan_infor (models.Model):

	app_id_send = models.CharField(db_index = True, max_length=100, null=True)
	matched_customer_id = models.ForeignKey(record_pcb_matched_customer_basic_infor,
											on_delete=models.CASCADE)
	
	loantype = models.CharField(max_length=30, null=True)

	cbcontractcode = models.CharField(max_length=100, null=True)
	ficontractcode = models.CharField(max_length=100, null=True)
	currency = models.CharField(max_length=50, null=True)
	referencenumber = models.CharField(max_length=50, null=True)
	role = models.CharField(max_length=50, null=True)

	encryptedficode = models.CharField(max_length=50, null=True)
	typeoffinancing = models.CharField(max_length=50, null=True)
	contractphase = models.CharField(max_length=50, null=True)
	startingdate = models.CharField(max_length=50, null=True)
	dateoflastupdate = models.CharField(max_length=50, null=True)


	enddateofcontract = models.CharField(max_length=100, null=True)
	monthlyinstalmentamount = models.CharField(max_length=100, null=True)
	remaininginstalmentsamount = models.CharField(max_length=100, null=True)
	

	creditlimit = models.CharField(max_length=50, null=True)
	residualamount = models.CharField(max_length=50, null=True)


	worststatus = models.CharField(max_length=50, null=True)



class record_pcb_customer_mobile_phone (models.Model):

	app_id_send = models.CharField(db_index = True, max_length=100, null=True)
	matched_customer_id = models.ForeignKey(record_pcb_matched_customer_basic_infor,
											on_delete=models.CASCADE)
	refer_type = models.CharField(max_length=20, null=True)
	refer_number = models.CharField(max_length=50, null=True)



class dedupe_id_model_data_finnone (models.Model):
	app_id = models.CharField(db_index = True, max_length=100, null=True)
	customer_name = models.CharField(max_length=300, null=True)
	customer_id_numer = models.CharField(max_length=100, null=True)
	dob = models.CharField(max_length=20, null=True)
	dpd = models.CharField(max_length=100, null=True)
	issue_place = models.CharField(max_length=200, null=True)
	operate_date = models.CharField(max_length=200, null=True)
	product = models.CharField(max_length=200, null=True)
	status = models.CharField(max_length=200, null=True)
	current_province = models.CharField(max_length=300, null=True)
	fb_province = models.CharField(max_length=300, null=True)
	on_system = models.CharField(max_length=100, null=True)
	spouse_id = models.CharField(max_length=300, null=True)
	spouse_name = models.CharField(max_length=300, null=True)



class dedupe_mobile_cux_model_data_finnone (models.Model):
	app_id_c = models.CharField(max_length=30, null=True)
	cif_no = models.CharField(max_length=30, null=True)
	addresstype = models.CharField(max_length=30, null=True)
	phone1 = models.CharField(max_length=50, null=True)
	mobile = models.CharField(max_length=110, null=True)
	address1 = models.CharField(max_length=120, null=True)
	dpd = models.DecimalField( max_digits=8, decimal_places=0, null=True, blank=True)
	status = models.CharField(max_length=20, null=True)



class dedupe_mobile_ref_model_data_finnone (models.Model):
	app_id_c = models.CharField(max_length=30, null=True)
	cif_no = models.CharField(max_length=30, null=True)
	reference_name = models.CharField(max_length=300, null=True)
	ref_relaion = models.CharField(max_length=10, null=True)
	phone1 = models.CharField(max_length=50, null=True)
	phone2 = models.CharField(max_length=50, null=True)
	stdisd = models.CharField(max_length=50, null=True)
	dpd = models.DecimalField( max_digits=8, decimal_places=0, null=True, blank=True)
	status = models.CharField(max_length=20, null=True)