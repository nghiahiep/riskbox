class DBRouter(object):

    def db_for_read(self, model, **hints):
        """Send all read operations on 'app_label' app models to associated db"""
        if model._meta.app_label == 'storage_data':
            return 'datadb'

        return None


    def db_for_write(self, model, **hints):
        """Send all write operations on 'app_label' app models to associated db"""
        if model._meta.app_label == 'storage_data':
            return 'datadb'

        return None
        

    def allow_relation(self, obj1, obj2, **hints):
        """Determine if relationship is allowed between two objects."""

        # Allow any relation between two models that are in the same app.
        # I prefer to make this global by using the following syntax
        return obj1._meta.app_label == obj2._meta.app_label


    def allow_migrate(self, db, app_label, model_name=None, **hints):
        
        # I think this was your biggest misunderstanding
        # the db_for_write will pick the correct DB for the migration
        # allow_migrate will only let you say which apps/dbs you 
        # should not migrate.  I *strongly* recommend not taking
        # the belt and braces approach that you had here.
        if app_label == 'storage_data':
            return db == 'datadb'
        return None

        #return True