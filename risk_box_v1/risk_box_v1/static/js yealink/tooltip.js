///////////////////////////////////////////////////////////////////////
//
function Tooltip() {  // The constructor function for the Tooltip class
    this.src = null;
    this.tooltip = document.createElement("div"); // create div for shadow
    this.tooltip.style.visibility = "hidden";     // starts off hidden
    this.tooltip.className = "tooltipShadow";     // so we can style it
	
    this.content = document.createElement("div"); // create div for content
    this.content.style.position = "relative";     // relatively positioned
    this.content.className = "tooltipContent";    // so we can style it
	this.content.appendChild(document.createTextNode("initial characters"));
	
    this.tooltip.appendChild(this.content);       // add content to shadow
	// compatible with ie6
	if(Sys && parseInt(Sys.ie) == 6)
	{
		this.selectCover = document.createElement("iframe");
		this.selectCover.height = DHTMLAPI.getElementHeight(this.selectCover.parentNode);
		this.tooltip.appendChild(this.selectCover);
	}
}

// Set the content and position of the tooltip and display it
Tooltip.prototype.show = function(text, x, y) {
    this.content.innerHTML = text;             // Set the text of the tooltip.
    this.tooltip.style.left = x + "px";        // Set the position.
    this.tooltip.style.top = y + "px";
    this.tooltip.style.visibility = "visible"; // Make it visible.

    // Add the tooltip to the document if it has not been added before
    if (this.tooltip.parentNode != document.body)
        document.body.appendChild(this.tooltip);
	//beneath the bottom of viewport
	var tipHeight = DHTMLAPI.getElementHeight(this.tooltip);
	var iOver = y - Geometry.getVerticalScroll() + tipHeight - Geometry.getViewportHeight();
	if(iOver > 0){
		if((y - tipHeight) < Geometry.getVerticalScroll())
			this.tooltip.style.top = Geometry.getVerticalScroll() + "px";
		else
			this.tooltip.style.top = (y - tipHeight) + "px";
	}
	if(this.selectCover){
		this.selectCover.height = DHTMLAPI.getElementHeight(this.selectCover.parentNode);
	}
};

// Hide the tooltip
Tooltip.prototype.hide = function() {
    this.tooltip.style.visibility = "hidden";  // Make it invisible.
};
Tooltip.X_OFFSET = 25;  // Pixels to the right of the mouse pointer
Tooltip.Y_OFFSET = 0;  // Pixels below the mouse pointer
Tooltip.DELAY = 500;    // Milliseconds after mouseover
Tooltip.prototype.schedule = function(target, e) {
    // Get the text to display.  If none, we don't do anything
    var text = target.getAttribute("tooltip");
    if (!text) return;
    this.src = e.target ? e.target : e.srcElement;
    // The event object holds the mouse position in window coordinates
    // We convert these to document coordinates using the Geometry module
    var x = e.clientX + Geometry.getHorizontalScroll();
    var y = e.clientY + Geometry.getVerticalScroll();

    // Add the offsets so the tooltip doesn't appear right under the mouse
    x += Tooltip.X_OFFSET;
    y += Tooltip.Y_OFFSET;

    // Schedule the display of the tooltip
    var self = this;  // We need this for the nested functions below
	if (g_IF_V73 || g_IF_V80)
	{
		self.show(text, x, y);
	}
	else
	{
		var timer = window.setTimeout(function() { self.show(text, x, y); },
                                  Tooltip.DELAY);
	}
	
    // Also, register an onmouseout handler to hide a tooltip or cancel
    // the pending display of a tooltip.
    if (target.addEventListener) target.addEventListener("mouseout", mouseout,
                                                         false);
    else if (target.attachEvent) target.attachEvent("onmouseout", mouseout);
    else target.onmouseout = mouseout;

    // Here is the implementation of the event listener
    function mouseout() {
        if(g_IF_V73 || g_IF_V80) {return;}
        self.hide();                // Hide the tooltip if it is displayed,
        window.clearTimeout(timer); // cancel any pending display,
        // and remove ourselves so we're called only once
        if (target.removeEventListener) 
            target.removeEventListener("mouseout", mouseout, false);
        else if (target.detachEvent) target.detachEvent("onmouseout",mouseout);
        else target.onmouseout = null;
    }
}

// Define a single global Tooltip object for general use
Tooltip.tooltip = new Tooltip();
Tooltip.schedule = function(target, e) { Tooltip.tooltip.schedule(target, e); }
Tooltip.hide = function() { Tooltip.tooltip.hide(); }