"""risk_box_v1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from main_page.views.login_page import page as alias_login_page

from dedupe_page.views.dedupe_page import page as alias_dedupe_page
from dedupe_page.views.dedupe_mobile_page import page as alias_dedupe_mobile_page

from cat_page.views.request_cat_page import page as alias_request_cat_page

from checklist.views.checklist_dc_page import page as alias_checklist_dc_page
from checklist.views.checklist_dc_page import get_app_data_from_finnone_for_dedupe_and_blacklist
from checklist.views.checklist_dc_page import search_customer_information_with_full_information_id_dob_name
from checklist.views.checklist_dc_page import search_customer_information_with_home_directory_style
from checklist.views.checklist_dc_page import search_customer_spouse_information_with_free_style
from checklist.views.checklist_dc_page import dc_cl_super_total_comment
from checklist.views.checklist_dc_page import dc_create_update_company_information


from checklist.views.checklist_phv_page import page as alias_checklist_phv_page
from checklist.views.checklist_phv_page import get_app_data_from_finnone_before_using_funtion_of_dedupe_mobi
from checklist.views.checklist_phv_page import search_customer_information_with_full_information_id_dob_name_on_phv_checklist
from checklist.views.checklist_phv_page import search_customer_information_with_home_directory_style_on_phv_checklist
from checklist.views.checklist_phv_page import search_customer_spouse_information_with_free_style_on_phv_checklist
from checklist.views.checklist_phv_page import search_mobile_dedupe_mobile_phone_cux_and_ref_on_phv_checklist
from checklist.views.checklist_phv_page import phv_create_update_company_information
from checklist.views.checklist_phv_page import phv_cl_super_total_comment
from checklist.views.checklist_phv_page import phv_comment_for_every_mobile_check
from checklist.views.checklist_phv_page import phv_create_send_fiv_data
from checklist.views.checklist_phv_page import phv_send_request_pcb


from checklist.views.checklist_und_page import page as alias_checklist_und_page
from checklist.views.checklist_und_page import get_app_data_from_finnone_before_using_funtion_of_checklist_und
from checklist.views.checklist_und_page import search_customer_information_with_full_information_id_dob_name_on_und_checklist
from checklist.views.checklist_und_page import search_customer_information_with_home_directory_style_on_und_checklist
from checklist.views.checklist_und_page import search_customer_spouse_information_with_free_style_on_und_checklist
from checklist.views.checklist_und_page import search_mobile_dedupe_mobile_phone_cux_and_ref_on_und_checklist
from checklist.views.checklist_und_page import und_create_update_company_information
from checklist.views.checklist_und_page import und_cl_super_total_comment


from fi_page.views.fi_und_page import page as alias_fi_und_page
from fi_page.views.fi_und_page import export_fi_request_to_xls

#phần này để update CSDL từ Finnone
from checklist.views.checklist_dc_page import ChecklistDedupeIDFinnoneDLView

#đừng hỏi tại sao mình thích đặt url dài, mình thích thì mình đặt thôi, ok!



urlpatterns = [
    url(r'^admin/', admin.site.urls,),
    url(r'^$', alias_login_page, name='login_page'),
    url(r'^dedupe/', alias_dedupe_page, name='dedupe_page'),

    url(r'^dedupe_mobile/', alias_dedupe_mobile_page, name='dedupe_mobile_page'),

    url(r'^request_cat/', alias_request_cat_page, name='request_cat_page'),



    url(r'^checklist_dc/', alias_checklist_dc_page, name='checklist_dc_page'),
    #cụm trả kết quả xử lý black list
    url(r'^get_app_data_from_finnone_for_dedupe_and_blacklist/$', get_app_data_from_finnone_for_dedupe_and_blacklist, name='get_app_data_from_finnone_for_dedupe_and_blacklist'),
    #cụm trả kết quả tra dedupe
    url(r'^search_customer_information_with_full_information_id_dob_name/$', search_customer_information_with_full_information_id_dob_name, name='search_customer_information_with_full_information_id_dob_name'),
    url(r'^search_customer_information_with_home_directory_style/$', search_customer_information_with_home_directory_style, name='search_customer_information_with_home_directory_style'),
    url(r'^search_customer_spouse_information_with_free_style/$', search_customer_spouse_information_with_free_style, name='search_customer_spouse_information_with_free_style'),
    url(r'^dc_create_update_company_information/$', dc_create_update_company_information, name='dc_create_update_company_information'),
    #cụm add kết qua kiểm tra giấy tờ
    url(r'^dc_cl_super_total_comment/$', dc_cl_super_total_comment, name='dc_cl_super_total_comment'),



    url(r'^checklist_phv/', alias_checklist_phv_page, name='checklist_phv_page'),
    #cụm get app, lấy số điện thoại của KH và người thân, ngoài ra không còn làm cái chi nữa, hãy để nó đơn giản như thế
    url(r'^get_app_data_from_finnone_before_using_funtion_of_dedupe_mobi/', get_app_data_from_finnone_before_using_funtion_of_dedupe_mobi, name='get_app_data_from_finnone_before_using_funtion_of_dedupe_mobi'),
    #cụm kiểm tra kết quả dedupe trên check list của phv
    url(r'^search_customer_information_with_full_information_id_dob_name_on_phv_checklist/', search_customer_information_with_full_information_id_dob_name_on_phv_checklist, name='search_customer_information_with_full_information_id_dob_name_on_phv_checklist'),
    url(r'^search_customer_information_with_home_directory_style_on_phv_checklist/', search_customer_information_with_home_directory_style_on_phv_checklist, name='search_customer_information_with_home_directory_style_on_phv_checklist'),
    url(r'^search_customer_spouse_information_with_free_style_on_phv_checklist/', search_customer_spouse_information_with_free_style_on_phv_checklist, name='search_customer_spouse_information_with_free_style_on_phv_checklist'),
    url(r'^search_mobile_dedupe_mobile_phone_cux_and_ref_on_phv_checklist/', search_mobile_dedupe_mobile_phone_cux_and_ref_on_phv_checklist, name='search_mobile_dedupe_mobile_phone_cux_and_ref_on_phv_checklist'),
    url(r'^phv_create_update_company_information/', phv_create_update_company_information, name='phv_create_update_company_information'),
    #cụm add kết quả check list của phv (only main items)
    url(r'^phv_cl_super_total_comment/', phv_cl_super_total_comment, name='phv_cl_super_total_comment'),
    url(r'^phv_comment_for_every_mobile_check/', phv_comment_for_every_mobile_check, name='phv_comment_for_every_mobile_check'),
    url(r'^phv_create_send_fiv_data/', phv_create_send_fiv_data, name='phv_create_send_fiv_data'),
    url(r'^phv_send_request_pcb/', phv_send_request_pcb, name='phv_send_request_pcb'),



    url(r'^checklist_und/', alias_checklist_und_page, name='checklist_und_page'),
    url(r'^get_app_data_from_finnone_before_using_funtion_of_checklist_und/', get_app_data_from_finnone_before_using_funtion_of_checklist_und, name='get_app_data_from_finnone_before_using_funtion_of_checklist_und'),
    url(r'^search_customer_information_with_full_information_id_dob_name_on_und_checklist/', search_customer_information_with_full_information_id_dob_name_on_und_checklist, name='search_customer_information_with_full_information_id_dob_name_on_und_checklist'),
    url(r'^search_customer_information_with_home_directory_style_on_und_checklist/', search_customer_information_with_home_directory_style_on_und_checklist, name='search_customer_information_with_home_directory_style_on_und_checklist'),
    url(r'^search_customer_spouse_information_with_free_style_on_und_checklist/', search_customer_spouse_information_with_free_style_on_und_checklist, name='search_customer_spouse_information_with_free_style_on_und_checklist'),
    url(r'^search_mobile_dedupe_mobile_phone_cux_and_ref_on_und_checklist/', search_mobile_dedupe_mobile_phone_cux_and_ref_on_und_checklist, name='search_mobile_dedupe_mobile_phone_cux_and_ref_on_und_checklist'),
    url(r'^und_create_update_company_information/', und_create_update_company_information, name='und_create_update_company_information'),
    url(r'^und_cl_super_total_comment/', und_cl_super_total_comment, name='und_cl_super_total_comment'),


    url(r'^fi_und/', alias_fi_und_page, name='fi_und_page'),
    url(r'^export_fi_request/', export_fi_request_to_xls, name='export_fi_request_to_xls'),

    #cụm url kích hoạt update CSDL từ Finnone
    url(r'^checklistdedupeidfinnonedlview/$', ChecklistDedupeIDFinnoneDLView.as_view(), name='ChecklistDedupeIDFinnoneDLView'),

]
